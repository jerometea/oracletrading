﻿using Application.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Scripts
{
    public class SaveTimezoneExchangeQuery : IRequest { }

    public class SaveTimezoneExchangeHandler : IRequestHandler<SaveTimezoneExchangeQuery>
    {
        private IExchangeTimezoneReader _script;

        public SaveTimezoneExchangeHandler(IExchangeTimezoneReader script)
        {
            _script = script;
        }
        public Task<Unit> Handle(SaveTimezoneExchangeQuery request, CancellationToken cancellationToken)
        {
            _script.OpenJson();
            return null;
        }
    }
}
