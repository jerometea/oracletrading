﻿using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Currencies.UpdatesCurrencies
{
    public class UpdateEuroRatesQuery : IRequest { }

    public class UpdateEuroRatesHandler : IRequestHandler<UpdateEuroRatesQuery>
    {
        IVisionTradingDbContext _context;
        IFixer _fixer;

        public UpdateEuroRatesHandler(IVisionTradingDbContext context, IFixer fixer)
        {
            _context = context;
            _fixer = fixer;
        }
        public async Task<Unit> Handle(UpdateEuroRatesQuery request, CancellationToken cancellationToken)
        {
            Dictionary<string, double> currenciesRates = await _fixer.CurrenciesRates();

            List<Currency> currencies = _context.Currency.ToList();

            foreach (Currency currency in currencies)
            {
                if (currenciesRates.ContainsKey(currency.CurrencyShortName))
                {
                    double rate = currenciesRates[currency.CurrencyShortName];
                    currency.EuroRate = Math.Round(rate, 2);
                }
            }

            // ILA and ILS are same currencies (israel)
            Currency ILA = currencies.FirstOrDefault(currency => currency.CurrencyShortName == "ILA");
            double ILSRate = currenciesRates["ILS"];
            ILA.EuroRate = Math.Round(ILSRate, 2);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}

