﻿using Application.Common.Interfaces;
using Domain;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.RegressionSubtypes.SaveRegressionSubtypes
{
    public class SaveRegressionSubtypeQuery : IRequest { }

    class SaveRegressionSubtypeHandler : IRequestHandler<SaveRegressionSubtypeQuery>
    {
        IVisionTradingDbContext _context;
        IRegressionSubtypeReader _regressionSubtypeReader;

        public SaveRegressionSubtypeHandler(IVisionTradingDbContext context, IRegressionSubtypeReader regressionSubtypeReader)
        {
            _context = context;
            _regressionSubtypeReader = regressionSubtypeReader;
        }

        public async Task<Unit> Handle(SaveRegressionSubtypeQuery request, CancellationToken cancellationToken)
        {

            List<RegressionSubtype> regressionSubtypes = _regressionSubtypeReader.OpenRegressionSubtypes(true);

            List<RegressionSubtype> regressionSubtypesFromDb = _context.RegressionSubtype.ToList();

            foreach (RegressionSubtype regressionSubtype in regressionSubtypes)
            {
                if (!regressionSubtypesFromDb.Any(rsFromDb => rsFromDb.RegressionSubtypeName == regressionSubtype.RegressionSubtypeName))
                {
                    _context.RegressionSubtype.Add(regressionSubtype);
                }
            }

            foreach (RegressionSubtype rsFromDb in regressionSubtypesFromDb)
            {
                if (!regressionSubtypes.Any(regressionSubtype => regressionSubtype.RegressionSubtypeName == rsFromDb.RegressionSubtypeName))
                {
                    _context.RegressionSubtype.Remove(rsFromDb);
                }
            }

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
