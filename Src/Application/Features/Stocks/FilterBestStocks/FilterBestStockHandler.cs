﻿using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using System.IO;

namespace Application.Features.Stocks.FilterBestStocks
{
    public class FilterBestStockQuery : IRequest<HashSet<string>> { }

    class FilterBestStockHandler : IRequestHandler<FilterBestStockQuery, HashSet<string>>
    {
        IVisionTradingDbContext _context;

        public FilterBestStockHandler(IVisionTradingDbContext context,
                                            IMediator mediator)
        {
            _context = context;
        }
        public async Task<HashSet<string>> Handle(FilterBestStockQuery request, CancellationToken cancellationToken)
        {
            DateTime todayDate = new DateTime(2021, 7, 19).Date;

            HashSet<string> symbols = _context.Stock
            .Where(stock => stock.Statistics.Any(stat => stat.Timeframe.TimeframeName == "Max" && stat.RegressionSubtype.Regression.HasPositiveCoefficient && stat.Score <= 15 && stat.Stock.LastPrice >= 50))
            .Where(stock => stock.Statistics.Any(stat => stat.Timeframe.TimeframeName == "FiveYears" && stat.RegressionSubtype.Regression.HasPositiveCoefficient && stat.Score <= 15 && stat.Change > 0))
            .Where(stock => stock.Statistics.Any(stat => stat.Timeframe.TimeframeName == "OneYear" && stat.RegressionSubtype.Regression.HasPositiveCoefficient && stat.Score <= 8 && stat.LastModified.Value.Date == todayDate))
            .Where(stock => stock.Statistics.Any(stat => stat.Timeframe.TimeframeName == "OneDay" && stat.LastModified.Value.Date == todayDate))
            .AsEnumerable()
            .OrderBy(stock => stock.Statistics
                .Where(stat => stat.Timeframe is not null && stat.Timeframe.TimeframeName == "OneDay" && stat.LastModified.Value.Date == todayDate)
                .Select(stat => stat.Change).DefaultIfEmpty(9999)
                .Min())
            .Select(a => a.Symbol).ToHashSet();

            return symbols;
        }
    }
}