﻿using Application.Common.Helpers;
using Application.Common.Interfaces;
using Application.Features.Stocks.FilterBestStocks;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Stocks.DownloadStocks
{
    public class DownloadStocksQuery : IRequest
    {
        public TimeframeSelector Timeframes { get; set; }
        public HashSet<string> Symbols { get; set; }
        public DateTimeOffset? FrenchOpenDate { get; set; }
        public bool HasFilterBestStocks { get; set; }
        public bool IsItForPortfolioExport { get; set; }
        public DownloadStocksQuery(TimeframeSelector timeframes, HashSet<string> symbols, DateTimeOffset? frenchOpenDate, bool hasFilterBestStocks, bool isItForPortfolioExport)
        {
            Timeframes = timeframes;
            Symbols = symbols;
            FrenchOpenDate = frenchOpenDate;
            HasFilterBestStocks = hasFilterBestStocks;
            IsItForPortfolioExport = isItForPortfolioExport;
        }
    }

    public class DownloadStocksHandler : IRequestHandler<DownloadStocksQuery>
    {
        IYahoo _downloader;
        IMediator _mediator;

        public DownloadStocksHandler(IYahoo downloader, IMediator mediator)
        {
            _downloader = downloader;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(DownloadStocksQuery request, CancellationToken cancellationToken)
        {
            if (request.HasFilterBestStocks)
            {
                FilterBestStockQuery query = new FilterBestStockQuery();
                request.Symbols = await _mediator.Send(query);
            }

            await _downloader.DownloadStockHistoric(request.Symbols, request.Timeframes, request.FrenchOpenDate, request.IsItForPortfolioExport);

            return Unit.Value;
        }
    }
}
