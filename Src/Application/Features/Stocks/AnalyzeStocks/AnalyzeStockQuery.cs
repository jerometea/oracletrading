﻿using Application.Common.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Features.Stocks.AnalyzeStocks
{
    public class AnalyzeStockQuery : IRequest
    {
        public TimeframeSelector Timeframes { get; set; }
        public bool HasFilterBestStocks { get; set; }


        public AnalyzeStockQuery(TimeframeSelector timeframes, bool hasFilterBestStocks)
        {
            Timeframes = timeframes;
            HasFilterBestStocks = hasFilterBestStocks;
        }
    }
}

