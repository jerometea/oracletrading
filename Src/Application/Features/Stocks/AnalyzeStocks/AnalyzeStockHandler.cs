﻿using Application.Common.Interfaces;
using Application.Common.Mapper;
using Application.Common.Models.Yahoo;
using Application.Common.PathConfigurations;
using Application.Features.Stocks.FilterBestStocks;
using Application.Interfaces;
using Domain.Calculations;
using Domain.Calculations.ChangeCounter;
using Domain.Entities;
using Domain.Statistics;
using MediatR;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Stocks.AnalyzeStocks
{
    public class AnalyzeStockHandler : IRequestHandler<AnalyzeStockQuery>
    {
        IVisionTradingDbContext _context;
        RegressionFit _calculateStatitic;
        Folder _paths;
        IStockReader _jsonReader;
        IMediator _mediator;

        public AnalyzeStockHandler(RegressionFit calculateStatistic,
                                         IVisionTradingDbContext context,
                                         IOptions<Folder> pathsConfiguration,
                                         IStockReader jsonReader,
                                         IMediator mediator)
        {
            _context = context;
            _calculateStatitic = calculateStatistic;
            _paths = pathsConfiguration.Value;
            _jsonReader = jsonReader;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(AnalyzeStockQuery request, CancellationToken cancellationToken)
        {

            List<Timeframe> periods = _context.Timeframe.ToList();
            List<Stock> stocks = _context.Stock.ToList();
            List<RegressionSubtype> regressionSubtypes = _context.RegressionSubtype.ToList();

            HashSet<string> bestStocks = null;

            if (request.HasFilterBestStocks)
            {

                FilterBestStockQuery query = new FilterBestStockQuery();
                bestStocks = await _mediator.Send(query);
            }

            List<YahooTimeframeModel> jsonStocks = _jsonReader.OpenJson(request.Timeframes, bestStocks);

            int count = 0;
            int nullFirstPrice = 0;
            foreach (YahooTimeframeModel json in jsonStocks)
            {
                count++;

                string symbol = json.result[0].meta.symbol;

                List<double?> historic = json.result[0].indicators.quote[0].close;
                string yahooTimeframe = json.result[0].meta.range;
                Timeframe timeframe = TimeframeMapper.ToTimeframeEntity(yahooTimeframe, periods);
                double integrity = PercentIntegrity.Calculate(historic);


                Stock stock = stocks.Where(stock => stock.Symbol == symbol).FirstOrDefault();

                if (stock == null)
                    continue;

                List<double> historicWithoutNulls = HistoricFill.CompleteInvalidHistoric(historic);

                Statistic result = _calculateStatitic.Analyse(historicWithoutNulls);

                Statistic statistic = stock.Statistics.Where(stat => stat.TimeframeId == timeframe.TimeframeId).FirstOrDefault();

                if (integrity >= 75 && result.Score <= 15)
                {
                    bool isNewStatistic = false;

                    if (statistic == null)
                    {
                        statistic = new Statistic();
                        isNewStatistic = true;
                    }

                    double firstPrice = 0;
                    double lastPrice= 0;

                    if (timeframe.TimeframeName.Contains("Day"))
                    {
                        List<double?> sixMonthsHistoric = jsonStocks.Where(json => json.result[0].meta.symbol == symbol && json.result[0].meta.range == "6mo").FirstOrDefault().result[0].indicators.quote[0].close;

                        string tempRange = yahooTimeframe.Replace("d", "");

                        int range = int.Parse(tempRange);

                        int firstPriceIndex = sixMonthsHistoric.Count - 1 - range;

                        if (sixMonthsHistoric[firstPriceIndex] is null)
                        {
                            nullFirstPrice++;
                            continue;
                        }

                        firstPrice = sixMonthsHistoric[firstPriceIndex].Value;
                        lastPrice = sixMonthsHistoric.Last().Value;
                    }
                    else
                    {
                        firstPrice = historic.First(price => price != null).Value;
                        lastPrice = historic.Last(price => price != null).Value;
                    }


                    statistic.Change = ChangePercent.Calculate(firstPrice, lastPrice);
                    statistic.TimeframeId = timeframe.TimeframeId;
                    statistic.Integrity = integrity;
                    statistic.TotalValues = historic.Count;
                    statistic.Score = result.Score;
                    statistic.RegressionSubtypeId = regressionSubtypes.Where(regressionSubtype => regressionSubtype.RegressionSubtypeName == result.RegressionSubtype.RegressionSubtypeName).FirstOrDefault().RegressionSubtypeId;
                    statistic.StockId = stocks.Where(stock => stock.Symbol == symbol).FirstOrDefault().StockId;
                    statistic.LastModified = DateTimeOffset.Now;

                    if (3 <= timeframe.TimeframeId && timeframe.TimeframeId <= 5)
                    {
                        var peaks = MinimaMaximaFinder.GetMinimasMaximasPositions(historicWithoutNulls);

                        ChangeCounter changeCounter = new ChangeCounter(historicWithoutNulls, peaks);
                        statistic.Changes.Clear();
                        statistic.Changes.AddRange(changeCounter.CountChanges());

                    }

                    if (isNewStatistic)
                    {
                        _context.Statistic.Add(statistic);
                    }
                }

                else
                {
                    if (statistic != null)
                        _context.Statistic.Remove(statistic);
                }
            }
            await _context.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
