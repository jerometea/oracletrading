﻿using Application.Common.Helpers;
using Application.Common.Interfaces;
using Application.Common.Models.Yahoo;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Stocks.SaveStocks.SaveBirthdate
{
    public class SaveBirthdateQuery : IRequest
    {
        public TimeframeSelector Timeframes { get; set; }
        public SaveBirthdateQuery(TimeframeSelector timeframes)
        {
            Timeframes = timeframes;
        }

    }
    class SaveBirthdateHandler : IRequestHandler<SaveBirthdateQuery>
    {
        IVisionTradingDbContext _context;
        IStockReader _jsonReader;

        public SaveBirthdateHandler(IVisionTradingDbContext context, IStockReader jsonReader)
        {
            _context = context;
            _jsonReader = jsonReader;
        }

        public async Task<Unit> Handle(SaveBirthdateQuery request, CancellationToken cancellationToken)
        {
            List<YahooTimeframeModel> maxJsons = _jsonReader.OpenJson(request.Timeframes, null);

            List<Stock> stocks = _context.Stock.ToList();

            int count = 0;

            foreach (YahooTimeframeModel json in maxJsons)
            {
                count++;

                int birthdateTimestamp = (int)json.result[0].timestamp.First(timestamp => timestamp.HasValue);

                DateTimeOffset birthdate = DateTimeOffset.FromUnixTimeSeconds(birthdateTimestamp);

                string symbol = json.result[0].meta.symbol;

                if (string.IsNullOrWhiteSpace(symbol))
                    continue;

                Stock stock = stocks.FirstOrDefault(stock => stock.Symbol == symbol);

                if (stock != null)
                {
                    stock.Birthdate = birthdate;
                }
            }

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
