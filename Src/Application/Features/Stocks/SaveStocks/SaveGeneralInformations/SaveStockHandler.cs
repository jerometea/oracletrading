﻿using Application.Common.Interfaces;
using Application.Common.Models.Yahoo;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Stocks.SaveStocks
{
    public class SaveStockHandler : IRequestHandler<SaveStockQuery>
    {
        IVisionTradingDbContext _context;
        IStockReader _jsonReader;

        public SaveStockHandler(IVisionTradingDbContext context, IStockReader jsonReader)
        {
            _context = context;
            _jsonReader = jsonReader;
        }

        public async Task<Unit> Handle(SaveStockQuery request, CancellationToken cancellationToken)
        {
            List<YahooTimeframeModel> jsonStocks = _jsonReader.OpenJson(request.Periods, null);
            List<Currency> currencies = _context.Currency.ToList();
            List<Timezone> timezones = _context.Timezone.ToList();
            List<Exchange> exchanges = _context.Exchange.ToList();
            List<Stock> stocks = _context.Stock.ToList();

            int count = 0;
            foreach (YahooTimeframeModel json in jsonStocks)
            {
                count++;
                Meta meta = json.result[0].meta;

                string symbolJson = meta.symbol;

                if (string.IsNullOrWhiteSpace(symbolJson))
                    continue;

                Stock stockFromDb = stocks.FirstOrDefault(stock => stock.Symbol == symbolJson);

                if (stockFromDb == null)
                {
                    continue;
                }

                string exchangeNameJson = string.IsNullOrWhiteSpace(meta.exchangeName) ? "n/a" : meta.exchangeName;
                string currencyJson = string.IsNullOrWhiteSpace(meta.currency) ? "n/a" : meta.currency;
                int volume = json.result[0].indicators.quote[0].volume == null ? 0 : (int)json.result[0].indicators.quote[0].volume.Max();

                Exchange exchange = exchanges.FirstOrDefault(exchange => exchange.ExchangeShortName == exchangeNameJson);
                Timezone timezone = timezones.FirstOrDefault(timezone => timezone.TimezoneName == timezoneJson);
                Currency currency = currencies.FirstOrDefault(currency => currency.CurrencyShortName.ToLower() == currencyJson.ToLower());

                Stock stock = new Stock();
                stockFromDb.ExchangeId = exchange.ExchangeId;
                exchange.TimezoneId = timezone.TimezoneId;
                stockFromDb.CurrencyId = currency.CurrencyId;
                stockFromDb.Symbol = symbolJson;
                stockFromDb.Volume = volume;

                _context.Stock.Add(stock);

            }
            await _context.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
