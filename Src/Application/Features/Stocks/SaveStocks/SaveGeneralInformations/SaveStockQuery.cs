﻿using Application.Common.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Features.Stocks.SaveStocks
{
    public class SaveStockQuery : IRequest
    {
        public TimeframeSelector Periods { get; set; }

        public SaveStockQuery(bool OneDay, bool TwoDays, bool ThreeDays, bool FourDays, bool FiveDays, 
                              bool SixDays, bool SevenDays, bool EightDays, bool NineDays, bool TenDays, bool ElevenDays, bool TwelveDays, 
                              bool OneMonth, bool SixMonths, bool OneYear, bool YTD, bool FiveYears, bool Max, 
                              bool SecondDay, bool ThirdDay, bool FourthDay, bool FifthDay)
        {
            Periods = new TimeframeSelector(OneDay, TwoDays, ThreeDays, FourDays, FiveDays,
                                            SixDays, SevenDays, EightDays, NineDays, TenDays, ElevenDays, TwelveDays,
                                            OneMonth, SixMonths, OneYear, YTD, FiveYears, Max, 
                                            SecondDay, ThirdDay, FourthDay, FifthDay);
        }
    }
}
