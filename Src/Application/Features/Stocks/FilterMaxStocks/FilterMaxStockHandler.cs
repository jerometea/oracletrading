﻿using Application.Common.Interfaces;
using Application.Features.Stocks.FilterBestStocks;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Stocks.FilterMaxStocks
{
    public class FilterMaxStockQuery : IRequest<HashSet<string>> { }
    class FilterMaxStockHandler : IRequestHandler<FilterMaxStockQuery, HashSet<string>>
    {
        IVisionTradingDbContext _context;

        public FilterMaxStockHandler(IVisionTradingDbContext context)
        {
            _context = context;
        }

        public async Task<HashSet<string>> Handle(FilterMaxStockQuery request, CancellationToken cancellationToken)
        {
            DateTime todayDate = DateTime.Now.Date;
            HashSet<string> symbols = _context.Stock.Where(stock => stock.Statistics.Any(stat => 
                                                     stat.Timeframe.TimeframeName == "Max" && 
                                                     stat.RegressionSubtype.Regression.HasPositiveCoefficient &&
                                                     stat.Score <= 13 &&
                                                     stat.Stock.LastPrice >= 150 &&
                                                     stat.Change > 200 &&
                                                     stat.LastModified.Value.Date == todayDate))
                                                    .Select(stock => stock.Symbol).ToHashSet();
            return symbols;
        }
    }
}
