﻿using Application.Common.Helpers;
using Application.Common.Interfaces;
using Application.Common.Models.Yahoo;
using Application.Features.Stocks.FilterMaxStocks;
using Domain.Calculations;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Stocks.UpdateStockPrice
{
    public class UpdateStockPriceQuery : IRequest { }
    class UpdateStockPriceHandler : IRequestHandler<UpdateStockPriceQuery>
    {
        IVisionTradingDbContext _context;
        IStockReader _jsonReader;


        public UpdateStockPriceHandler(IVisionTradingDbContext context, IStockReader jsonReader)
        {
            _context = context;
            _jsonReader = jsonReader;
        }

        public async Task<Unit> Handle(UpdateStockPriceQuery request, CancellationToken cancellationToken)
        {

            // get Max timeframe
            TimeframeSelector timeframe = new TimeframeSelector(false, false, false, false, false, false, false, false,false, false, false, false, false, false, false, false, false,true,false, false, false, false);
            List<YahooTimeframeModel> jsonStocks = _jsonReader.OpenJson(timeframe, null);
            List<Stock> stocks = _context.Stock.ToList();

            int count = 0;

            foreach (YahooTimeframeModel json in jsonStocks)
            {
                count++;
                string symbol = json.result[0].meta.symbol;

                Stock stock = stocks.Where(stock => stock.Symbol == symbol).FirstOrDefault();

                if (stock == null)
                    continue;

                List<double?> historic = json.result[0].indicators.quote[0].close;

                if (stock.Currency.CurrencyShortName == "BTC" || stock.Currency.CurrencyShortName == "ETH")
                {
                    continue;
                }

                double euroRate = stock.Currency.EuroRate;

                double lastPriceInLocalCurrency = historic.Last(price => price != null).Value;
                lastPriceInLocalCurrency = Math.Round(lastPriceInLocalCurrency, 6);

                double lastPrice = CurrencyConverter.ConvertToEuro(euroRate, lastPriceInLocalCurrency);

                if (double.IsNaN(euroRate) ||
                    double.IsNaN(lastPriceInLocalCurrency) || double.IsInfinity(lastPriceInLocalCurrency) ||
                    double.IsNaN(lastPrice) || double.IsInfinity(lastPrice))
                {
                    Console.WriteLine($"{symbol} | {euroRate} | {stock.Currency.CurrencyShortName}");
                }

                stock.LastPrice = lastPrice;
                stock.LastPriceInForeignCurrency = lastPriceInLocalCurrency;

            }
            await _context.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
