﻿using Application.Common.Interfaces;
using Application.Features.Simulator.UpdatePortfolios.UpdatePositions;
using Domain.Entities.Simulator;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Simulator.UpdatePortfolios.UpdatePortfolio
{
    public class UpdatePortfolioQuery : IRequest
    {
        public int PortfolioId { get; set; }
        public bool IsItPortfolioUpdate { get; set; }
        public int DaysAfterOpenDate { get; set; }
        public UpdatePortfolioQuery(int portfolioId, bool isItPortfolioUpdate, int daysAfterOpenDate)
        {
            PortfolioId = portfolioId;
            IsItPortfolioUpdate = isItPortfolioUpdate;
            DaysAfterOpenDate = daysAfterOpenDate;
        }
    }

    class UpdatePortfolioHandler : IRequestHandler<UpdatePortfolioQuery>
    {
        IVisionTradingDbContext _context;
        IMediator _mediator;

        public UpdatePortfolioHandler(IVisionTradingDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(UpdatePortfolioQuery request, CancellationToken cancellationToken)
        {
            Portfolio portfolio = _context.Portfolio.Where(portfolio => portfolio.PortfolioId == request.PortfolioId).FirstOrDefault();

            if (request.IsItPortfolioUpdate)
            {
                UpdatePositionQuery updatePositionQuery = new UpdatePositionQuery(portfolio, request.DaysAfterOpenDate);
                await _mediator.Send(updatePositionQuery);

                if (portfolio.IsInRealTime)
                {
                    portfolio.LastUpdate = DateTime.Now.Date;
                    portfolio.Days = (int)(portfolio.LastUpdate - portfolio.OpenDate.Date).TotalDays;
                }
                else
                {
                    portfolio.BeforeLastUpdate = portfolio.LastUpdate;
                    portfolio.LastUpdate = portfolio.OpenDate.AddDays(portfolio.Days).Date;
                }

                double intervalUpdate = (portfolio.LastUpdate - portfolio.BeforeLastUpdate).TotalDays;

                if (intervalUpdate != 0)
                {
                    portfolio.BeforeLastTotalValue = portfolio.TotalValue;

                    portfolio.TotalValue = _context.Position.Where(position => position.PortfolioId == portfolio.PortfolioId)
                                                            .Sum(position => position.TotalValue);

                    portfolio.TodayChange = Math.Round(portfolio.TotalValue - portfolio.BeforeLastTotalValue, 2);
                }

                if (portfolio.BeforeLastTotalValue == 0)
                {
                    portfolio.TodayChangePercentage = 0;
                }
                else
                {
                    portfolio.TodayChangePercentage = portfolio.TodayChange / portfolio.BeforeLastTotalValue * 100;
                    portfolio.TodayChangePercentage = Math.Round(portfolio.TodayChangePercentage, 2);
                }

                portfolio.Days = request.DaysAfterOpenDate;
            }

            portfolio.PositionsBough = portfolio.Positions.Count;

            portfolio.PositionsPositive = portfolio.Positions.Where(position => (int)position.Gain > 0).Count();
            portfolio.PositionsNeutral = portfolio.Positions.Where(position => (int)position.Gain == 0).Count();
            portfolio.PositionsNegative = portfolio.Positions.Where(position => (int)position.Gain < 0).Count();

            double sucessRate = portfolio.PositionsPositive / portfolio.PositionsBough * 100;
            portfolio.SuccessRate = Math.Round(sucessRate, 2);

            portfolio.TotalValue = _context.Position.Where(position => position.PortfolioId == portfolio.PortfolioId)
                                                    .Sum(position => position.TotalValue);

            portfolio.Gain = Math.Round(portfolio.TotalValue - portfolio.Investment, 2);

            portfolio.GainPercentage = portfolio.Gain / portfolio.Investment * 100;
            portfolio.GainPercentage = Math.Round(portfolio.GainPercentage, 2);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
