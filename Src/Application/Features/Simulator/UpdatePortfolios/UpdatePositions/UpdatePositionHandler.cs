﻿using Application.Common.Helpers;
using Application.Common.Interfaces;
using Application.Common.Models.Yahoo;
using Application.Features.Stocks.DownloadStocks;
using Domain.Calculations;
using Domain.Entities.Simulator;
using MediatR;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TimeZoneConverter;

namespace Application.Features.Simulator.UpdatePortfolios.UpdatePositions
{

    public class UpdatePositionHandler : IRequestHandler<UpdatePositionQuery>
    {
        IVisionTradingDbContext _context;
        IStockReader _jsonReader;
        IMediator _mediator;

        public UpdatePositionHandler(IVisionTradingDbContext context, IStockReader jsonReader, IMediator mediator)
        {
            _context = context;
            _jsonReader = jsonReader;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(UpdatePositionQuery request, CancellationToken cancellationToken)
        {
            List<Position> positions = _context.Position.Where(position => position.PortfolioId == request.Portfolio.PortfolioId).ToList();

            HashSet<string> symbols = positions.Select(position => position.Stock.Symbol).ToHashSet();

            TimeframeSelector timeframes = new TimeframeSelector(true,false,false,false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,false);

            DateTimeOffset downloadDate = request.Portfolio.OpenDate.AddDays(request.DaysAfterOpenDate);
            DownloadStocksQuery downloadStockLastPrice = new DownloadStocksQuery(timeframes, symbols, downloadDate, false, true);
            await _mediator.Send(downloadStockLastPrice);

            List<YahooTimeframeModel> jsonStocks = _jsonReader.OpenJson(timeframes, symbols);

            foreach (Position position in positions)
            {
                YahooTimeframeModel oneDayJson = jsonStocks.FirstOrDefault(json => json.result[0].meta.symbol == position.Stock.Symbol);

                if (oneDayJson == null)
                    continue;

                double euroRate = position.Stock.Currency.EuroRate;

                List<int?> DatesOfPricesInTimestamps = oneDayJson.result[0].timestamp.ToList();

                int mostRecenDatePriceInTimestamp = DatesOfPricesInTimestamps.Last(timestamp => timestamp != null).Value;

                List<double?> pricesInLocalCurrency = oneDayJson.result[0].indicators.quote[0].close;

                double lastPriceInForeignCurrency = pricesInLocalCurrency.Last(price => price != null).Value;

                lastPriceInForeignCurrency = Math.Round(lastPriceInForeignCurrency, 6);

                double currentEuroPrice = CurrencyConverter.ConvertToEuro(euroRate, lastPriceInForeignCurrency);

                if (currentEuroPrice == 0) // foreign currency too low to be converted in euro (eg : 0.38 INR = 0.0042 EURO)
                    continue;

                position.CurrentPrice = currentEuroPrice;

                DateTimeOffset mostRecentDatePrice = DateTimeOffset.FromUnixTimeSeconds(mostRecenDatePriceInTimestamp);

                double intervalUpdate = (mostRecentDatePrice - position.ForeignLastUpdateDate.Date).TotalDays;

                string foreignTimeZoneId = TZConvert.IanaToWindows(position.Stock.Exchange.Timezone.TimezoneName);

                TimeZoneInfo foreignStandardTime = TimeZoneInfo.FindSystemTimeZoneById(foreignTimeZoneId);


                DateTimeOffset mostRecentDatePriceInForeignDate = TimeZoneInfo.ConvertTime(mostRecentDatePrice, foreignStandardTime);

                if (intervalUpdate > 0)
                {
                    if (mostRecentDatePriceInForeignDate.DateTime.Date != position.ForeignLastUpdateDate.DateTime.Date)
                    {
                        position.BeforeLastTotalValue = position.TotalValue;
                    }
                    position.BeforeLastUpdate = position.ForeignLastUpdateDate.Date;
                }

                else
                {
                    position.TodayChangePercentage = 0;
                }

                position.Gain = position.Quantity * currentEuroPrice - position.Investment;
                position.Gain = Math.Round(position.Gain, 2);

                position.TotalValue = currentEuroPrice * position.Quantity;

                if (position.ForeignOpenDate.Date == position.BeforeLastUpdate)
                {
                    position.TodayChange = Math.Round(position.TotalValue - position.Investment, 2);

                    position.TodayChangePercentage = Math.Round(position.TodayChange / position.Investment * 100, 2);
                }

                else
                {
                    position.TodayChange = Math.Round(position.TotalValue - position.BeforeLastTotalValue, 2);

                    if (position.BeforeLastTotalValue > 0)
                        position.TodayChangePercentage = Math.Round((position.TotalValue - position.BeforeLastTotalValue) / position.BeforeLastTotalValue * 100, 2);
                }

                position.CurrentPriceInForeignCurrency = lastPriceInForeignCurrency;

                position.ForeignLastUpdateDate = TimeZoneInfo.ConvertTime(mostRecentDatePrice, foreignStandardTime);

                position.Days = position.ForeignLastUpdateDate.Subtract(position.ForeignLastUpdateDate).Days;

                position.GainPercentage = position.Gain / position.Investment * 100;

                position.GainPercentage = Math.Round(position.GainPercentage, 2);
            }

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}