﻿using Domain.Entities.Simulator;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Features.Simulator.UpdatePortfolios.UpdatePositions
{
    public class UpdatePositionQuery : IRequest
    {
        public Portfolio Portfolio { get; set; }
        public int DaysAfterOpenDate { get; set; }

        public UpdatePositionQuery(Portfolio portfolio, int daysAfterOpenDate)
        {
            Portfolio = portfolio;
            DaysAfterOpenDate = daysAfterOpenDate;
        }
    }
}
