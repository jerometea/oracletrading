﻿using Application.Common.Interfaces;
using Domain.Entities.Simulator;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Simulator.ExportPortfolios.ExportOptions
{
    public class ExportOptionsQuery : IRequest
    {
        public bool HasEveryPastPortfolio { get; set; }
        public bool HasEveryRealTimePortfolio { get; set; }
        public int PortfolioId { get; set; }
        public ExportOptionsQuery(bool hasEveryRealTimePortfolioExport, bool hasEveryPastPortfolioExport, int portfolioId)
        {
            HasEveryRealTimePortfolio = hasEveryRealTimePortfolioExport;
            HasEveryPastPortfolio = hasEveryPastPortfolioExport;
            PortfolioId = portfolioId;
        }
    }
    class ExportOptionsHandler : IRequestHandler<ExportOptionsQuery>
    {
        IVisionTradingDbContext _context;
        IMediator _mediator;

        public ExportOptionsHandler(IVisionTradingDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(ExportOptionsQuery request, CancellationToken cancellationToken)
        {

            List<Portfolio> portfolioExport = new List<Portfolio>();

            if (request.HasEveryPastPortfolio)
            {
                List<Portfolio> pastPortfolios = _context.Portfolio.Where(portfolio => !portfolio.IsInRealTime).ToList();
                portfolioExport.AddRange(pastPortfolios);
            }

            if (request.HasEveryRealTimePortfolio)
            {
                List<Portfolio> realTimePortfolios = _context.Portfolio.Where(portfolio => portfolio.IsInRealTime).ToList();
                portfolioExport.AddRange(realTimePortfolios);
            }

            if (request.PortfolioId > 0)
            {
                Portfolio portfolio = await _context.Portfolio.FindAsync(request.PortfolioId);
                portfolioExport.Add(portfolio);
            }

            foreach (Portfolio portfolio in portfolioExport)
            {
                int startDay = portfolio.Days;

                int endDay;

                if (portfolio.IsInRealTime)
                {
                    endDay = (int)DateTimeOffset.Now.Subtract(portfolio.OpenDate).TotalDays;
                }
                else
                {
                    endDay = 90;
                }

                while (startDay <= endDay)
                {
                    DayOfWeek dayOfWeek = portfolio.OpenDate.AddDays(startDay).DayOfWeek;
                    if (dayOfWeek != DayOfWeek.Saturday && dayOfWeek != DayOfWeek.Sunday)
                    {
                        ExportPortfolioQuery query = new ExportPortfolioQuery(portfolio.PortfolioId, startDay);
                        await _mediator.Send(query);
                    }
                    startDay++;
                }
            }
            return Unit.Value;
        }
    }
}
