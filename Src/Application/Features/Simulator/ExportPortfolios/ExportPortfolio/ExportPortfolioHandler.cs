﻿using Application.Common.Interfaces;
using Application.Common.Interfaces.Files;
using Application.Features.Simulator.UpdatePortfolios.UpdatePortfolio;
using Domain.Entities.Simulator;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Simulator.ExportPortfolios
{
    public class ExportPortfolioQuery : IRequest
    {
        public int PortfolioId { get; set; }
        public int DaysAfterOpenDate { get; set; }
        public ExportPortfolioQuery(int portfolioId, int daysAfterOpenDate)
        {
            PortfolioId = portfolioId;
            DaysAfterOpenDate = daysAfterOpenDate;
        }
    }

    class ExportPortfolioHandler : IRequestHandler<ExportPortfolioQuery>
    {
        IVisionTradingDbContext _context;
        IMediator _mediator;
        IExportPortfolio _export;
        public ExportPortfolioHandler(IVisionTradingDbContext context, IExportPortfolio export, IMediator mediator)
        {
            _export = export;
            _context = context;
            _mediator = mediator;
        }
        public async Task<Unit> Handle(ExportPortfolioQuery request, CancellationToken cancellationToken)
        {
            Portfolio portfolio = _context.Portfolio.Find(request.PortfolioId);

            SortedDictionary<string, List<string>> gains = new SortedDictionary<string, List<string>>();

            await _context.SaveChangesAsync(cancellationToken);
            if(request.DaysAfterOpenDate > 0)
            {
                UpdatePortfolioQuery updatePortfolioQuery = new UpdatePortfolioQuery(request.PortfolioId, true, request.DaysAfterOpenDate);

                await _mediator.Send(updatePortfolioQuery);
            }

            gains.Add("^0_SuccessRate", new List<string>() { (portfolio.SuccessRate * 10).ToString() });

            gains.Add("^0_GainPercentage", new List<string>() { (portfolio.GainPercentage * 100).ToString() });

            string date = portfolio.OpenDate.AddDays(portfolio.Days).Date.ToShortDateString();

            gains.Add("^0_Date", new List<string>() { date });

            foreach (Position position in portfolio.Positions)
            {

                if (gains.ContainsKey(position.Stock.Symbol))
                {
                    gains[position.Stock.Symbol].Add(position.Gain.ToString());
                }
                else
                {
                    gains.Add(position.Stock.Symbol, new List<string>() { position.Gain.ToString() });
                }
            }

            int iteratedSymbols = 0;

            var chunkSymbols = new SortedDictionary<string, List<string>>();
            var allSymbols = new List<SortedDictionary<string, List<string>>>();
            allSymbols.Add(chunkSymbols);

            foreach (var gain in gains)
            {
                if (iteratedSymbols > 0 && iteratedSymbols % 200 == 0)
                {
                    chunkSymbols = new SortedDictionary<string, List<string>>();
                    chunkSymbols.Add("^0_SuccessRate", new List<string>() { (portfolio.SuccessRate * 10).ToString() });
                    chunkSymbols.Add("^0_GainPercentage", new List<string>() { (portfolio.GainPercentage * 100).ToString() });
                    chunkSymbols.Add("^0_Date", new List<string>() { date });
                    allSymbols.Add(chunkSymbols);
                }
                chunkSymbols.Add(gain.Key, gain.Value);
                iteratedSymbols++;
            }

            int sheet = 0;
            foreach (var chunkSymbol in allSymbols)
            {
                _export.ExportReport(request.PortfolioId, chunkSymbol, portfolio.IsInRealTime, sheet);
                sheet++;
            }

            return Unit.Value;

        }
    }
}
