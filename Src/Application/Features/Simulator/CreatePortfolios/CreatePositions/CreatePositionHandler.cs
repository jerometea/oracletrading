﻿using Application.Common.Helpers;
using Application.Common.Interfaces;
using Application.Common.Models.Yahoo;
using Domain.Calculations;
using Domain.Entities;
using Domain.Entities.Simulator;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TimeZoneConverter;

namespace Application.Features.Simulator.CreatePortfolios.CreatePositions
{
    public class CreatePositionQuery : IRequest
    {
        public int PortfolioId { get; set; }
        public HashSet<string> Symbols { get; set; }
        public DateTimeOffset? PastOpenDate { get; set; }

        public CreatePositionQuery(int portfolioId, HashSet<string> symbols, DateTimeOffset? pastOpenDate)
        {
            PortfolioId = portfolioId;
            Symbols = symbols;
            PastOpenDate = pastOpenDate;
        }
    }

    public class CreatePositionHandler : IRequestHandler<CreatePositionQuery>
    {
        IVisionTradingDbContext _context;
        IStockReader _jsonReader;

        public CreatePositionHandler(IVisionTradingDbContext context, IStockReader jsonReader)
        {
            _context = context;
            _jsonReader = jsonReader;
        }

        public async Task<Unit> Handle(CreatePositionQuery request, CancellationToken cancellationToken)
        {
            List<Stock> stocks = _context.Stock.Where(stock => request.Symbols.Contains(stock.Symbol)).ToList();

            List<Statistic> statistics = _context.Statistic.Where(statistic => request.Symbols.Contains(statistic.Stock.Symbol)).ToList();

            TimeframeSelector timeframes = new TimeframeSelector(true,false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false);

            List<YahooTimeframeModel> jsonStocks = _jsonReader.OpenJson(timeframes, request.Symbols);

            List<Currency> currencies = _context.Currency.ToList();

            foreach (YahooTimeframeModel json in jsonStocks)
            {
                Stock stock = stocks.FirstOrDefault(s => s.Symbol == json.result[0].meta.symbol);

                if (stock == null)
                    continue;

                double euroRate = stock.Currency.EuroRate;

                List<double?> pricesInLocalCurrency = json.result[0].indicators.quote[0].close;

                double lastPriceInLocalCurrency = pricesInLocalCurrency.Last(price => price != null).Value;

                lastPriceInLocalCurrency = Math.Round(lastPriceInLocalCurrency, 6);

                Position position = new Position();

                position.PortfolioId = request.PortfolioId;

                position.StockId = stock.StockId;

                position.PurchasePrice = CurrencyConverter.ConvertToEuro(euroRate, lastPriceInLocalCurrency);

                position.CurrentPrice = position.PurchasePrice;

                position.Quantity = (int)(20_000 / position.PurchasePrice);

                if (position.Quantity == 0)
                    continue;

                position.Investment = position.Quantity * position.PurchasePrice;

                position.TotalValue = position.Quantity * position.CurrentPrice;

                position.TodayChange = 0;
                position.TodayChangePercentage = 0;

                position.Gain = 0;
                position.GainPercentage = 0;

                position.PurchasePriceInForeignCurrency = lastPriceInLocalCurrency;
                position.CurrentPriceInForeignCurrency = lastPriceInLocalCurrency;

                position.Days = 0;

                string foreignTimeZoneId = TZConvert.IanaToWindows(stock.Exchange.Timezone.TimezoneName);

                TimeZoneInfo foreignStandardTime = TimeZoneInfo.FindSystemTimeZoneById(foreignTimeZoneId);

                position.ForeignOpenDate = request.PastOpenDate != null ? request.PastOpenDate.Value : TimeZoneInfo.ConvertTime(DateTimeOffset.UtcNow, foreignStandardTime);
                position.ForeignCloseDate = null;
                position.ForeignLastUpdateDate = position.ForeignOpenDate;

                position.BeforeLastUpdate = position.ForeignOpenDate.Date;
                position.BeforeLastTotalValue = 0;

                List<Statistic> SymbolStatistics = statistics.Where(stat => stat.Stock.Symbol == stock.Symbol).ToList();

                foreach (Statistic statistic in SymbolStatistics)
                {
                    StatisticPosition statisticPosition = new StatisticPosition();
                    statisticPosition.PositionId = position.PositionId;
                    statisticPosition.Integrity = statistic.Integrity;
                    statisticPosition.RegressionSubtypeId = statistic.RegressionSubtypeId;
                    statisticPosition.Score = statistic.Score;
                    statisticPosition.StockId = statistic.StockId;
                    statisticPosition.TimeframeId = statistic.TimeframeId;
                    statisticPosition.TotalValues = statistic.TotalValues;
                    statisticPosition.Change = statistic.Change;

                    position.StatisticsArchives.Add(statisticPosition);
                }

                _context.Position.Add(position);
            }

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
