﻿using Application.Common.Helpers;
using Application.Common.Interfaces;
using Application.Features.Simulator.CreatePortfolios.CreatePositions;
using Application.Features.Simulator.UpdatePortfolios.UpdatePortfolio;
using Application.Features.Stocks.AnalyzeStocks;
using Application.Features.Stocks.DownloadStocks;
using Application.Features.Stocks.FilterBestStocks;
using Domain.Entities.Simulator;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Simulator.CreatePortfolios.CreatePortfolio
{
    class CreatePortfolioHandler : IRequestHandler<CreatePortfolioQuery>
    {
        IVisionTradingDbContext _context;
        IMediator _mediator;

        public CreatePortfolioHandler(IVisionTradingDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(CreatePortfolioQuery request, CancellationToken cancellationToken)
        {
            Portfolio portfolio = new Portfolio();

            portfolio.IsInRealTime = request.IsItInRealTime;

            if (string.IsNullOrWhiteSpace(request.OpenDate))
            {
                portfolio.OpenDate = SetOpenDateOnWeekday.SetDate(DateTimeOffset.Now);
                portfolio.BeforeLastUpdate = portfolio.OpenDate.Date;
                portfolio.LastUpdate = portfolio.OpenDate.Date;
            }

            else
            {
                DateTimeOffset tempOpenDate = DateTimeOffset.Parse($"{ request.OpenDate } +2:00"); // Paris offset is +2:00
                portfolio.OpenDate = SetOpenDateOnWeekday.SetDate(tempOpenDate);
                portfolio.BeforeLastUpdate = portfolio.OpenDate.DateTime.Date;
                portfolio.LastUpdate = portfolio.OpenDate.DateTime.Date;
            }

            _context.Portfolio.Add(portfolio);

            await _context.SaveChangesAsync(cancellationToken);

            // Select symbols
            FilterBestStockQuery SelectSymbolsToCreatePortfolioQuery = new FilterBestStockQuery();
            HashSet<string> symbols = await _mediator.Send(SelectSymbolsToCreatePortfolioQuery);

            //download historic
            TimeframeSelector timeframesDownload = new TimeframeSelector(true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,false,false,false);
            DateTimeOffset? openDate = portfolio.OpenDate;

            DownloadStocksQuery downloadStockLastPriceQuery = new DownloadStocksQuery(timeframesDownload, symbols, openDate, false, true);
            await _mediator.Send(downloadStockLastPriceQuery);

            // create positions
            CreatePositionQuery createPositionsQuery = new CreatePositionQuery(portfolio.PortfolioId, symbols, openDate);
            await _mediator.Send(createPositionsQuery);

            portfolio.Investment = _context.Position.Where(position => position.PortfolioId == portfolio.PortfolioId)
                                                    .Sum(position => position.Investment);

            // Initialize portfolio's basic values according to positions like total investement
            UpdatePortfolioQuery updatePortfolioQuery = new UpdatePortfolioQuery(portfolio.PortfolioId, false, 0);
            await _mediator.Send(updatePortfolioQuery);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
