﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Features.Simulator.CreatePortfolios.CreatePortfolio
{
    public class CreatePortfolioQuery : IRequest
    {
        public string OpenDate { get; set; }
        public bool IsItInRealTime { get; set; }
        public CreatePortfolioQuery(string openDate, bool isItInRealTime)
        {
            OpenDate = openDate;
            IsItInRealTime = isItInRealTime;
        }
    }
}
