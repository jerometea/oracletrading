﻿using Application.Common.Helpers;
using Application.Common.Interfaces;
using Application.Common.Interfaces.Files;
using Domain.Entities.Simulator;
using MediatR;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.Stocks.Screenshot
{
    public class ScreenshotGraphicQuery : IRequest { }

    class ScreenshotGraphicHandler : IRequestHandler<ScreenshotGraphicQuery>
    {
        IScreenshotGraphic _screenshotGraphic;
        IVisionTradingDbContext _context;

        public ScreenshotGraphicHandler(IVisionTradingDbContext context, IScreenshotGraphic screenshotGraphic)
        {
            _screenshotGraphic = screenshotGraphic;
            _context = context;
        }

        public Task<Unit> Handle(ScreenshotGraphicQuery request, CancellationToken cancellationToken)
        {
            List<Portfolio> portfolios = _context.Portfolio.Where(p => p.PortfolioId == 180).ToList();
            foreach (Portfolio portfolio in portfolios)
            {

                foreach (Position position in portfolio.Positions)
                {
                    Dictionary<string, string> startDates = new Dictionary<string, string>();

                    string openDate = portfolio.OpenDate.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));

                    string oneDayStartDate = portfolio.OpenDate.AddWorkingDays(-1).ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
                    string fiveDaysStartDate = portfolio.OpenDate.AddWorkingDays(-7).ToString("d", CultureInfo.CreateSpecificCulture("en-US"));

                    string oneMonthStartDate = portfolio.OpenDate.AddMonths(-1).ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
                    string threeMonthsStartDate = portfolio.OpenDate.AddMonths(-3).ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
                    string sixMonthsStartDate = portfolio.OpenDate.AddMonths(-6).ToString("d", CultureInfo.CreateSpecificCulture("en-US"));

                    string oneYearStartDate = portfolio.OpenDate.AddYears(-1).ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
                    string YTDStartDate = new DateTime(portfolio.OpenDate.Year, 1, 1).ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
                    string twoYearsStartDate = portfolio.OpenDate.AddYears(-2).ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
                    string FiveYearsStartDate = portfolio.OpenDate.AddYears(-5).ToString("d", CultureInfo.CreateSpecificCulture("en-US"));

                    string maxStartDate = new DateTime(1971, 01, 01).ToString("d", CultureInfo.CreateSpecificCulture("en-US"));

                    startDates.Add("1 ONE DAY", oneDayStartDate);
                    startDates.Add("2 FIVE DAYS", fiveDaysStartDate);

                    startDates.Add("2 ONE MONTH", oneMonthStartDate);
                    startDates.Add("3 THREE MONTHS", threeMonthsStartDate);
                    startDates.Add("4 SIX MONTHS", sixMonthsStartDate);

                    startDates.Add("5 ONE YEAR", oneYearStartDate);

                    if (!openDate.Contains("1/1"))
                    {
                        startDates.Add("6 YTD", YTDStartDate);
                    }

                    startDates.Add("7 TWO YEARS", twoYearsStartDate);
                    startDates.Add("8 FIVE YEARS", FiveYearsStartDate);
                    startDates.Add("9 MAX", maxStartDate);

                    foreach (var startDate in startDates)
                    {
                        _screenshotGraphic.Capture(position.Stock.Symbol, portfolio.PortfolioId, startDate.Key, startDate.Value, openDate, portfolio.IsInRealTime);
                    }
                }
            }

            return null;
        }
    }
}
