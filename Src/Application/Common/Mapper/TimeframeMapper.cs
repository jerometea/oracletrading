﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Common.Mapper
{
    public class TimeframeMapper
    {
        // Analyze stocks
        public static Timeframe ToTimeframeEntity(string yahooTimeframe, List<Timeframe> timeframes)
        {
            if (yahooTimeframe == "1d")
                return timeframes.Where(p => p.TimeframeName == "OneDay").FirstOrDefault();
            else if (yahooTimeframe == "5d")
                return timeframes.Where(p => p.TimeframeName == "FiveDays").FirstOrDefault();
            else if (yahooTimeframe == "1mo")
                return timeframes.Where(p => p.TimeframeName == "OneMonth").FirstOrDefault();
            else if (yahooTimeframe == "6mo")
                return timeframes.Where(p => p.TimeframeName == "SixMonths").FirstOrDefault();
            else if (yahooTimeframe == "1y")
                return timeframes.Where(p => p.TimeframeName == "OneYear").FirstOrDefault();
            else if (yahooTimeframe == "ytd")
                return timeframes.Where(p => p.TimeframeName == "YTD").FirstOrDefault();
            else if (yahooTimeframe == "5y")
                return timeframes.Where(p => p.TimeframeName == "FiveYears").FirstOrDefault();
            else if (yahooTimeframe == "max")
                return timeframes.Where(p => p.TimeframeName == "Max").FirstOrDefault();

            // Below days do not exist for yahoo api
            else if (yahooTimeframe == "2d")
                return timeframes.Where(p => p.TimeframeName == "TwoDays").FirstOrDefault();
            else if (yahooTimeframe == "3d")
                return timeframes.Where(p => p.TimeframeName == "ThreeDays").FirstOrDefault();
            else if (yahooTimeframe == "4d")
                return timeframes.Where(p => p.TimeframeName == "FourDays").FirstOrDefault();

            else if (yahooTimeframe == "6d")
                return timeframes.Where(p => p.TimeframeName == "SixDays").FirstOrDefault();
            else if (yahooTimeframe == "7d")
                return timeframes.Where(p => p.TimeframeName == "SevenDays").FirstOrDefault();
            else if (yahooTimeframe == "8d")
                return timeframes.Where(p => p.TimeframeName == "EightDays").FirstOrDefault();
            else if (yahooTimeframe == "9d")
                return timeframes.Where(p => p.TimeframeName == "NineDays").FirstOrDefault();
            else if (yahooTimeframe == "10d")
                return timeframes.Where(p => p.TimeframeName == "TenDays").FirstOrDefault();
            else if (yahooTimeframe == "11d")
                return timeframes.Where(p => p.TimeframeName == "ElevenDays").FirstOrDefault();
            else if (yahooTimeframe == "12d")
                return timeframes.Where(p => p.TimeframeName == "TwelveDays").FirstOrDefault();

            else if (yahooTimeframe == "2nd")
                return timeframes.Where(p => p.TimeframeName == "SecondDay").FirstOrDefault();
            else if (yahooTimeframe == "3rd")
                return timeframes.Where(p => p.TimeframeName == "ThirdDay").FirstOrDefault();
            else if (yahooTimeframe == "4th")
                return timeframes.Where(p => p.TimeframeName == "FourthDay").FirstOrDefault();
            else if (yahooTimeframe == "5th")
                return timeframes.Where(p => p.TimeframeName == "FifthDay").FirstOrDefault();

            else
                return null;
        }

        public static string ToYahooTimeframe(string timeframeFromFileName)
        {
            if (timeframeFromFileName == "OneDay") return "1d";
            else if (timeframeFromFileName == "FiveDays") return "5d";
            else if (timeframeFromFileName == "OneMonth") return "1mo";
            else if (timeframeFromFileName == "SixMonths") return "6mo";
            else if (timeframeFromFileName == "OneYear") return "1y";
            else if (timeframeFromFileName == "YTD") return "ytd";
            else if (timeframeFromFileName == "FiveYears") return "5y";
            else if (timeframeFromFileName == "Max") return "max";

            // Below days do not exist for yahoo api
            else if (timeframeFromFileName == "TwoDays") return "2d";
            else if (timeframeFromFileName == "ThreeDays") return "3d";
            else if (timeframeFromFileName == "FourDays") return "4d";

            else if (timeframeFromFileName == "SixDays") return "6d";
            else if (timeframeFromFileName == "SevenDays") return "7d";
            else if (timeframeFromFileName == "EightDays") return "8d";
            else if (timeframeFromFileName == "NineDays") return "9d";
            else if (timeframeFromFileName == "TenDays") return "10d";
            else if (timeframeFromFileName == "ElevenDays") return "11d";
            else if (timeframeFromFileName == "TwelveDays") return "12d";


            else if (timeframeFromFileName == "SecondDay") return "2nd";
            else if (timeframeFromFileName == "ThirdDay") return "3rd";
            else if (timeframeFromFileName == "FourthDay") return "4th";
            else if (timeframeFromFileName == "FifthDay") return "5th";

            else return null;
        }

        public static HashSet<string> ToString(bool OneDay, bool TwoDays, bool ThreeDays, bool FourDays, bool FiveDays, 
                                               bool SixDays, bool SevenDays, bool EightDays, bool NineDays, bool TenDays, bool ElevenDays, bool TwelveDays,
                                               bool OneMonth, bool SixMonths, bool OneYear, bool YTD, bool FiveYears, bool Max,
                                               bool SecondDay, bool ThirdDay, bool FourthDay, bool FifthDay)
        {
            HashSet<string> StringPeriods = new HashSet<string>();

            if (OneDay) StringPeriods.Add("OneDay");

            if (TwoDays) StringPeriods.Add("TwoDays");
            if (ThreeDays) StringPeriods.Add("ThreeDays");
            if (FourDays) StringPeriods.Add("FourDays");

            if (SixDays) StringPeriods.Add("SixDays");
            if (SevenDays) StringPeriods.Add("SevenDays");
            if (EightDays) StringPeriods.Add("EightDays");
            if (NineDays) StringPeriods.Add("NineDays");
            if (TenDays) StringPeriods.Add("TenDays");
            if (ElevenDays) StringPeriods.Add("ElevenDays");
            if (TwelveDays) StringPeriods.Add("TwelveDays");

            if (FiveDays) StringPeriods.Add("FiveDays");
            if (OneMonth) StringPeriods.Add("OneMonth");
            if (SixMonths) StringPeriods.Add("SixMonths");
            if (OneYear) StringPeriods.Add("OneYear");
            if (YTD) StringPeriods.Add("YTD");
            if (FiveYears) StringPeriods.Add("FiveYears");
            if (Max) StringPeriods.Add("Max");

            if (SecondDay) StringPeriods.Add("SecondDay");
            if (ThirdDay) StringPeriods.Add("ThirdDay");
            if (FourthDay) StringPeriods.Add("FourthDay");
            if (FifthDay) StringPeriods.Add("FifthDay");

            return StringPeriods;
        }
    }
}
