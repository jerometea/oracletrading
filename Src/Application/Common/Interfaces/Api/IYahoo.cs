﻿using Application.Common.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IYahoo
    {
        public Task<Unit> DownloadStockHistoric(HashSet<string> symbols, TimeframeSelector timeframes, DateTimeOffset? frenchOpenDate, bool isItForPortfolioExport);
    }
}
