﻿using Domain.Entities;
using Domain.Entities.Simulator;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IVisionTradingDbContext
    {
        public DatabaseFacade Database { get; }
        public Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        public int SaveChanges();

        public DbSet<Category> Category { get; set; }
        public DbSet<Currency> Currency { get; set; }
        public DbSet<Exchange> Exchange { get; set; }
        public DbSet<Timeframe> Timeframe { get; set; }
        public DbSet<Provider> Provider { get; set; }
        public DbSet<Regression> Regression { get; set; }
        public DbSet<RegressionSubtype> RegressionSubtype { get; set; }
        public DbSet<Statistic> Statistic { get; set; }
        public DbSet<StatisticPosition> StatisticPosition { get; set; }
        public DbSet<Stock> Stock { get; set; }
        public DbSet<Timezone> Timezone { get; set; }
        public DbSet<Position> Position { get; set; }
        public DbSet<Portfolio> Portfolio { get; set; }
        public DbSet<Change> Change { get; set; }    
    }
}
