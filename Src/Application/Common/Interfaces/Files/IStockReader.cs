﻿using Application.Common.Helpers;
using Application.Common.Models.Yahoo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IStockReader
    {
        public List<YahooTimeframeModel> OpenJson(TimeframeSelector timeframeSelector, HashSet<string> symbolsToRead);
    }
}
