﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Common.Interfaces.Files
{
    public interface IExportPortfolio
    {
        void ExportReport(int portfolioId, SortedDictionary<string, List<string>> portfolioGains, bool isItRealTimePortfolio, int sheet);
    }
}
