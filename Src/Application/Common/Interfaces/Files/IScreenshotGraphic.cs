﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Common.Interfaces.Files
{
    public interface IScreenshotGraphic
    {
        public void Capture(string symbol, int portfolioId, string timeframe, string startDate, string openDate, bool isItRealTimePortfolio);
    }
}
