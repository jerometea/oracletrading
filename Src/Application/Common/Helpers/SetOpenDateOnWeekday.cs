﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Common.Helpers
{
    public static class SetOpenDateOnWeekday
    {
        public static DateTimeOffset SetDate(DateTimeOffset openDate)
        {
            if (openDate.DayOfWeek == DayOfWeek.Saturday) return openDate.AddDays(-1);
            else if (openDate.DayOfWeek == DayOfWeek.Sunday) return openDate.AddDays(-2);
            else return openDate;
        }
    }
}
