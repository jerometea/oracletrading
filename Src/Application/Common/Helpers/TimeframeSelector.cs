﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using Application.Common.Mapper;
using System.ComponentModel;

namespace Application.Common.Helpers
{
    public class TimeframeSelector
    {
        public HashSet<string> TimeframesString { get; set; }
        public bool HasOneDay { get; set; }
        public bool HasFiveDays { get; set; }
        public bool HasOneMonth { get; set; }
        public bool HasSixMonths { get; set; }
        public bool HasOneYear { get; set; }
        public bool HasYTD { get; set; }
        public bool HasFiveYears { get; set; }
        public bool HasMax { get; set; }

        public bool HasTwoDays { get; set; }
        public bool HasThreeDays { get; set; }
        public bool HasFourDays { get; set; }
        public bool HasSixDays { get; set; }
        public bool HasSevenDays { get; set; }
        public bool HasEightDays { get; set; }
        public bool HasNineDays { get; set; }
        public bool HasTenDays { get; set; }
        public bool HasElevenDays { get; set; }
        public bool HasTwelveDays { get; set; }

        public bool HasSecondDay { get; }
        public bool HasThirdDay { get; }
        public bool HasFourthDay { get; }
        public bool HasFifthDay { get; }

        public TimeframeSelector(bool hasOneDay, bool hasTwoDays, bool hasThreeDays, bool hasFourDays, bool hasFiveDays,
                                 bool hasSixDays, bool hasSevenDays, bool hasEightDays, bool hasNineDays, bool hasTenDays, bool hasElevenDays, bool hasTwelveDays,
                                 bool hasOneMonth, bool hasSixMonths, bool hasOneYear, bool hasYtd, bool hasFiveYears, bool hasMax,
                                 bool hasSecondDay,bool hasThirdDay, bool hasFourthDay, bool hasFifthDay)
        {
            TimeframesString = TimeframeMapper.ToString(hasOneDay, hasTwoDays, hasThreeDays, hasFourDays, hasFiveDays,
                                                        hasSixDays, hasSevenDays, hasEightDays, hasNineDays, hasTenDays, hasElevenDays, hasTwelveDays,
                                                        hasOneMonth, hasSixMonths, hasOneYear, hasYtd, hasFiveYears, hasMax,
                                                        hasSecondDay, hasThirdDay, hasFourthDay, hasFifthDay);

            HasOneDay = hasOneDay;
            HasFiveDays = hasFiveDays;
            HasOneMonth = hasOneMonth;
            HasSixMonths = hasSixMonths;
            HasOneYear = hasOneYear;
            HasYTD = hasYtd;
            HasFiveYears = hasFiveYears;
            HasMax = hasMax;

            HasTwoDays = hasTwoDays;
            HasThreeDays = hasThreeDays;
            HasFourDays = hasFourDays;

            HasSixDays =   hasSixDays;
            HasSevenDays = hasSevenDays;
            HasEightDays =  hasEightDays;
            HasNineDays =   hasNineDays;
            HasTenDays =    hasTenDays;
            HasElevenDays = hasElevenDays;
            HasTwelveDays = hasTwelveDays;

            HasSecondDay = hasSecondDay;
            HasThirdDay = hasThirdDay;
            HasFourthDay = hasFourthDay;
            HasFifthDay = hasFifthDay;
        }
    }
}
