﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Common.Helpers
{
    public static class DateTimeOffsetExtension
    {
        public static DateTimeOffset AddWorkingDays(this DateTimeOffset openDate, int days)
        {
            if (days > 0) throw new ArgumentOutOfRangeException("days must be a negative number");

            for (int i = 0; i >= days; i--)
            {
                DateTimeOffset iterateDay = openDate.AddDays(i);
                
                if(iterateDay.DayOfWeek == DayOfWeek.Sunday)
                {
                    days-=2;
                }
            }

            return openDate.AddDays(days);
        }
    }
}
