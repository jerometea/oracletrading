﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Common.Helpers
{
    public static class RemoveNullsFromBuggyHistoric
    {
        /// <summary>
        /// Clean buggy historic from yahoo. 
        /// For 2/3/4 days and 1 month timeframe yahoo sends price historic with nulls and last value is the current price 
        /// like that { 121, 122, 121, 123, null, null, null, null, null, null, null, null, 715 }
        /// </summary>
        /// <param name="buggyHistoricWithNullsOccurence"> 2/3/4 timeframe buggy historic</param>
        /// <returns></returns>
        public static List<double?> RemoveNulls(List<double?> buggyHistoricWithNullsOccurence)
        {
            List<double?> historicWithoutNull = new List<double?>();

            buggyHistoricWithNullsOccurence.Reverse();

            int firstNonUllPricePosition = 0;
            for(int i = 1; i < buggyHistoricWithNullsOccurence.Count; i++)
            {
                if (buggyHistoricWithNullsOccurence[i].HasValue)
                {
                    firstNonUllPricePosition = i;
                    break;
                }
            }

            for(int i = firstNonUllPricePosition; i < buggyHistoricWithNullsOccurence.Count; i++)
            {
                historicWithoutNull.Add(buggyHistoricWithNullsOccurence[i]);
            }

            historicWithoutNull.Reverse();

            return historicWithoutNull;

        }
    }
}
