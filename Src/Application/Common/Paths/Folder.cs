﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.PathConfigurations
{
    public class Folder
    {
        public string Regressions { get; set; }
        public string Json { get; set; }
        public string Portfolios { get; set; }
    }
}
