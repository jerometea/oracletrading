﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Models.Yahoo
{
    public class YahooResearchModel
    {
        public string Symbol { get; set; }
        public string Exchange { get; set; }
        public string Shortname { get; set; }
        public string Longname { get; set; }
        public string ProviderStockName { get; set; }
        public string Providers { get; set; }
        public string QuoteType { get; set; }
        public string Index { get; set; }
        public float Score { get; set; }
        public string TypeDisp { get; set; }
        public bool isYahooFinance { get; set; }

    }

}
