using Application.Common.Interfaces;
using Application.Common.Interfaces.Files;
using Application.Common.PathConfigurations;
using Application.Common.UserSecrets;
using Application.Interfaces;
using AutoMapper;
using Domain;
using Domain.Statistics;
using Infrastructure.Api.Fixer.GetEuroRates;
using Infrastructure.Files;
using Infrastructure.Files.Portfolios;
using Infrastructure.Imports.GuruFocus;
using Infrastructure.Persistence;
using Infrastructure.Providers;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Serialization;
using System;
using System.Reflection;

namespace OracleTrading
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region load assemblies for MediatR
            var applicationAssembly = Assembly.Load("Application");
            var domainAssembly = Assembly.Load("Domain");
            services.AddMediatR(applicationAssembly, domainAssembly);
            #endregion

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddScoped(typeof(IExchangeTimezoneReader), typeof(TimezoneExchangeReader));
            services.AddScoped(typeof(IGuruFocus), typeof(GurufocusScript));
            services.AddScoped(typeof(IYahoo), typeof(HistoricDownloader));
            services.AddScoped(typeof(IFixer), typeof(GetEuroRatesApi));

            services.AddScoped(typeof(IRegressionSubtypeReader), typeof(RegressionSubtypeReader));

            services.AddScoped<RegressionFit>();

            services.AddScoped(typeof(IVisionTradingDbContext), typeof(VisionTradingDbContext));

            services.AddDbContext<VisionTradingDbContext>(options => options.UseSqlServer((Configuration.GetConnectionString("VisionTradingConnection")))
            .UseLazyLoadingProxies());

            services.AddControllers().AddNewtonsoftJson(s => s.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver());

            services.Configure<Folder>(Configuration.GetSection("Folder"));
            services.Configure<UserSecret>(Configuration.GetSection("UserSecret"));

            services.AddScoped(typeof(IStockReader), typeof(StockReader));
            services.AddScoped(typeof(IScreenshotGraphic), typeof(ScreenshotGraphic));
            services.AddScoped(typeof(IExportPortfolio), typeof(ExportPortfolio));

            services.AddSwaggerGen();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/error");
            }

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
