﻿using Application.Common.Helpers;
using Application.Features.Stocks;
using Application.Features.Stocks.AnalyzeStocks;
using Application.Features.Stocks.DownloadStocks;
using Application.Features.Stocks.SaveStocks;
using Application.Features.Stocks.Screenshot;
using Application.Features.Stocks.UpdateStockPrice;
using AutoMapper;
using Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Presentation.Controllers
{
    [Route("api/stocks")]
    [ApiController]
    public class StockController : ControllerBase
    {
        IMediator _mediator;

        public StockController(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator;
        }

        [HttpGet("save")]
        public async Task<ActionResult> SaveStocks()
        {
            SaveStockQuery query = new SaveStockQuery(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false);
            await _mediator.Send(query);
            return Ok("Finish");
        }


        [HttpGet("update_price")]
        public async Task<ActionResult> UpdateStockPrice()
        {
            UpdateStockPriceQuery query = new UpdateStockPriceQuery();
            _mediator.Send(query);
            return Ok("Finish");
        }

        [HttpGet("analyze/{Days}/{OneMonth}/{SixMonths}/{OneYear}/{FiveYears}/{Max}/{hasFilterBestStocks}")]
        public async Task<ActionResult> AnalyzeStocks(bool Days, bool OneMonth, bool SixMonths, bool OneYear, bool FiveYears, bool Max, 
                                                      bool SecondDay, bool ThirdDay, bool FourthDay, bool FifthDay, bool hasFilterBestStocks)
        {
            TimeframeSelector timeframes = new TimeframeSelector(Days, Days, Days, Days, Days, Days, Days, Days, Days, Days, Days, Days, OneMonth, SixMonths || Days, OneYear, false, FiveYears, Max, false, false, false, false);
            await _mediator.Send(new AnalyzeStockQuery(timeframes, hasFilterBestStocks));
            return Ok("Finish");
        }

        [HttpGet("download/{Days}/{OneMonth}/{SixMonths}/{OneYear}/{FiveYears}/{Max}/{hasFilterBestStocks}/{openDate}")]
        public async Task<ActionResult> DownloadStocks(bool Days, bool OneMonth, bool SixMonths, bool OneYear, bool FiveYears, bool Max, bool hasFilterBestStocks, string openDate)
        {
            DateTimeOffset? openDateSetOnWeekday = null;

            if (!string.IsNullOrWhiteSpace(openDate))
            {
                DateTimeOffset? openDateNonSetOnWeekday = DateTimeOffset.Parse($"{ openDate } +2:00"); // Paris offset is +2:00
                openDateSetOnWeekday = SetOpenDateOnWeekday.SetDate(openDateNonSetOnWeekday.Value);
            }

            TimeframeSelector timeframes = new TimeframeSelector(Days, Days, Days, Days, Days, Days, Days, Days, Days, Days, Days, Days, OneMonth, SixMonths, OneYear, false, FiveYears, Max, false, false, false, false);

            DownloadStocksQuery downloadQuery = new DownloadStocksQuery(timeframes, null, openDateSetOnWeekday, hasFilterBestStocks, false);

            await _mediator.Send(downloadQuery);

            return Ok("finish");
        }
    }
}
