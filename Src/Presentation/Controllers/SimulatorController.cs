﻿using Application.Features.Simulator.CreatePortfolios.CreatePortfolio;
using Application.Features.Simulator.ExportPortfolios;
using Application.Features.Simulator.ExportPortfolios.ExportOptions;
using Application.Features.Simulator.UpdatePortfolios.UpdatePortfolio;
using Application.Features.Stocks.FilterBestStocks;
using Application.Features.Stocks.Screenshot;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SimulatorController : ControllerBase
    {
        IMediator _mediator;

        public SimulatorController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("create/{openDate}/{isItInRealTime}")]
        public async Task<ActionResult> CreatePortfolio(string openDate, bool isItInRealTime)
        {
            CreatePortfolioQuery createPortfolioQuery = new CreatePortfolioQuery(openDate, isItInRealTime);

            await _mediator.Send(createPortfolioQuery);

            return Ok("finish");
        }

        [HttpGet("export/{portfolioId}")]
        public async Task<ActionResult> ExportPortfolio(int portfolioId)
        {
            ExportOptionsQuery exportOptionQuery = new ExportOptionsQuery(false, false, portfolioId);
            await _mediator.Send(exportOptionQuery);
            return Ok("finish");
        }

        [HttpGet("filter_stock")]
        public async Task<ActionResult> FilterBestStock()
        {
            FilterBestStockQuery query = new FilterBestStockQuery();
            HashSet<string> symbols = await _mediator.Send(query);
            return Ok(symbols);
        }

        [HttpGet("screenshot")]
        public async Task<ActionResult> ScreenshotGraphic()
        {
            ScreenshotGraphicQuery query = new ScreenshotGraphicQuery();
            _mediator.Send(query);
            return Ok("Finish");
        }
    }
}
