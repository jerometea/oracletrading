﻿using Application.Features.RegressionSubtypes.SaveRegressionSubtypes;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegressionSubtypesController : ControllerBase
    {
        IMediator _mediator;

        public RegressionSubtypesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("save")]
        public async Task<ActionResult> SaveRegressionSubtype()
        {
            await _mediator.Send(new SaveRegressionSubtypeQuery());
            return Ok("finish");
        }
    }
}
