﻿using Application.Features.Currencies.UpdatesCurrencies;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrenciesController : ControllerBase
    {
        IMediator _mediator;

        public CurrenciesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("update")]
        public async Task<ActionResult> UpdateEuroRates()
        {
            await _mediator.Send(new UpdateEuroRatesQuery());
            return Ok("finish");
        }
    }
}
