﻿using Application.Scripts;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TimezoneExchangeController : ControllerBase
    {
        private IMediator _mediator;

        public TimezoneExchangeController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet()]
        public async Task<ActionResult> SaveTimezoneExchange()
        {
            await _mediator.Send(new SaveTimezoneExchangeQuery());
            return Ok("finish");
        }
    }
}
