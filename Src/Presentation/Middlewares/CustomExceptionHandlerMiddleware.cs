﻿using Application.Common.Exceptions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;

namespace WebApi.Middlewares
{
    public class CustomExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        public CustomExceptionHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                // Call the next delegate/middleware in the pipeline
                await _next(context);
            }

            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            HttpStatusCode errorStatusCode;
            string errorMessage;

            switch (exception)
            {
                case NotFoundException notFoundException:
                    errorStatusCode = HttpStatusCode.NotFound;
                    errorMessage = notFoundException.Message;
                    break;

                case DbUpdateException duplicateKeyException when ((SqlException) duplicateKeyException.InnerException).Number == 2601 :
                    errorStatusCode = HttpStatusCode.Conflict;
                    errorMessage = duplicateKeyException.InnerException.ToString();
                    break;

                default :
                    errorStatusCode = HttpStatusCode.InternalServerError;
                    errorMessage = exception.Message.ToString();
                    break;
            }

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int) errorStatusCode;

            return context.Response.WriteAsync(errorMessage);
        }
    }

    public static class CustomExceptionHandlerMiddlewareExtensions
    {
        public static IApplicationBuilder UseCustomExceptionHandler(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CustomExceptionHandlerMiddleware>();
        }
    }
}
