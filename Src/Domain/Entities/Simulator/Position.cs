﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Simulator
{
    public class Position : AuditableEntity
    {
        public Position()
        {
            StatisticsArchives = new HashSet<StatisticPosition>();
        }

        public int PositionId { get; set; }
        public int? StockId { get; set; }
        public int? PortfolioId { get; set; }
        public int Quantity { get; set; }

        public double PurchasePrice { get; set; }
        public double CurrentPrice { get; set; }
        public double Investment { get; set; }
        public double TodayChange { get; set; }
        public double TodayChangePercentage { get; set; }
        public double TotalValue { get; set; }

        public double Gain { get; set; }
        public double GainPercentage { get; set; }

        public double PurchasePriceInForeignCurrency { get; set; }
        public double CurrentPriceInForeignCurrency { get; set; }

        public int Days { get; set; }

        public DateTimeOffset ForeignOpenDate { get; set; }
        public DateTimeOffset ForeignLastUpdateDate { get; set; }
        public DateTimeOffset? ForeignCloseDate { get; set; }

        public DateTime BeforeLastUpdate { get; set; }
        public double BeforeLastTotalValue { get; set; }

        public virtual ICollection<StatisticPosition> StatisticsArchives { get; set; }
        public virtual Stock Stock { get; set; }
        public virtual Portfolio Portfolio { get; set; }
    }
}



