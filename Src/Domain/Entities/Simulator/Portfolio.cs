﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Simulator
{
    public class Portfolio : AuditableEntity
    {
        public Portfolio()
        {
            Positions = new List<Position>();
        }

        public int PortfolioId { get; set; }
        public string Comment { get; set; }

        public DateTimeOffset OpenDate { get; set; }
        public double TodayChange { get; set; }
        public double TodayChangePercentage { get; set; }
        public double Gain { get; set; }
        public double GainPercentage { get; set; }
        public double Investment { get; set; }
        public double TotalValue { get; set; }
        public int Days { get; set; }
        public bool IsInRealTime { get; set; }

        public DateTime LastUpdate { get; set; }
        public DateTime BeforeLastUpdate { get; set; }
        public double BeforeLastTotalValue { get; set; }

        public int PositionsBough { get; set; }
        public double PositionsPositive { get; set; }
        public int PositionsNeutral { get; set; }
        public int PositionsNegative { get; set; }
        public double SuccessRate { get; set; }

        public virtual List<Position> Positions { get; set; }
    }
}
