﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
    public class RegressionSubtype
    {
        public RegressionSubtype()
        {
            Statistics = new HashSet<Statistic>();
            Prices = new List<double>();
        }

        public int RegressionSubtypeId { get; set; }
        public string RegressionSubtypeName { get; set; }

        public int? FirstY { get; set; }
        public int? LastY { get; set; }
        public int? RegressionId { get; set; }
        public string Symbol { get; set; }
        public int Count { get; set; }

        [NotMapped]
        public List<double> Prices { get; set; }

        public virtual Regression Regression { get; set; }
        public virtual ICollection<Statistic> Statistics { get; set; }
    }
}