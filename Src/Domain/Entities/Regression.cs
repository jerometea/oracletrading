﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Regression
    {
        public Regression()
        {
            RegressionSubtypes = new HashSet<RegressionSubtype>();
        }

        public int RegressionId { get; set; }
        public string RegressionName { get; set; }
        public bool HasPositiveCoefficient { get; set; }
        public virtual ICollection<RegressionSubtype> RegressionSubtypes { get; set; }
    }
}
