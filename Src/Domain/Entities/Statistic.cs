﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
    public class Statistic : AuditableEntity
    {

        public Statistic()
        {
            Changes = new List<Change>();
        }

        public int StatisticId { get; set; }
        public int? StockId { get; set; }
        public int? RegressionSubtypeId { get; set; }
        public double Score { get; set; }
        public double Integrity { get; set; }
        public int TotalValues { get; set; }
        public int? TimeframeId { get; set; }
        public double Change { get; set; }

        public virtual List<Change> Changes { get; set; }
        public virtual Stock Stock { get; set; }
        public virtual RegressionSubtype RegressionSubtype { get; set; }
        public virtual Timeframe Timeframe { get; set; }
    }
}
