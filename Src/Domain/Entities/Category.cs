﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Category
    {
        public Category()
        {
            Stocks = new HashSet<Stock>();
        }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public virtual ICollection<Stock> Stocks { get; set; }
    }
}
