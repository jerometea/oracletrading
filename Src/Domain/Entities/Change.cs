﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Change
    {
        public int ChangeId { get; set; }
        public double Percent { get; set; }
        public int Range { get; set; }
        public int HistoricStartRangeIndex { get; set; }
        public int HistoricEndRangeIndex { get; set; }
        public int? StatisticId { get; set; }
        public virtual Statistic Statistic { get; set; }
        public double PeakPrice { get; set; }
        public double EndRangePrice { get; set; }
    }
}
