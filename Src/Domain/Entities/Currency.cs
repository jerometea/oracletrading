﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Currency : AuditableEntity
    {
        public Currency()
        {
            Stocks = new HashSet<Stock>();
        }

        public int CurrencyId { get; set; }
        public string CurrencyShortName { get; set; }
        public string CurrencyName { get; set; }
        public double EuroRate { get; set; }
        public virtual ICollection<Stock> Stocks { get; set; }
    }
}
