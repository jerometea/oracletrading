﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Provider
    {
        public Provider()
        {
            Stocks = new HashSet<Stock>();
        }

        public int ProviderId { get; set; }
        public string ProviderName { get; set; }

        public virtual  ICollection<Stock> Stocks { get; set; }
    }
}
