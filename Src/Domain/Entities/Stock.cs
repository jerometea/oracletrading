﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Stock : AuditableEntity
    {
        public Stock()
        {
            Statistics = new HashSet<Statistic>();
        }

        public int StockId { get; set; }
        public string Symbol { get; set; }
        public string StockShortName { get; set; }
        public int? ExchangeId { get; set; }
        public int? CurrencyId { get; set; }
        public int Volume { get; set; }
        public int? ProviderId { get; set; }
        public string StockName { get; set; }
        public int? CategoryId { get; set; }
        public double LastPrice { get; set; }
        public double LastPriceInForeignCurrency{ get; set; }
        public DateTimeOffset? Birthdate { get; set; }

        // Foreign key in db
        public virtual Exchange Exchange { get; set; }
        public virtual Category Category { get; set; }
        public virtual Currency Currency { get; set; }
        public virtual Provider Provider { get; set; }
                
        public virtual ICollection<Statistic> Statistics { get; set; }
    }
}