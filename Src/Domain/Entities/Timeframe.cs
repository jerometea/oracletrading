﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Timeframe
    {
        public Timeframe()
        {
            Statistics = new HashSet<Statistic>();
        }

        public int TimeframeId { get; set; }
        public string TimeframeName { get; set; }
        public virtual ICollection<Statistic> Statistics { get; set; }
    }
}
