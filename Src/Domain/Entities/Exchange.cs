﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Exchange
    {
        public Exchange()
        {
            Stocks = new HashSet<Stock>();
        }

        public int ExchangeId { get; set; }
        public string ExchangeName { get; set; }
        public string ExchangeShortName { get; set; }
        public int? TimezoneId { get; set; }

        public virtual  ICollection<Stock> Stocks { get; set; }
        public virtual Timezone Timezone { get; set; }
    }
}
