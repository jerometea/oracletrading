﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
    public class Timezone
    {
        public Timezone()
        {
            Exchanges = new HashSet<Exchange>();
        }

        public int TimezoneId { get; set; }
        public string TimezoneName { get; set; }

        public virtual ICollection<Exchange> Exchanges { get; set; }
    }
}
