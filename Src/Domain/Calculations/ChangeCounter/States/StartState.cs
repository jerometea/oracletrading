﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Calculations.ChangeCounter.States
{
    public class StartState : CounterState
    {
        public override CounterState SelectWhichPeakItIsState(ChangeCounter changeCounter, int peakIndex)
        {
            return changeCounter.Peaks[peakIndex].isMaxima ? new FromMaximaState() : new FromMinimaState();
        }
    }
}
