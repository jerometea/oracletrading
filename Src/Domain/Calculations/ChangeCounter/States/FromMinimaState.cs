﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Calculations.ChangeCounter.States
{
    public class FromMinimaState : CounterState
    {
        public override bool IsNextPeak(double changePercent)
        {
            return changePercent <= 2.5;
        }

        public override bool IsHigherChangeThanExistingChange(double changePercentToEvaluate, Change highestExistingChange)
        {
            return changePercentToEvaluate > highestExistingChange.Percent;
        }

        public override bool IsPriceGreaterThanPeak(double peakPrice, double price)
        {
            return price < peakPrice;
        }
    }
}
