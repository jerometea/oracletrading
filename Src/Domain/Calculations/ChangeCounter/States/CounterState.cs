﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Calculations.ChangeCounter.States
{
    public abstract class CounterState
    {
        public virtual CounterState SelectWhichPeakItIsState(ChangeCounter changeCounter, int peakIndex) { throw new NotImplementedException(); }
        public virtual bool IsNextPeak(double changePercent) { throw new NotImplementedException(); }
        public virtual bool IsHigherChangeThanExistingChange(double changePercentToEvaluate, Change highestExistingChange) { throw new NotImplementedException(); }
        public virtual bool IsPriceGreaterThanPeak(double peakPrice, double price) { throw new NotImplementedException(); }
    }
}
