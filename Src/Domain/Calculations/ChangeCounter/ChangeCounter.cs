﻿using Domain.Calculations.ChangeCounter.States;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Calculations.ChangeCounter
{
    public class ChangeCounter
    {
        CounterState _currentState;

        public List<(int HistoricIndex, bool isMaxima)> Peaks { get; set; }
        public List<double> Historic { get; }

        public ChangeCounter(List<double> historic, List<(int minimaMaximaHistoricIndex, bool isMaxima)> peaks)
        {
            _currentState = new StartState();
            Historic = historic;
            Peaks = peaks;
        }

        public List<Change> CountChanges()
        {
            List<Change> HighestChangesPercent = new List<Change>();

            for (int i = 0; i < Peaks.Count - 1; i++)
            {
                _currentState = new StartState();

                _currentState = _currentState.SelectWhichPeakItIsState(this, i);

                double peakPrice = Historic[Peaks[i].HistoricIndex];

                if (peakPrice == 0) continue;

                for (int rangeDay = 1; rangeDay <= 15; rangeDay++)
                {
                    bool hasReachedEndOfHistoric = Peaks[i].HistoricIndex + rangeDay >= Historic.IndexOf(Historic.Last()) ? true : false;

                    if (hasReachedEndOfHistoric) break;

                    double endPrice = Historic[Peaks[i].HistoricIndex + rangeDay];

                    bool isPriceGreaterThanPeak = _currentState.IsPriceGreaterThanPeak(peakPrice, endPrice);

                    if (isPriceGreaterThanPeak) break;

                    double changePercent = ChangePercent.Calculate(peakPrice, endPrice);

                    if (Math.Abs(changePercent) >= 3.5)
                    {
                        Change highestChangeForThisPeak = HighestChangesPercent.FirstOrDefault(peak => peak.HistoricStartRangeIndex == Peaks[i].HistoricIndex && peak.Range <= 5);

                        if (highestChangeForThisPeak is not null && rangeDay <= 5)
                        {
                            bool isHigherChangeThanExistingChange = _currentState.IsHigherChangeThanExistingChange(changePercent, highestChangeForThisPeak);

                            if (isHigherChangeThanExistingChange)
                            {
                                HighestChangesPercent.Remove(highestChangeForThisPeak);
                            }
                            else
                            {
                                continue;
                            }
                        }

                        if (rangeDay > 5)
                        {
                            Change lastHighestChange = null;

                            Change a = HighestChangesPercent.FirstOrDefault(peak => peak.HistoricStartRangeIndex == Peaks[i].HistoricIndex && peak.Range > 5);

                            if (a is not null && rangeDay > 5)
                            {
                                bool isHigherChangeThanExistingChange = _currentState.IsHigherChangeThanExistingChange(changePercent, a);

                                if (isHigherChangeThanExistingChange)
                                {
                                    HighestChangesPercent.Remove(a);
                                }
                                else
                                {
                                    continue;
                                }
                            }

                            if (HighestChangesPercent.Count > 0)
                            {
                                lastHighestChange = HighestChangesPercent.FirstOrDefault(change => change.Range > 5 && change.HistoricEndRangeIndex == Peaks[i].HistoricIndex + rangeDay);
                            }
                            if (lastHighestChange is not null)
                            {
                                bool isHigherChangeThanExistingChange = _currentState.IsHigherChangeThanExistingChange(changePercent, lastHighestChange);

                                if (isHigherChangeThanExistingChange)
                                {
                                    HighestChangesPercent.Remove(lastHighestChange);
                                }
                                else
                                {
                                    break;
                                }
                            }

                            if (highestChangeForThisPeak is not null)
                            {
                                bool isCHangeHigherThanShortRangeChange = _currentState.IsHigherChangeThanExistingChange(changePercent, highestChangeForThisPeak);
                                if (!isCHangeHigherThanShortRangeChange)
                                {
                                    continue;
                                }
                            }
                        }

                        Change change = new Change();
                        change.HistoricStartRangeIndex = Peaks[i].HistoricIndex;
                        change.HistoricEndRangeIndex = Peaks[i].HistoricIndex + rangeDay;
                        change.Percent = changePercent;
                        change.Range = rangeDay;
                        change.PeakPrice = Math.Round(peakPrice, 2);
                        change.EndRangePrice = Math.Round(endPrice, 2);
                        HighestChangesPercent.Add(change);
                    }

                    (int peakPosition, bool isMaxima)? isNextPeak = Peaks.FirstOrDefault(peak => peak.HistoricIndex == Peaks[i].HistoricIndex + rangeDay);

                    bool hasFoundNextlPeak = false;

                    if (isNextPeak != (0, false) && isNextPeak.Value.isMaxima == Peaks[i].isMaxima)
                    {
                        hasFoundNextlPeak = _currentState.IsNextPeak(changePercent);
                    }

                    if (hasFoundNextlPeak)
                    {
                        break;
                    }
                }
            }
            return HighestChangesPercent;
        }
    }
}
