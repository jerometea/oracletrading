﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Statistics
{
    public class HistoricFill
    {
        static int    x1_ValidPrice = -1,
                      x2_ValidPrice = -1;

        static double y1_ValidPrice = -1,
                      y2_ValidPrice = -1;

        public static List<double> CompleteInvalidHistoric(List<double?> invalidHistoric)
        {
            bool isFirstPriceInvalid = invalidHistoric[0] == null ? true : false;
            bool isLastPriceInvalid = invalidHistoric[invalidHistoric.Count - 1] == null ? true : false;

            int beginInvalidPriceRange = -1;
            int endInvalidPriceRange = -1;

            // complete invalid prices at end of historic
            if (isLastPriceInvalid)
            {
                for (int i = invalidHistoric.Count - 1; i > 0; i--)
                {
                    if (invalidHistoric[i] != null && y2_ValidPrice > -1) // 2 : find 2nd valid price
                    {
                        x1_ValidPrice = i;
                        y1_ValidPrice = invalidHistoric[i].Value;
                        break;
                    }
                    if (invalidHistoric[i] != null) // 1 : find 1st valid price
                    {
                        x2_ValidPrice = i;
                        y2_ValidPrice = invalidHistoric[i].Value;
                    }
                }

                for (int x_InvalidPrice = x2_ValidPrice + 1; x_InvalidPrice < invalidHistoric.Count; x_InvalidPrice++)
                {
                    invalidHistoric[x_InvalidPrice] = FindPrice(x_InvalidPrice);
                }

                x1_ValidPrice = -1;
                x2_ValidPrice = -1;
                y1_ValidPrice = -1;
                y2_ValidPrice = -1;
            }
            #region Complete invalid prices
            // 0 complete beginning of historic if start with invalid price
            // 1 begin invalid price range 
            // 2 end invalid price range
            // 3 complete invalid price range
            #endregion
            for (int i = 0; i < invalidHistoric.Count; i++)
            {
                if (i == 0 && isFirstPriceInvalid) // 0
                {
                    beginInvalidPriceRange = i;
                }
                else if (beginInvalidPriceRange > -1 && x1_ValidPrice == -1 && isFirstPriceInvalid && invalidHistoric[i] != null && x1_ValidPrice > -1) // 0 : find 2nd valid price from the end
                {
                    x2_ValidPrice = i;
                    y2_ValidPrice = invalidHistoric[i].Value;
                }
                else if (beginInvalidPriceRange > -1 && x1_ValidPrice == -1 && isFirstPriceInvalid && invalidHistoric[i] != null) // 0 : find 1st valid price from the end
                {
                    x1_ValidPrice = i;
                    y1_ValidPrice = invalidHistoric[i].Value;
                }

                else if (invalidHistoric[i] == null && beginInvalidPriceRange == -1) // 1
                {
                    beginInvalidPriceRange = i;
                    x1_ValidPrice = i - 1;
                    y1_ValidPrice = invalidHistoric[i - 1].Value;
                }

                else if (invalidHistoric[i] != null && beginInvalidPriceRange > -1) // 2
                {
                    endInvalidPriceRange = i;
                    x2_ValidPrice = i;
                    y2_ValidPrice = invalidHistoric[i].Value;
                }
                if (beginInvalidPriceRange > -1 && endInvalidPriceRange > -1) // 3
                {
                    for (int j = beginInvalidPriceRange; j < endInvalidPriceRange; j++)
                    {
                        invalidHistoric[j] = FindPrice(j);
                    }
                    beginInvalidPriceRange = -1;
                    endInvalidPriceRange = -1;
                    x1_ValidPrice = -1;
                    y1_ValidPrice = -1;
                    x2_ValidPrice = -1;
                    y2_ValidPrice = -1;
                }
            }

            var validHistoric = invalidHistoric.Where(x => x != null).Select(x => x.Value).ToList();

            return validHistoric;
        }

        static double FindPrice(int x)
        {
            double slope = (y2_ValidPrice - y1_ValidPrice) / (x2_ValidPrice - x1_ValidPrice);
            double b = y1_ValidPrice - x1_ValidPrice * slope;
            double y = slope * x + b;
            return y;
        }
    }
}
