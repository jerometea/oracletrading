﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;
using Domain.Entities;
using Domain.Calculations;

namespace Domain.Statistics
{
    public class RegressionFit
    {
        public List<RegressionSubtype> RegressionSubtypes { get; }

        public RegressionFit(IRegressionSubtypeReader regressionSubtypesRegister)
        {
            RegressionSubtypes = regressionSubtypesRegister.OpenRegressionSubtypes(false);
        }

        public Statistic Analyse(List<double> prices)
        {
            List<double> historicPercent = ConvertPriceHistoricToPercentage(prices);
            Statistic lowestScore = GetLowestScore(historicPercent);
            return lowestScore;
        }

        List<double> ConvertPriceHistoricToPercentage(List<double> priceHistoric)
        {
            double highestPrice = priceHistoric.Max();
            List<double> percentPriceHistoric = new List<double>();

            foreach (double price in priceHistoric)
            {
                double percentage = price * 100 / highestPrice;
                percentPriceHistoric.Add(percentage);
            }
            return percentPriceHistoric;
        }

        Statistic GetLowestScore(List<double> historicPercent)
        {
            RegressionSubtype bestRegressionSubtype = new RegressionSubtype() { RegressionSubtypeName = "n/a" };

            Statistic bestStatistic = new Statistic() { Score = 100, RegressionSubtype = bestRegressionSubtype };

            foreach (RegressionSubtype regressionSubtype in RegressionSubtypes)
            {
                int distance = Distance.Calculate(regressionSubtype.Prices.Count, historicPercent.Count);
                if (distance <= 5)
                {
                    double score = CalculateScore(regressionSubtype.Prices, historicPercent);
                    if (score < bestStatistic.Score)
                    {
                        bestStatistic.Score = (int) score;
                        bestStatistic.RegressionSubtype.RegressionSubtypeName = regressionSubtype.RegressionSubtypeName;
                    }
                }
            }
            return bestStatistic;
        }

        double CalculateScore(List<double> regressionSubtypePrices, List<double> historicPercent)
        {
            List<double> allVariances = new List<double>();
            int maxRange = Math.Min(regressionSubtypePrices.Count(), historicPercent.Count());
            for (int i = 0; i < maxRange; i++)
            {
                double variance = Math.Pow(regressionSubtypePrices[i] - historicPercent[i], 2);
                allVariances.Add(variance);
            }
            double score = Math.Sqrt(allVariances.Average());
            return score;
        }
    }
}
