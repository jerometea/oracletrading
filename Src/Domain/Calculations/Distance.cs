﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Calculations
{
    public static class Distance
    {
        public static int Calculate(int firstPoint, int secondPoint)
        {
            int distance = Math.Abs(firstPoint - secondPoint);
            return distance;
        }
    }
}
