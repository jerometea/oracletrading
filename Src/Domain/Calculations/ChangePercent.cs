﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Calculations
{
    public static class ChangePercent
    {
        public static double Calculate(double firstPrice, double lastPrice)
        {
            if (firstPrice == 0) throw new ArgumentException("division by 0 is impossible", "firstPrice");

            double changePercent = (lastPrice - firstPrice) / firstPrice * 100;

            changePercent = Math.Round(changePercent, 1);

            return changePercent;
        }
    }
}
