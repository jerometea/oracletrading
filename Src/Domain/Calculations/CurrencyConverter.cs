﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Calculations
{
    static public class CurrencyConverter
    {
        public static double ConvertToEuro(double euroRate, double price)
        {
            return Math.Round(price / euroRate, 6);
        }
    }
}
