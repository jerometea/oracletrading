﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Calculations
{
    public static class HighChangeCounter
    {
        public static List<Change> Count(List<double> prices, List<(int minimaMaximaPosition, bool isMaxima)> peaks)
        {
            List<Change> highestChangesPercent = new List<Change>();

            for (int peakIndex = 0; peakIndex < peaks.Count - 20; peakIndex++)
            {
                double highestChangePercent = 0;
                int highestChangeDay = 0;
                bool hasFoundEqualPeak = false;

                for (int rangeDay = 1; rangeDay <= 20; rangeDay++)
                {
                    double peakPrice = prices[peakIndex];

                    double endPrice = prices[peakIndex + rangeDay];

                    (int peaksPositions, bool isMaxima)? endPeak = peaks.FirstOrDefault(peak => peak.minimaMaximaPosition == peakIndex + rangeDay);

                    double changePercent = ChangePercent.Calculate(peakPrice, endPrice);

                    if (endPeak.HasValue && endPeak.Value.isMaxima == peaks[peakIndex].isMaxima)
                    {
                        if (endPeak.Value.isMaxima)
                        {
                            hasFoundEqualPeak = (-2 <= changePercent && changePercent <= 2) ? true : false;
                        }
                        else
                        {
                            hasFoundEqualPeak = changePercent <= 2 ? true : false;
                        }
                    }

                    if (hasFoundEqualPeak) break;

                    if (peaks[peakIndex].isMaxima && changePercent < highestChangePercent)
                    {
                        Change previousHighestChange = highestChangesPercent.FirstOrDefault(peak => peak.Range == highestChangeDay);

                        highestChangePercent = changePercent;
                        highestChangeDay = rangeDay;
                    }

                    else if (!peaks[peakIndex].isMaxima && changePercent < highestChangePercent)
                    {
                        Change previousHighestChange = highestChangesPercent.FirstOrDefault(peak => peak.Range == highestChangeDay);

                        if(previousHighestChange is null)
                        {
                            highestChangeDay = rangeDay;

                            highestChangePercent = changePercent;
                        }

                        if(previousHighestChange is not null && highestChangePercent < previousHighestChange.Percent )
                        {
                            highestChangesPercent.Remove(previousHighestChange);
                        }
                    }
                }

                if (Math.Abs(highestChangePercent) >= 3.5)
                {
                    Change change = new Change();

                    change.Percent = highestChangePercent;

                    change.Range = highestChangeDay;

                    highestChangesPercent.Add(change);
                }

            }
            return highestChangesPercent;
        }
    }
}
