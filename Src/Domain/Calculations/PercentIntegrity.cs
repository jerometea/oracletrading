﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Statistics
{
    public static class PercentIntegrity
    {
        public static double Calculate(List<double?> historic)
        {
            double countValidPrice = 0;
            foreach (var price in historic)
            {
                if (price != null)
                {
                    countValidPrice++;
                }
            }
            double percentageIntegrity = Math.Ceiling( countValidPrice / historic.Count * 100 ) ;
            return percentageIntegrity;
        }
    }
}
