﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Calculations
{
    public class MinimaMaximaFinder
    {
        public static List<(int position, bool isMaxima)> GetMinimasMaximasPositions(IList<double> prices)
        {
            var minimasMaximasIndexes = new List<(int peakIndex, bool isMaxima)>();

            int horizontalPosition = 0;

            CurveState curveState = CurveState.GoingUp;

            for (int pricePosition = 1; pricePosition != prices.Count; pricePosition++)
            {
                double price = prices[pricePosition];
                double previousPrice = prices[pricePosition - 1];

                double substractionSign = Math.Sign(price - previousPrice);

                switch (substractionSign)
                {
                    case -1:
                        if (curveState == CurveState.GoingUp)
                        {
                            minimasMaximasIndexes.Add((pricePosition - 1, true));
                        }
                        curveState = CurveState.GoingDown;
                        break;
                    case 0:
                        if (curveState == CurveState.GoingDown)
                        {
                            horizontalPosition = pricePosition;
                        }
                        curveState = (curveState == CurveState.GoingUp) ? CurveState.GoingUp : CurveState.Horizontal;
                        break;
                    case 1:
                        if (curveState == CurveState.GoingDown)
                        {
                            minimasMaximasIndexes.Add((pricePosition - 1, false));
                        }
                        else if (curveState == CurveState.Horizontal)
                        {
                            minimasMaximasIndexes.Add(((horizontalPosition + pricePosition - 1) / 2, false));
                        }
                        curveState = CurveState.GoingUp;
                        break;
                }
            }
            return minimasMaximasIndexes;
        }
    }

    enum CurveState
    {
        GoingDown, Horizontal, GoingUp
    }
}
