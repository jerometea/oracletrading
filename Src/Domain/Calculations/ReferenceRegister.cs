﻿using Domain.Entities;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Domain.Statistics
{
    public class ReferenceRegister : IReferenceRegister
    {
        public Dictionary<string, List<double>> References { get; }

        public ReferenceRegister()
        {
            References = new Dictionary<string, List<double>>();
            StoreReferences();
        }

        void StoreReferences()
        {
            var allPaths = new HashSet<string>()
            {
                @"F:\3Bourse\Projets\Datas\\References\YTD_72C.txt"
            };

            foreach (var path in allPaths)
            {
                var referenceName = Path.GetFileNameWithoutExtension(path);
                var referencePrices = ReadReferencePrices(path);

                References.Add(referenceName, referencePrices);
            }
        }

        List<double> ReadReferencePrices(string pathFile)
        {
            List<double> referenceValues = new List<double>();

            using (StreamReader sr = new StreamReader(pathFile))
            {
                while (!sr.EndOfStream)
                {
                    double line = double.Parse(sr.ReadLine());
                    referenceValues.Add(line);
                }
            }
            return referenceValues;
        }
    }
}
