﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public interface IRegressionSubtypeReader
    {
        public List<RegressionSubtype> OpenRegressionSubtypes(bool openOriginalRegressionSubtypesOnly);
    }
}
