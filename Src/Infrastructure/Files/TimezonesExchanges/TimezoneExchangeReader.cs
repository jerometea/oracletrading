﻿using Application.Common.Interfaces;
using Application.Common.Models.Yahoo;
using Application.Common.PathConfigurations;
using Application.Interfaces;
using Domain.Entities;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Files
{
    public class TimezoneExchangeReader : IExchangeTimezoneReader
    {
        private IVisionTradingDbContext _context;
        Folder _folders;

        public TimezoneExchangeReader(IVisionTradingDbContext context,
                              IOptions<Folder> pathsConfiguration)
        {
            _context = context;
            _folders = pathsConfiguration.Value;
        }
        public void OpenJson()
        {
            List<string> allFiles = Directory.EnumerateFiles($"{_folders.Json}/HistoricalDatas", "*", SearchOption.AllDirectories).ToList();

            int count = 0;

            HashSet<string> allExchangeWithTimezone = new HashSet<string>();
            HashSet<string> allTimezones = new HashSet<string>();
            HashSet<string> allCurrencies = new HashSet<string>();
            string fileError = _folders + @"ParallelTest2.txt";
            foreach (var file in allFiles)
            {
                count++;

                if (!File.Exists(file))
                    continue;

                long fileSize = new FileInfo(file).Length;

                if (fileSize == 0)
                    continue;

                JObject json = JObject.Parse(File.ReadAllText(file));
                YahooTimeframeModel stock = JsonConvert.DeserializeObject<YahooTimeframeModel>(json["chart"].ToString());
                if (stock.result == null)
                {
                    using (StreamWriter tw = new StreamWriter(fileError, true))
                    {
                        tw.WriteLine($"line {90} {file}");
                    }
                    continue;
                }

                Meta meta = stock.result[0].meta;

                List<double?> close = stock.result[0].indicators.quote[0].close;
                var exchangeName = meta.exchangeName;
                var timezone = meta.exchangeTimezoneName;
                var currency = meta.currency;

                if (string.IsNullOrWhiteSpace(exchangeName))
                {
                    exchangeName = "n/a";
                }
                if (string.IsNullOrWhiteSpace(timezone))
                {
                    timezone = "n/a";
                }
                if (string.IsNullOrWhiteSpace(currency))
                {
                    currency = "n/a";
                }

                string exchangeWithTimezone = $"{exchangeName}#{timezone}";
                // Exchange
                if (!allExchangeWithTimezone.Contains(exchangeWithTimezone))
                {
                    allExchangeWithTimezone.Add(exchangeWithTimezone);
                }
                // Timezone
                if (!allTimezones.Contains(timezone))
                {
                    allTimezones.Add(timezone);
                }
                // Currency
                if (!allCurrencies.Contains(currency))
                {
                    allCurrencies.Add(currency);
                }
            };

            // Timezone
            List<Timezone> allTimezoneEntities = new List<Timezone>();
            foreach (var timezoneName in allTimezones)
            {
                var timezone = new Timezone() { TimezoneName = timezoneName };
                allTimezoneEntities.Add(timezone);
            }
            allTimezoneEntities.Sort((current, next) => current.TimezoneName.CompareTo(next.TimezoneName));
            foreach (var timezoneEntity in allTimezoneEntities)
            {
                _context.Timezone.Add(timezoneEntity);
                try
                {
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    using (StreamWriter tw = new StreamWriter(fileError, true))
                    {
                        tw.WriteLine($"timezone : {timezoneEntity} line {152}");
                        continue;
                    }
                }
            }

            //// Exchange
            List<Exchange> allExchangesEntites = new List<Exchange>();
            foreach (var exchangeWithTimezone in allExchangeWithTimezone)
            {
                var exchange = exchangeWithTimezone.Split('#')[0];
                var timezone = exchangeWithTimezone.Split('#')[1];

                foreach (var timezoneEntity in allTimezoneEntities)
                {
                    if (timezoneEntity.TimezoneName.Equals(timezone))
                    {
                        var exchangeEntity = new Exchange() { ExchangeShortName = exchange, TimezoneId = timezoneEntity.TimezoneId };
                        allExchangesEntites.Add(exchangeEntity);
                    }
                }
            }
            allExchangesEntites.Sort((current, next) => current.ExchangeShortName.CompareTo(next.ExchangeShortName));
            foreach (var exchangeEntity in allExchangesEntites)
            {
                _context.Exchange.Add(exchangeEntity);
                try
                {
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    using (StreamWriter tw = new StreamWriter(fileError, true))
                    {
                        tw.WriteLine($"exchange : {exchangeEntity} line {186}");
                        continue;
                    }
                }
            }



            // Currency
            string currencyFilebefore = _folders + @"currencyBefore.txt";
            var currenciesFromDb = _context.Currency.ToList();
            var currencyNames = new HashSet<string>();
            foreach (var c in currenciesFromDb)
            {
                currencyNames.Add(c.CurrencyShortName);
            }
            using (StreamWriter tw = new StreamWriter(currencyFilebefore, true))
            {
                foreach (var c in allCurrencies)
                {
                    tw.WriteLine(c);
                }
            }

            foreach (var currencyFromDb in currencyNames)
            {
                foreach (var currency in allCurrencies)
                {
                    if (currencyFromDb.Equals(currency))
                    {
                        allCurrencies.Remove(currency);
                    }
                }
            }
            string currencyFileAfter = _folders + @"currencyAfter.txt";
            using (StreamWriter tw = new StreamWriter(currencyFilebefore, true))
            {
                foreach (var c in currencyFileAfter)
                {
                    tw.WriteLine(c);
                }
            }


            List<Currency> allCurrencyEntites = new List<Currency>();
            foreach (var currency in allCurrencies)
            {
                var currencyEntity = new Currency() { CurrencyShortName = currency };
                allCurrencyEntites.Add(currencyEntity);
            }
            allCurrencyEntites.Sort((current, next) => current.CurrencyShortName.CompareTo(next.CurrencyShortName));
            foreach (var currencyEntity in allCurrencyEntites)
            {
                _context.Currency.Add(currencyEntity);
                try
                {
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    using (StreamWriter tw = new StreamWriter(fileError, true))
                    {
                        tw.WriteLine($"currency : {currencyEntity.CurrencyShortName} line {232} error {e.Message}");
                        continue;
                    }
                }
            }
        }
    }
}
