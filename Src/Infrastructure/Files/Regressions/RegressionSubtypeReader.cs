﻿using Application.Common.Interfaces;
using Application.Common.PathConfigurations;
using Domain;
using Domain.Entities;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Text;
using ClosedXML.Excel;
using System.Linq;

namespace Infrastructure.Files
{
    public class RegressionSubtypeReader : IRegressionSubtypeReader
    {
        string _folder;
        IVisionTradingDbContext _context;

        public RegressionSubtypeReader(IOptions<Folder> pathsConfiguration, IVisionTradingDbContext context)
        {
            _folder = pathsConfiguration.Value.Regressions;
            _context = context;
        }

        public List<RegressionSubtype> OpenRegressionSubtypes(bool openOriginalRegressionSubtypesOnly)
        {
            List<RegressionSubtype> regressionSubtypes = new List<RegressionSubtype>();
            List<Regression> regressions = _context.Regression.ToList();

            string stringPattern = openOriginalRegressionSubtypesOnly ? "*_*.xlsx" : "*.xlsx";

            string[] csvPaths = Directory.GetFiles(_folder, stringPattern, SearchOption.AllDirectories);

            foreach (var csvPath in csvPaths)
            {
                RegressionSubtype file = OpenCsv(csvPath, regressions);

                regressionSubtypes.Add(file);
            }
            return regressionSubtypes;
        }

        RegressionSubtype OpenCsv(string csvPath, List<Regression> regressions)
        {
            RegressionSubtype regressionSubtype = new RegressionSubtype();

            XLWorkbook workbook = new XLWorkbook(csvPath);

            IXLWorksheet sheet = workbook.Worksheets.Worksheet(1);

            int priceRow = 2;

            int priceColumn = 3;

            string cellPrice = sheet.Cell(priceRow, priceColumn).Value?.ToString();

            while (!string.IsNullOrWhiteSpace(cellPrice))
            {
                double price = double.Parse(cellPrice);
                regressionSubtype.Prices.Add(price);
                priceRow++;
                cellPrice = sheet.Cell(priceRow, priceColumn).Value?.ToString().Replace(".", ",");
            }


            if(regressionSubtype.Prices.Count == 0)
            {

            }
            string regressionName = sheet.Cell("D2").Value.ToString();

            bool IsPositive = (bool)sheet.Cell("E2").Value;

            regressionSubtype.Regression = regressions.Where(regression => regression.RegressionName == regressionName &&
                                                                           regression.HasPositiveCoefficient == IsPositive)
                                                      .FirstOrDefault();

            regressionSubtype.FirstY = sheet.Cell("F2").GetValue<int?>();

            regressionSubtype.LastY = sheet.Cell("G2").GetValue<int?>();

            regressionSubtype.Symbol = sheet.Cell("H2").Value.ToString();
            regressionSubtype.RegressionSubtypeName = sheet.Cell("J2").Value.ToString();

            regressionSubtype.Count = regressionSubtype.Prices.Count;

            return regressionSubtype;
        }
    }
}
