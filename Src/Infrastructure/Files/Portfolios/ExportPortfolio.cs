﻿using Application.Common.Interfaces.Files;
using Application.Common.PathConfigurations;
using ClosedXML.Excel;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Files.Portfolios
{
    public class ExportPortfolio : IExportPortfolio
    {
        string _folder;

        public ExportPortfolio(IOptions<Folder> pathsConfiguration)
        {
            _folder = pathsConfiguration.Value.Portfolios;
        }

        public void ExportReport(int portfolioId, SortedDictionary<string, List<string>> portfolioGains, bool isItRealTimePortfolio, int sheetNumber)
        {
            string path;

            string firstSymbol = null;
            string lastSymbol = portfolioGains.Keys.Last();

            int index = 0;
            foreach (var symbol in portfolioGains)
            {
                if (index == 3)
                {
                    firstSymbol = symbol.Key;
                    break;
                }
                index++;
            }

            if (isItRealTimePortfolio)
            {
                path = $"{_folder}/{portfolioId} - real time/{portfolioId} - {firstSymbol} - {lastSymbol} - real time.xlsx";
            }
            else
            {
                path = $"{_folder}/{portfolioId}/{portfolioId} - {firstSymbol} - {lastSymbol}.xlsx"; ;
            }

            XLWorkbook workbook;
            IXLWorksheet sheet;
            int initialWriteRow;
            bool isItFileCreation = false;

            if (File.Exists(path))
            {
                workbook = new XLWorkbook(path);

                sheet = workbook.Worksheets.Worksheet(1);

                int lastRow = sheet.LastCellUsed().Address.RowNumber;

                int dateColumn = 1;

                string mostRecentDateInFile = sheet.Cell(lastRow, dateColumn).Value.ToString();

                mostRecentDateInFile = Convert.ToDateTime(mostRecentDateInFile).ToShortDateString();

                string mostRecentDateInDb = Convert.ToDateTime(portfolioGains["^0_Date"][0]).ToShortDateString();

                initialWriteRow = mostRecentDateInDb == mostRecentDateInFile ? lastRow : lastRow += 1;
            }
            else
            {
                workbook = new XLWorkbook();
                sheet = workbook.Worksheets.Add("Sheet1");
                initialWriteRow = 1;
                isItFileCreation = true;
            }

            int writeColumn = 1;

            int writeRow = initialWriteRow;

            foreach (KeyValuePair<string, List<string>> symbol in portfolioGains)
            {
                if (isItFileCreation)
                {
                    if (symbol.Key != "^0_Date") // Don't write column title
                    {
                        sheet.Cell(writeRow, writeColumn).Value = symbol.Key;
                    }
                    writeRow++;
                }

                foreach (string price in symbol.Value)
                {
                    if (symbol.Key == "^0_Date")
                    {
                        sheet.Cell(writeRow, writeColumn).Value = price;
                    }
                    else
                    {
                        sheet.Cell(writeRow, writeColumn).Value = Convert.ToDouble(price);
                    }
                    writeRow++;
                }

                writeRow = initialWriteRow;
                writeColumn++;
            }
            workbook.SaveAs(path);
        }
    }
}
