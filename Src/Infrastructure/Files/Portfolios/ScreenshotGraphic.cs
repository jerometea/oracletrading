﻿using Application.Common.Interfaces.Files;
using Application.Common.PathConfigurations;
using Microsoft.Extensions.Options;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Files.Portfolios
{
    public class ScreenshotGraphic : IScreenshotGraphic
    {
        IWebDriver _driver;
        bool _isCookieDenied = true;
        private string _symbolsFile;

        public ScreenshotGraphic(IOptions<Folder> folder)
        {

            _symbolsFile = $"{folder.Value.Portfolios}/YahooStocks.csv";
            string chrome = $"{folder.Value.Portfolios}/chromedriver";

            var options = new ChromeOptions();
            options.AddArgument("headless");
            options.AddArgument("window-size=1920,1080");
            _driver = new ChromeDriver(chrome, options);
        }

        public void Capture(string symbol, int portfolioId, string timeframe, string startDate, string openDate, bool isItRealTimePortfolio)
        {
            string path = isItRealTimePortfolio ? $"{_symbolsFile}/{portfolioId} - real time/timeframes/{symbol}/{timeframe}.png" :
                                                  $"{_symbolsFile}/{portfolioId}/timeframes/{symbol}/{timeframe}.png";

            if (File.Exists(path))
                return;

            string symbolDirectory = Path.GetDirectoryName(path);

            string timeframesDirectory = Path.GetDirectoryName(symbolDirectory);

            string protfolioDirectory = Path.GetDirectoryName(timeframesDirectory);

            Directory.CreateDirectory(protfolioDirectory);

            Directory.CreateDirectory(timeframesDirectory);

            Directory.CreateDirectory(symbolDirectory);

            string url = $"https://fr.finance.yahoo.com/chart/{ symbol }#eyJpbnRlcnZhbCI6ImRheSIsInBlcmlvZGljaXR5IjoxLCJjYW5kbGVXaWR0aCI6MzguODcxNzk0ODcxNzk0ODcsImZsaXBwZWQiOmZhbHNlLCJ2b2x1bWVVbmRlcmxheSI6dHJ1ZSwiYWRqIjp0cnVlLCJjcm9zc2hhaXIiOnRydWUsImNoYXJ0VHlwZSI6Im1vdW50YWluIiwiZXh0ZW5kZWQiOmZhbHNlLCJtYXJrZXRTZXNzaW9ucyI6e30sImFnZ3JlZ2F0aW9uVHlwZSI6Im9obGMiLCJjaGFydFNjYWxlIjoibGluZWFyIiwicGFuZWxzIjp7ImNoYXJ0Ijp7InBlcmNlbnQiOjEsImRpc3BsYXkiOiJMSFgiLCJjaGFydE5hbWUiOiJjaGFydCIsImluZGV4IjowLCJ5QXhpcyI6eyJuYW1lIjoiY2hhcnQiLCJwb3NpdGlvbiI6bnVsbH0sInlheGlzTEhTIjpbXSwieWF4aXNSSFMiOlsiY2hhcnQiLCLigIx2b2wgdW5kcuKAjCJdfX0sImxpbmVXaWR0aCI6Miwic3RyaXBlZEJhY2tncm91bmQiOnRydWUsImV2ZW50cyI6dHJ1ZSwiY29sb3IiOiIjMDA4MWYyIiwic3RyaXBlZEJhY2tncm91ZCI6dHJ1ZSwicmFuZ2UiOnsicGVyaW9kaWNpdHkiOnsiaW50ZXJ2YWwiOiJkYXkiLCJwZXJpb2QiOjF9LCJkdExlZnQiOiIyMDE5LTEyLTI1VDIzOjAwOjAwLjAwMFoiLCJkdFJpZ2h0IjoiMjAyMC0wMi0yMFQyMzowMDowMC4wMDBaIiwicGFkZGluZyI6MH0sImV2ZW50TWFwIjp7ImNvcnBvcmF0ZSI6eyJkaXZzIjp0cnVlLCJzcGxpdHMiOnRydWV9LCJzaWdEZXYiOnt9fSwiY3VzdG9tUmFuZ2UiOnsic3RhcnQiOjE1NzczMTQ4MDAwMDAsImVuZCI6MTU4MjIzOTYwMDAwMH0sInN5bWJvbHMiOlt7InN5bWJvbCI6IkxIWCIsInN5bWJvbE9iamVjdCI6eyJzeW1ib2wiOiJMSFgiLCJxdW90ZVR5cGUiOiJFUVVJVFkiLCJleGNoYW5nZVRpbWVab25lIjoiQW1lcmljYS9OZXdfWW9yayJ9LCJwZXJpb2RpY2l0eSI6MSwiaW50ZXJ2YWwiOiJkYXkifV0sInN0dWRpZXMiOnsi4oCMdm9sIHVuZHLigIwiOnsidHlwZSI6InZvbCB1bmRyIiwiaW5wdXRzIjp7ImlkIjoi4oCMdm9sIHVuZHLigIwiLCJkaXNwbGF5Ijoi4oCMdm9sIHVuZHLigIwifSwib3V0cHV0cyI6eyJVcCBWb2x1bWUiOiJyZ2JhKDIwMCwgMjQwLCAyMjAsIDAuOCkiLCJEb3duIFZvbHVtZSI6InJnYmEoMjU1LCA0OCwgNjAsIDAuOCkifSwicGFuZWwiOiJjaGFydCIsInBhcmFtZXRlcnMiOnsid2lkdGhGYWN0b3IiOjAuNDUsImNoYXJ0TmFtZSI6ImNoYXJ0IiwicGFuZWxOYW1lIjoiY2hhcnQifX19fQ--";

            _driver.Navigate().GoToUrl(url);

            if (_isCookieDenied)
            {
                AcceptCookie(0);
                _isCookieDenied = false;
            }

            if (timeframe.Contains("ONE DAY"))
            {
                HideDividend();
            }

            OpenDatePicker(0);

            SetStartDate(startDate, 0);

            SetOpenDate(openDate, 0);

            ApplyDates(0);
            Thread.Sleep(1000);
            TakeScreenShot(path);
        }

        void AcceptCookie(int indexPath)
        {
            List<string> xpathPopup = new List<string>()
            {
                "/html/body/div/div/div/div/form/div[2]/div[2]/button[1]",
                "/html/body/div/div/div/div/form/div[3]/div/button"
            };

            try
            {
                _driver.FindElement(By.XPath(xpathPopup[indexPath])).Click();
            }
            catch (Exception)
            {
                if (indexPath == xpathPopup.Count - 1)
                {
                    throw;
                }
                AcceptCookie(indexPath += 1);
            }
        }

        void OpenDatePicker(int indexPath)
        {
            List<string> xpathDatePicker = new List<string>(){
                "/html/body/div[1]/div/div/div[1]/div/div[2]/div/div/section/section/div[1]/div/div[1]/div[3]/div/div/div",
                "/html/body/div[1]/div/div/div[1]/div/div[2]/div/div/section/section/div[1]/div/div[1]/div[2]/div/div"
            };

            var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(1));

            try
            {
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath(xpathDatePicker[indexPath])));
                _driver.FindElement(By.XPath(xpathDatePicker[indexPath])).Click();
            }
            catch (Exception)
            {
                if (indexPath == xpathDatePicker.Count - 1)
                {
                    throw;
                }
                OpenDatePicker(indexPath += 1);
            }
        }

        void HideDividend()
        {
            try
            {
                string xpathEvent = "/html/body/div[1]/div/div/div[1]/div/div[2]/div/div/section/section/div[1]/div/div[1]/div[1]/div/div/span";
                var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(1));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath(xpathEvent)));
                _driver.FindElement(By.XPath(xpathEvent)).Click();

                string xpathDividend = "/html/body/div[1]/div/div/div[1]/div/div[2]/div/div/section/section/div[1]/div/div[1]/div[1]/div/div/div/div[3]/div/ul/li[1]/label/span/span";
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath(xpathDividend)));
                _driver.FindElement(By.XPath(xpathDividend)).Click();

                string xpathFraction = "/html/body/div[1]/div/div/div[1]/div/div[2]/div/div/section/section/div[1]/div/div[1]/div[1]/div/div/div/div[3]/div/ul/li[2]/label/span/span";
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath(xpathFraction)));
                _driver.FindElement(By.XPath(xpathFraction)).Click();
            }
            catch (Exception) { }
        }

        void SetStartDate(string startDate, int indexStartDate)
        {
            List<string> xpathStartDates = new List<string>(){
                "/html/body/div[1]/div/div/div[1]/div/div[2]/div/div/section/section/div[1]/div/div[1]/div[3]/div/div/div/div/div[3]/div[1]/div[1]/form/div/input",
                "/html/body/div[1]/div/div/div[1]/div/div[2]/div/div/section/section/div[1]/div/div[1]/div[2]/div/div/div/div/div[3]/div[1]/div[1]/form/div/input",
                "/html/body/div[1]/div/div/div[1]/div/div[2]/div/div/section/section/div[1]/div/div[1]/div[3]/div/div/div/div/div[3]/div[1]/div[1]/form/div/label/input"
            };
            try
            {
                IWebElement textBoxstartDate = _driver.FindElement(By.XPath(xpathStartDates[indexStartDate]));
                textBoxstartDate.Clear();
                textBoxstartDate.Clear();
                textBoxstartDate.Clear();
                textBoxstartDate.SendKeys(startDate);
            }
            catch (Exception)
            {
                if (indexStartDate == xpathStartDates.Count - 1)
                {
                    throw;
                }
                SetStartDate(startDate, indexStartDate + 1);
            }
        }

        void SetOpenDate(string openDate, int indexOpenDate)
        {

            List<string> xpathOpenDates = new List<string>(){
                "/html/body/div[1]/div/div/div[1]/div/div[2]/div/div/section/section/div[1]/div/div[1]/div[3]/div/div/div/div/div[3]/div[2]/div[1]/form/div/input",
                "/html/body/div[1]/div/div/div[1]/div/div[2]/div/div/section/section/div[1]/div/div[1]/div[2]/div/div/div/div/div[3]/div[2]/div[1]/form/div/input",
                "/html/body/div[1]/div/div/div[1]/div/div[2]/div/div/section/section/div[1]/div/div[1]/div[3]/div/div/div/div/div[3]/div[2]/div[1]/form/div/label/input"
            };

            try
            {
                IWebElement textBoxOpenDate = _driver.FindElement(By.XPath(xpathOpenDates[indexOpenDate]));
                textBoxOpenDate.Clear();
                textBoxOpenDate.SendKeys(openDate);
            }

            catch (Exception)
            {
                if (indexOpenDate == xpathOpenDates.Count - 1)
                {
                    throw;
                }
                SetOpenDate(openDate, indexOpenDate + 1);
            }
        }

        void ApplyDates(int indexApplyButton)
        {
            List<string> xpathApplyButtons = new List<string>(){
                "/html/body/div[1]/div/div/div[1]/div/div[2]/div/div/section/section/div[1]/div/div[1]/div[3]/div/div/div/div/div[3]/div[3]/button[1]",
                "/html/body/div[1]/div/div/div[1]/div/div[2]/div/div/section/section/div[1]/div/div[1]/div[2]/div/div/div/div/div[3]/div[3]/button[1]"
            };
            var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(1));
            try
            {
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath(xpathApplyButtons[indexApplyButton])));
                _driver.FindElement(By.XPath(xpathApplyButtons[indexApplyButton])).Click();
            }
            catch (Exception)
            {
                if (indexApplyButton == xpathApplyButtons.Count - 1)
                {
                    throw;
                }
                ApplyDates(indexApplyButton += 1);
            }
        }

        void TakeScreenShot(string path)
        {
            Screenshot image = ((ITakesScreenshot)_driver).GetScreenshot();
            image.SaveAsFile(path, ScreenshotImageFormat.Png);
        }
    }
}
