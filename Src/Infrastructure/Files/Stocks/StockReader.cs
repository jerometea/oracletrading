﻿using Application.Common.Helpers;
using Application.Common.Interfaces;
using Application.Common.Mapper;
using Application.Common.Models.Yahoo;
using Application.Common.PathConfigurations;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Files
{
    public class StockReader : IStockReader
    {
        string _jsonFolder;

        public StockReader(IOptions<Folder> pathsConfiguration)
        {
            _jsonFolder = pathsConfiguration.Value.Json;
        }

        public List<YahooTimeframeModel> OpenJson(TimeframeSelector periods, HashSet<string> symbolsToRead)
        {
            List<string> allFiles = Directory.EnumerateFiles($"{_jsonFolder}/HistoricalDatas", "*", SearchOption.AllDirectories).ToList();

            var watch = System.Diagnostics.Stopwatch.StartNew();

            int count = 0;

            List<YahooTimeframeModel> jsonStocks = new List<YahooTimeframeModel>();

            watch.Start();

            int fileNotExist = 0;
            int fileZeroSize = 0;
            int jsonNull = 0;
            int historicNull = 0;
            int symbolNull = 0;
            int timezoneNull = 0;

            const bool forceNonParallel = false;
            var options = new ParallelOptions { MaxDegreeOfParallelism = forceNonParallel ? 1 : -1 };

            Parallel.ForEach(allFiles, options, file =>
           {
               count++;

               string symbolFromFileTitle = Path.GetFileNameWithoutExtension(file).Split("_")[0];

               if (symbolsToRead != null && !symbolsToRead.Contains(symbolFromFileTitle))
               {
                   return;
               }


               if (!File.Exists(file))
                   return;

               if (!periods.TimeframesString.Any(period => file.Contains(period)))
               {
                   fileNotExist++;
                   return;
               }

               long fileSize = new FileInfo(file).Length;

               if (fileSize == 0)
               {
                   fileZeroSize++;
                   return;
               }

               JObject json = null;
               try
               {

                   json = JObject.Parse(File.ReadAllText(file));
               }
               catch (Exception e)
               {
                   return;
               }

               YahooTimeframeModel stockJson = JsonConvert.DeserializeObject<YahooTimeframeModel>(json["chart"].ToString());

               if (stockJson.result == null)
               {
                   jsonNull++;
                   return;
               }

               List<double?> historic = stockJson.result[0].indicators.quote[0].close;

               if (historic == null)
               {
                   historicNull++;
                   return;
               }

               string symbolFromJson = stockJson.result[0].meta.symbol;

               if (symbolFromJson == null)
               {
                   symbolNull++;
                   return;
               }

               string timezone = stockJson.result[0].meta.timezone;
               if (timezone == null)
               {
                   timezoneNull++;
                   return;
               }

               string range = stockJson.result[0].meta.range;

               if (string.IsNullOrWhiteSpace(range))
               {
                   string timeframeFromFileName = Path.GetFileNameWithoutExtension(file).Split("_")[1];
                   range = TimeframeMapper.ToYahooTimeframe(timeframeFromFileName);
                   stockJson.result[0].meta.range = range;
               }

               if ((historic.Count >= 600 && (range == "2d" || range == "3d" || range == "4d")) ||
                   (historic.Count >= 250 && range == "1mo"))
               {
                   historic = RemoveNullsFromBuggyHistoric.RemoveNulls(historic);
                   stockJson.result[0].indicators.quote[0].close = historic;
               }

               if (stockJson is null) return;

               jsonStocks.Add(stockJson);

           });

            var elapsedMs = watch.ElapsedMilliseconds;

            return jsonStocks;
        }
    }
}
