﻿using Application.Common.Models.Yahoo;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Infrastructure.Files.Stocks
{
    public static class SymbolReader
    {
        public static HashSet<string> OpenCsv(string path)
        {
            List<YahooResearchModel> yahooStocks;
            var symbols = new HashSet<string>();

            using (var reader = new StreamReader(path))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csv.Configuration.Delimiter = ";";
                yahooStocks = csv.GetRecords<YahooResearchModel>().ToList();
            }

            foreach (var yahooStock in yahooStocks)
            {
                if (!symbols.Contains(yahooStock.Symbol))
                {
                    symbols.Add(yahooStock.Symbol);
                }
            }
            return symbols;
        }
    }
}
