﻿using Application.Common.Interfaces;
using Application.Common.UserSecrets;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Infrastructure.Api.Fixer.GetEuroRates
{
    public class GetEuroRatesApi : IFixer
    {
        string _key;

        public GetEuroRatesApi(IOptions<UserSecret> userSecret)
        {
            _key = userSecret.Value.Fixer;
        }

        public async Task<Dictionary<string, double>> CurrenciesRates()
        {
            string api = "http://data.fixer.io/api/latest?access_key=" + _key;

            HttpClient client = new HttpClient();

            string responseBody = null;

            try
            {
                responseBody = await client.GetStringAsync(api);
            }

            catch(HttpRequestException e)
            {
                Console.WriteLine("Message :{0} ", e.Message);
            }

            JObject reponse = JObject.Parse(responseBody);

            string tempEuroRates = reponse["rates"].ToString();

            tempEuroRates = Regex.Replace(tempEuroRates, @"\t|\n|\r", "");

            Dictionary<string, double> euroRates = JsonConvert.DeserializeObject<Dictionary<string, double>>(tempEuroRates);

            return euroRates;
        }
    }
}
