﻿using Application.Common.Helpers;
using Infrastructure.Providers.Yahoo;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Api.Yahoo.DownloadStockHistoric
{
    public class YahooApi
    {
        const string INTERVAL_1MN = "1m", INTERVAL_2MN = "2m", INTERVAL_15M = "15m", INTERVAL_30M = "30m",
                     INTERVAL_1H = "1h", INTERVAL_1D = "1d", INTERVAL_1WK = "1wk", INTERVAL_1MO = "1mo";

        const string TIMEFRAME_1D = "1d", TIMEFRAME_5D = "5d",
                     TIMEFRAME_1M = "1mo", TIMEFRAME_6M = "6mo",
                     TIMEFRAME_YTD = "ytd", TIMEFRAME_1Y = "1y", TIMEFRAME_5Y = "5y", TIMEFRAME_MAX = "max";

        const string TIMEFRAME_2D = "two days timeframe does not exist for yahoo api",
                     TIMEFRAME_3D = "three days timeframe does not exist for yahoo api",
                     TIMEFRAME_4D = "four days timeframe does not exist for yahoo api",
                     TIMEFRAME_6D = "six days timeframe does not exist for yahoo api",
                     TIMEFRAME_7D = "seven days timeframe does not exist for yahoo api",
                     TIMEFRAME_8D = "eight days timeframe does not exist for yahoo api",
                     TIMEFRAME_9D = "nine days timeframe does not exist for yahoo api",
                     TIMEFRAME_10D = "ten days timeframe does not exist for yahoo api",
                     TIMEFRAME_11D = "eleven days timeframe does not exist for yahoo api",
                     TIMEFRAME_12D = "twelve days timeframe does not exist for yahoo api";


        public static Dictionary<string, List<Request>> GenerateRequests(HashSet<string> symbols, TimeframeSelector timeframes, DateTimeOffset? frenchOpenDate, bool isItForPortfolioExport)
        {
            Dictionary<string, List<Request>> requests = new Dictionary<string, List<Request>>();

            bool hasOneHourInterval = false;
            bool hasFifteenMinutesInterval = false;
            bool hasTwoMinutesInterval = false;

            double totalDaysFromOpenDateToNow = 99999;

            if (frenchOpenDate != null)
            {
                totalDaysFromOpenDateToNow = (DateTimeOffset.Now - frenchOpenDate.Value).TotalDays;
            }

            //        days
            // 1mn  < 30
            // 2mn  < 60
            // 15mn < 60
            // 1h   < 730

            // To download historic of... | open date must be less than...
            // one day                    | 60 days
            // five days                  | 730 days


            if (totalDaysFromOpenDateToNow <= 55 || frenchOpenDate == null)
            {
                hasTwoMinutesInterval = true;
                hasFifteenMinutesInterval = true;
            }

            if (isItForPortfolioExport)
            {
                hasTwoMinutesInterval = false;
            }

            if (totalDaysFromOpenDateToNow <= 720 || frenchOpenDate == null)
            {
                hasOneHourInterval = true;
            }

            foreach (string symbol in symbols)
            {

                //if (symbol != "KRTX") continue;

                string oneDayRequest = timeframes.HasOneDay ? BuildTimeframeRequest(symbol, hasTwoMinutesInterval ? INTERVAL_15M : INTERVAL_1H, TIMEFRAME_1D, frenchOpenDate) : null;

                string twoDaysRequest = timeframes.HasTwoDays ? BuildTimeframeRequest(symbol, hasTwoMinutesInterval ? INTERVAL_15M : INTERVAL_1H, TIMEFRAME_2D, frenchOpenDate) : null;
                string threeDaysRequest = timeframes.HasThreeDays ? BuildTimeframeRequest(symbol, hasTwoMinutesInterval ? INTERVAL_15M : INTERVAL_1H, TIMEFRAME_3D, frenchOpenDate) : null;
                string fourDaysRequest = timeframes.HasFourDays ? BuildTimeframeRequest(symbol, hasTwoMinutesInterval ? INTERVAL_15M : INTERVAL_1H, TIMEFRAME_4D, frenchOpenDate) : null;

                string sixDaysRequest = timeframes.HasSixDays ? BuildTimeframeRequest(symbol, hasTwoMinutesInterval ? INTERVAL_15M : INTERVAL_1H, TIMEFRAME_6D, frenchOpenDate) : null;
                string sevenDaysRequest = timeframes.HasSevenDays ? BuildTimeframeRequest(symbol, hasTwoMinutesInterval ? INTERVAL_30M : INTERVAL_1H, TIMEFRAME_7D, frenchOpenDate) : null;
                string eightDaysRequest = timeframes.HasEightDays ? BuildTimeframeRequest(symbol, hasTwoMinutesInterval ? INTERVAL_30M : INTERVAL_1H, TIMEFRAME_8D, frenchOpenDate) : null;
                string nineDaysRequest = timeframes.HasNineDays ? BuildTimeframeRequest(symbol, hasTwoMinutesInterval ? INTERVAL_30M : INTERVAL_1H, TIMEFRAME_9D, frenchOpenDate) : null;
                string tenDaysRequest = timeframes.HasTenDays ? BuildTimeframeRequest(symbol, hasTwoMinutesInterval ? INTERVAL_30M : INTERVAL_1H, TIMEFRAME_10D, frenchOpenDate) : null;
                string elevenDaysRequest = timeframes.HasElevenDays ? BuildTimeframeRequest(symbol, hasTwoMinutesInterval ? INTERVAL_30M : INTERVAL_1H, TIMEFRAME_11D, frenchOpenDate) : null;
                string twelveDaysRequest = timeframes.HasTwelveDays ? BuildTimeframeRequest(symbol, hasTwoMinutesInterval ? INTERVAL_30M : INTERVAL_1H, TIMEFRAME_12D, frenchOpenDate) : null;


                string fiveDaysRequest = timeframes.HasFiveDays ? BuildTimeframeRequest(symbol, hasFifteenMinutesInterval ? INTERVAL_15M : INTERVAL_1H, TIMEFRAME_5D, frenchOpenDate) : null;
                string oneMonthRequest = timeframes.HasOneMonth ? BuildTimeframeRequest(symbol, hasOneHourInterval ? INTERVAL_1H : INTERVAL_1D, TIMEFRAME_1M, frenchOpenDate) : null;
                string sixMonthsRequest = timeframes.HasSixMonths ? BuildTimeframeRequest(symbol, INTERVAL_1D, TIMEFRAME_6M, frenchOpenDate) : null;
                string ytdRequest = timeframes.HasYTD ? BuildTimeframeRequest(symbol, INTERVAL_1WK, TIMEFRAME_YTD, frenchOpenDate) : null;
                string oneYearRequest = timeframes.HasOneYear ? BuildTimeframeRequest(symbol, INTERVAL_1D, TIMEFRAME_1Y, frenchOpenDate) : null;
                string fiveYearsRequest = timeframes.HasFiveYears ? BuildTimeframeRequest(symbol, INTERVAL_1MO, TIMEFRAME_5Y, frenchOpenDate) : null;
                string maxRequest = timeframes.HasMax ? BuildTimeframeRequest(symbol, INTERVAL_1MO, TIMEFRAME_MAX, frenchOpenDate) : null;

                List<Request> currentSymbolRequests = new List<Request>();

                currentSymbolRequests.Add(new Request { Timeframe = "OneDay", Api = oneDayRequest });

                currentSymbolRequests.Add(new Request { Timeframe = "TwoDays", Api = twoDaysRequest });
                currentSymbolRequests.Add(new Request { Timeframe = "ThreeDays", Api = threeDaysRequest });
                currentSymbolRequests.Add(new Request { Timeframe = "FourDays", Api = fourDaysRequest });

                currentSymbolRequests.Add(new Request { Timeframe = "SixDays", Api = sixDaysRequest });
                currentSymbolRequests.Add(new Request { Timeframe = "SevenDays", Api = sevenDaysRequest });
                currentSymbolRequests.Add(new Request { Timeframe = "EightDays", Api = eightDaysRequest });
                currentSymbolRequests.Add(new Request { Timeframe = "NineDays", Api = nineDaysRequest });
                currentSymbolRequests.Add(new Request { Timeframe = "TenDays", Api = tenDaysRequest });
                currentSymbolRequests.Add(new Request { Timeframe = "ElevenDays", Api = elevenDaysRequest });
                currentSymbolRequests.Add(new Request { Timeframe = "TwelveDays", Api = twelveDaysRequest });


                currentSymbolRequests.Add(new Request { Timeframe = "FiveDays", Api = fiveDaysRequest });
                currentSymbolRequests.Add(new Request { Timeframe = "OneMonth", Api = oneMonthRequest });
                currentSymbolRequests.Add(new Request { Timeframe = "SixMonths", Api = sixMonthsRequest });
                currentSymbolRequests.Add(new Request { Timeframe = "YTD", Api = ytdRequest });
                currentSymbolRequests.Add(new Request { Timeframe = "OneYear", Api = oneYearRequest });
                currentSymbolRequests.Add(new Request { Timeframe = "FiveYears", Api = fiveYearsRequest });
                currentSymbolRequests.Add(new Request { Timeframe = "Max", Api = maxRequest });

                List<Request> singleDaysRequests = BuildSingleDayRequest(timeframes, symbol, hasTwoMinutesInterval ? INTERVAL_15M : INTERVAL_1H, frenchOpenDate);

                currentSymbolRequests.AddRange(singleDaysRequests);
                requests.Add(symbol, currentSymbolRequests);
            }

            return requests;
        }

        static string BuildTimeframeRequest(string symbol, string interval, string timeframe, DateTimeOffset? frenchOpenDate)
        {
            symbol = SymbolValidator.FormatForRequest(symbol);

            if (symbol == "NUMG")
            {

            }

            if (frenchOpenDate == null) // For portfolio in real time (no need open date)
            {
                string url = $"https://query1.finance.yahoo.com/v8/finance/chart/{ symbol }?region=US&lang=en-US&includePrePost=false&interval={interval}&range={timeframe}&corsDomain=finance.yahoo.com&.tsrc=finance";
                return url;
            }

            else
            {
                DateTimeOffset TimeframeStart = frenchOpenDate.Value;

                switch (timeframe)
                {
                    case TIMEFRAME_1D:
                        TimeframeStart = TimeframeStart.AddWorkingDays(-1);
                        break;

                    case TIMEFRAME_2D:
                        TimeframeStart = TimeframeStart.AddWorkingDays(-2);
                        break;

                    case TIMEFRAME_3D:
                        TimeframeStart = TimeframeStart.AddWorkingDays(-3);
                        break;

                    case TIMEFRAME_4D:
                        TimeframeStart = TimeframeStart.AddWorkingDays(-4);
                        break;

                    case TIMEFRAME_5D:
                        TimeframeStart = TimeframeStart.AddWorkingDays(-5);
                        break;

                    case TIMEFRAME_6D:
                        TimeframeStart = TimeframeStart.AddWorkingDays(-6);
                        break;
                    case TIMEFRAME_7D:
                        TimeframeStart = TimeframeStart.AddWorkingDays(-7);
                        break;
                    case TIMEFRAME_8D:
                        TimeframeStart = TimeframeStart.AddWorkingDays(-8);
                        break;
                    case TIMEFRAME_9D:
                        TimeframeStart = TimeframeStart.AddWorkingDays(-9);
                        break;
                    case TIMEFRAME_10D:
                        TimeframeStart = TimeframeStart.AddWorkingDays(-10);
                        break;
                    case TIMEFRAME_11D:
                        TimeframeStart = TimeframeStart.AddWorkingDays(-11);
                        break;
                    case TIMEFRAME_12D:
                        TimeframeStart = TimeframeStart.AddWorkingDays(-12);
                        break;

                    case TIMEFRAME_1M:
                        TimeframeStart = TimeframeStart.AddMonths(-1);
                        break;
                    case TIMEFRAME_6M:
                        TimeframeStart = TimeframeStart.AddMonths(-6);
                        break;
                    case TIMEFRAME_YTD:
                        TimeframeStart = new DateTimeOffset(frenchOpenDate.Value.Year, 1, 1, 0, 0, 0, DateTimeOffset.Now.Offset);
                        break;
                    case TIMEFRAME_1Y:
                        TimeframeStart = TimeframeStart.AddYears(-1);
                        break;
                    case TIMEFRAME_5Y:
                        TimeframeStart = TimeframeStart.AddYears(-5);
                        break;
                    case TIMEFRAME_MAX:
                        TimeframeStart = TimeframeStart.AddYears(-50);
                        break;
                }

                long timestampStart = TimeframeStart.ToUnixTimeSeconds();

                frenchOpenDate = new DateTimeOffset(frenchOpenDate.Value.Year, frenchOpenDate.Value.Month, frenchOpenDate.Value.Day, 23, 59, 59, frenchOpenDate.Value.Offset);
                long timestampEnd = frenchOpenDate.Value.ToUnixTimeSeconds();

                string url = $"https://query1.finance.yahoo.com/v8/finance/chart/{ symbol }?region=FR&lang=fr-FR&includePrePost=false&interval={interval}&useYfid=trued&" +
                             $"corsDomain=fr.finance.yahoo.com&period1={timestampStart}&period2={timestampEnd}";


                if (symbol == "AMZN")
                {

                }
                return url;
            }
        }

        static List<Request> BuildSingleDayRequest(TimeframeSelector timeframes, string symbol, string interval, DateTimeOffset? frenchOpenDate)
        {
            List<Request> requests = new List<Request>();
            int weekdayCursor = -2;

            List<(string timeframeSingleDay, int weekdayCursor)> singleDaysTimeframes = new List<(string timeframeSingleDay, int weekdayCursor)>()
            {
                ("SecondDay", -1),
                ("ThirdDay", -1),
                ("FourthDay", -1),
                ("FifthDay", -1)
            };


            for (int i = 0; i < singleDaysTimeframes.Count; i++)
            {
                weekdayCursor += SetWeekdayCursor(frenchOpenDate, weekdayCursor);

                bool HasFileDownload = (bool) typeof(TimeframeSelector).GetProperty($"Has{singleDaysTimeframes[i].timeframeSingleDay}").GetValue(timeframes);

                if (HasFileDownload)
                {
                    singleDaysTimeframes[i] = (singleDaysTimeframes[i].timeframeSingleDay, weekdayCursor);
                    DateTimeOffset startDay = frenchOpenDate.Value.AddDays(weekdayCursor);
                    string api = BuilSingleDaydUrl(startDay, symbol, interval);
                    requests.Add(new Request() { Timeframe = singleDaysTimeframes[i].timeframeSingleDay, Api = api });
                }

                weekdayCursor--;
            }

            return requests;
        }

        static string BuilSingleDaydUrl(DateTimeOffset startDay, string symbol, string interval)
        {
            double startDayTimestamp = startDay.ToUnixTimeSeconds();
            DateTimeOffset endDay = new DateTimeOffset(startDay.Year, startDay.Month, startDay.Day, 23, 59, 59, startDay.Offset);
            double endDayTimestamp = endDay.ToUnixTimeSeconds();
            string url = $"https://query1.finance.yahoo.com/v8/finance/chart/{ symbol }?region=FR&lang=fr-FR&includePrePost=false&interval={interval}&useYfid=trued&" +
                           $"corsDomain=fr.finance.yahoo.com&period1={startDayTimestamp}&period2={endDayTimestamp}";
            return url;
        }

        static int SetWeekdayCursor(DateTimeOffset? frenchOpenDate, int day)
        {
            if (frenchOpenDate.Value.AddDays(day).DayOfWeek == DayOfWeek.Sunday)
                return -2;

            else if (frenchOpenDate.Value.AddDays(day).DayOfWeek == DayOfWeek.Saturday)
                return -1;

            else return 0;
        }
    }
}
