﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Providers.Yahoo
{
    /// <summary>
    /// Model used with CSV helper
    /// </summary>
    public class NotFound
    {
        public string StockName { get; set; }
    }
}
