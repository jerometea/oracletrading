﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Providers.Yahoo
{
    public class SymbolValidator
    {
        /// <summary>
        /// Windows forbids AUX AUX. CON CON. for folder and file name see more name in link
        /// https://docs.microsoft.com/en-us/windows/win32/fileio/naming-a-file#naming-conventions
        ///  </summary>
        /// <param name="symbol">symbol name to parse</param>
        /// <returns>Symbol name with % separator. Eg: AUX -> AUX%</returns>
        public static string FormatForWindows(string symbol)
        {
            List<string> forbiddenNames = new List<string>{ "CON", "PRN", "AUX", "NUL", "COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8",
                                                            "COM9", "COM0", "LPT1", "LPT2", "LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", "LPT9", "LPT0"};
            var symbolParts = symbol.Split('.');
            var prefixSymbol = symbolParts[0];

            if (forbiddenNames.Contains(prefixSymbol))
            {
                if (symbolParts.Length == 1)
                {
                    return symbol += "%"; // % used like a separator
                }
                else
                {
                    return symbol.Replace(".", "%");
                }
            }
            else
            {
                return symbol;
            }
        }
        /// <summary>
        /// Replaces ^ by %5E in the symbol 
        /// </summary>
        /// <param name="symbol">symbol name to parse</param>
        /// <returns>Symbol without ^ char</returns>
        public static string FormatForRequest(string symbol)
        {
            return symbol.Replace("^", "%5E");
        }
    }

}
