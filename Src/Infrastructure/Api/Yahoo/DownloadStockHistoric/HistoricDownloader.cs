﻿using CsvHelper;
using Infrastructure.Providers.Yahoo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Application.Common.Helpers;
using Application.Common.Models.Yahoo;
using Infrastructure.Api.Yahoo.DownloadStockHistoric;
using Infrastructure.Files.Stocks;
using Application.Common.PathConfigurations;
using Microsoft.Extensions.Options;

namespace Infrastructure.Providers
{
    public class HistoricDownloader : IYahoo
    {
        string _folder;
        private string _historicFolder;
        private string _symbolsFile;

        public HistoricDownloader(IOptions<Folder> folder)
        {
            _folder = folder.Value.Json;
            _historicFolder = $"{ _folder }/HistoricalDatas";
            _symbolsFile = $"{ _folder }/YahooStocks.csv";
        }

        public async Task<Unit> DownloadStockHistoric(HashSet<string> symbolsToDownload, TimeframeSelector timeframes, DateTimeOffset? frenchOpenDate, bool isItForPortfolioExport)
        {
            if (symbolsToDownload == null)
            {
                symbolsToDownload = SymbolReader.OpenCsv(_symbolsFile);
            }

            Dictionary<string, List<Request>> symbols = YahooApi.GenerateRequests(symbolsToDownload, timeframes, frenchOpenDate, isItForPortfolioExport);

            Directory.CreateDirectory(_historicFolder);

            await Download(symbols);

            return Unit.Value;
        }

        async Task<Unit> Download(Dictionary<string, List<Request>> symbols)
        {
            List<Task> tasksDownloads = new List<Task>();

            foreach (KeyValuePair<string, List<Request>> symbol in symbols)
            {
                string symbolName = SymbolValidator.FormatForWindows(symbol.Key);

                string symbolFolder = $"{ _historicFolder }/{ symbolName }";

                Directory.CreateDirectory(symbolFolder);

                foreach (Request request in symbol.Value)
                {
                    if (request.Api != null)
                    {
                        tasksDownloads.Add(Task.Run(() => DownloadHistoric(request, symbolName, symbolFolder)));
                    }
                }
            }

            await Task.WhenAll(tasksDownloads);

            
            return Unit.Value;
        }

        async Task<Unit> DownloadHistoric(Request request, string symbol, string symbolFolder)
        {
            WebClient client = new WebClient();

            string file = $"{ symbolFolder }/{ symbol }_{ request.Timeframe }.json";

            try
            {
                await client.DownloadFileTaskAsync(request.Api, file);
            }
            catch (Exception) { }

            return Unit.Value;
        }

    }

}
