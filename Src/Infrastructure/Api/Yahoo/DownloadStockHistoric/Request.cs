﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Providers.Yahoo
{
    public class Request
    {
        public string Timeframe { get; set; }
        public string Api { get; set; }
    }
}
