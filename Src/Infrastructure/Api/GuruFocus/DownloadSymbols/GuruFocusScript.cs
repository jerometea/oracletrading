﻿using Application.Common.Interfaces;
using Application.Interfaces;
using CsvHelper;
using Domain.Entities;
using HtmlAgilityPack;
using Infrastructure.Providers.GuruFocus;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Infrastructure.Imports.GuruFocus
{
    public class GurufocusScript : IGuruFocus
    {
        string guruFocusStockCsvPath = PathTo.Infrastructure + @"\Providers\CSV\GuruFocusStocks2.csv";
        string pageLog = PathTo.Infrastructure + @"\Providers\CSV\GuruFocus\CSV\";

        string guruTest = PathTo.Infrastructure + @"\Providers\CSV\GuruFocus\CSV\Test\guruTest.csv";
        string pageLogTest = PathTo.Infrastructure + @"\Providers\CSV\GuruFocus\CSV\Test\";

        int symbolPos = 2;
        int stockNamePos = 5;
        int pricePos = 7;

        Regex marketPattern = new Regex(@"(?<=\/stock/)(.*?)(?=\:)");


        public void DownloadSymbols()
        {

            var web = new HtmlWeb();

            for (int i = 1023; i <= 1202; i++) // scrap 1023 pages
            {
                List<CsvStockModel> stocks = new List<CsvStockModel>();

                var url = "https://www.gurufocus.com/stock_list.php?p=" + i + "&n=100";
                var htmlDoc = web.Load(url);

                var stockNodesFromCurrentPage = htmlDoc.DocumentNode.SelectNodes("//tbody/tr");

                foreach (var singleStockNode in stockNodesFromCurrentPage)
                {
                    var stockInformations = singleStockNode.Descendants(0).ToList();
                    var marketHtml = stockInformations[0].ChildNodes[0].Attributes["href"].Value;
                    var symbolAndCountry = stockInformations[symbolPos].InnerHtml.Split(".");

                    var symbol = symbolAndCountry[0];
                    var stockName = stockInformations[stockNamePos].InnerHtml;
                    var market = marketPattern.Match(marketHtml).ToString();
                    var countryName = symbolAndCountry.Length > 1 ? symbolAndCountry[1] : null;

                    var csvStockModel = new CsvStockModel()

                    {
                        Symbol = symbol,
                        StockName = stockName,
                        Market = market,
                        CountryName = countryName
                    };

                    try
                    {
                        var cleanPriceAndCurrency = Regex.Replace(stockInformations[pricePos].InnerHtml, @"\s+", " ");
                        float price = float.Parse(cleanPriceAndCurrency.Split(" ")[1]);
                        var currency = cleanPriceAndCurrency.Split(" ")[0].ToString();

                        csvStockModel.Price = price;
                        csvStockModel.Currency = currency;

                        stocks.Add(csvStockModel);
                    }

                    catch (Exception e)
                    {
                        ExceptionLogger(e, csvStockModel, i);
                    }

                }

                using (var writer = new StreamWriter(guruFocusStockCsvPath, true, Encoding.UTF8))
                using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    csv.WriteRecords(stocks);
                }

                Task.Delay(300);
                Console.WriteLine(i);
            }
        }

        void ExceptionLogger(Exception e, CsvStockModel stock, int page)
        {
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(pageLog, "pages.txt"), true))
            {
                outputFile.WriteLine("page : {0}, {1} {2} {3} {4} {5} {6}, {7}",
                    page,
                    stock.Symbol, stock.StockName, stock.Market, stock.CountryName, stock.Price, stock.Currency,
                    e.GetType());
            }
        }
    }

}
