﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Infrastructure.Providers.GuruFocus
{
    public class PathTo
    {
        static string PresentationFolder = Environment.CurrentDirectory;

        static string SrcFolder = Directory.GetParent(PresentationFolder).FullName;

        public static string Infrastructure { get => SrcFolder + @"\Infrastructure\"; }
    }
}
