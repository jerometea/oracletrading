﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Imports.GuruFocus
{
    class CsvStockModel
    {
        public string Symbol { get; set; }
        public string StockName { get; set; }
        public string Market { get; set; }
        public string CountryName { get; set; }
        public float Price { get; set; }
        public string Currency { get; set; }
    }
}
