﻿using Domain.Entities;
using Domain.Entities.Simulator;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Infrastructure.Persistence.Configurations
{
    public static class ColumnSettings
    {
        public static ModelBuilder AllowNullForeignKeys(this ModelBuilder builder)
        {
            builder.Entity<Exchange>()
                .HasOne(exchange => exchange.Timezone)
                .WithMany(timezone => timezone.Exchanges)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<Stock>()
                .HasOne(stock => stock.Exchange)
                .WithMany(exchange => exchange.Stocks)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<Stock>()
                .HasOne(stock => stock.Category)
                .WithMany(category => category.Stocks)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<Stock>()
                .HasOne(stock => stock.Currency)
                .WithMany(currency => currency.Stocks)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<Stock>()
                .HasOne(stock => stock.Provider)
                .WithMany(provider => provider.Stocks)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<Statistic>()
                .HasOne(stat => stat.RegressionSubtype)
                .WithMany(regressionSubtype => regressionSubtype.Statistics)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<Statistic>()
                .HasOne(stat => stat.Stock)
                .WithMany(stock => stock.Statistics)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<StatisticPosition>()
                .HasOne(statisticPosition => statisticPosition.Position)
                .WithMany(position => position.StatisticsArchives)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<Position>()
                    .HasOne(position => position.Portfolio)
                    .WithMany(portfolio => portfolio.Positions)
                    .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<Change>()
                    .HasOne(change => change.Statistic)
                    .WithMany(stat => stat.Changes)
                    .OnDelete(DeleteBehavior.SetNull);

            return builder;
        }

        public static ModelBuilder StoreUniqueNames(this ModelBuilder builder)
        {
            builder.Entity<Timezone>().HasIndex(timezone => timezone.TimezoneName).IsUnique();

            builder.Entity<Exchange>().HasIndex(exchange => exchange.ExchangeName).IsUnique();
            builder.Entity<Exchange>().HasIndex(exchange => exchange.ExchangeShortName).IsUnique();

            builder.Entity<Category>().HasIndex(category => category.CategoryName).IsUnique();

            builder.Entity<Provider>().HasIndex(provider => provider.ProviderName).IsUnique();

            builder.Entity<Currency>().HasIndex(currency => currency.CurrencyName).IsUnique();
            builder.Entity<Currency>().HasIndex(currency => currency.CurrencyShortName).IsUnique();

            builder.Entity<Stock>().HasIndex(s => s.Symbol).IsUnique();

            builder.Entity<RegressionSubtype>().HasIndex(regressionSubtype => regressionSubtype.RegressionSubtypeName).IsUnique();

            builder.Entity<Timeframe>().HasIndex(period => period.TimeframeName).IsUnique();

            return builder;
        }
    }
}
