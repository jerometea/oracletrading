﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace Infrastructure.DataAccess
{
    using Infrastructure.Persistence;
    using Microsoft.EntityFrameworkCore;
    using System;

    public class VIsionTradingDbContextFactory : IDesignTimeDbContextFactory<VisionTradingDbContext>
    {
        public VisionTradingDbContext CreateDbContext(string[] args)
        {

            string connectionString = ReadDefaultConnectionStringFromAppSettings();

            var builder = new DbContextOptionsBuilder<VisionTradingDbContext>();
            builder.UseSqlServer(connectionString);
            return new VisionTradingDbContext(builder.Options);
        }

        string ReadDefaultConnectionStringFromAppSettings()
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(Environment.CurrentDirectory) + "/Presentation")
                .AddJsonFile("appsettings.json")
                .Build();

            string connectionString = configuration.GetValue<string>("ConnectionStrings:VisionTradingConnection");

            return connectionString;

        }
    }
}
