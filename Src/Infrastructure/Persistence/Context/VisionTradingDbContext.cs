﻿using Domain.Entities;
using Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Application.Common.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using Domain.Common;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Domain.Entities.Simulator;

namespace Infrastructure.Persistence
{
    public class VisionTradingDbContext : DbContext, IVisionTradingDbContext
    {
        public VisionTradingDbContext(DbContextOptions<VisionTradingDbContext> opt) : base(opt)
        {
            
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.AllowNullForeignKeys()
                        .StoreUniqueNames();
        }

        public DbSet<Category> Category { get; set; }
        public DbSet<Currency> Currency { get; set; }
        public DbSet<Exchange> Exchange { get; set; }
        public DbSet<Timeframe> Timeframe { get; set; }
        public DbSet<Provider> Provider { get; set; }
        public DbSet<Regression> Regression { get; set; }
        public DbSet<RegressionSubtype> RegressionSubtype { get; set; }
        public DbSet<Statistic> Statistic { get; set; }
        public DbSet<StatisticPosition> StatisticPosition { get; set; }
        public DbSet<Stock> Stock { get; set; }
        public DbSet<Timezone> Timezone { get; set; }
        public DbSet<Position> Position { get; set; }
        public DbSet<Portfolio> Portfolio { get; set; }
        public DbSet<Change> Change { get; set; }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken )
        {
            foreach (EntityEntry<AuditableEntity> entry in ChangeTracker.Entries<AuditableEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.Created = DateTimeOffset.Now;
                        break;
                    case EntityState.Modified:
                        entry.Entity.LastModified = DateTimeOffset.Now;
                        break;
                }
            }

            return base.SaveChangesAsync(cancellationToken);
        }
    }
}
