﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class allow_set_null_PositionId_for_statisticArchive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StatisticArchive_Position_PositionId",
                table: "StatisticArchive");

            migrationBuilder.AddForeignKey(
                name: "FK_StatisticArchive_Position_PositionId",
                table: "StatisticArchive",
                column: "PositionId",
                principalTable: "Position",
                principalColumn: "PositionId",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StatisticArchive_Position_PositionId",
                table: "StatisticArchive");

            migrationBuilder.AddForeignKey(
                name: "FK_StatisticArchive_Position_PositionId",
                table: "StatisticArchive",
                column: "PositionId",
                principalTable: "Position",
                principalColumn: "PositionId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
