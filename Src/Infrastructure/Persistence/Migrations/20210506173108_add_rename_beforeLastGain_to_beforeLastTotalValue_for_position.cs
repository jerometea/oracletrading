﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class add_rename_beforeLastGain_to_beforeLastTotalValue_for_position : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "BeforeLastGain",
                table: "Position",
                newName: "BeforeLastTotalValue");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "BeforeLastTotalValue",
                table: "Position",
                newName: "BeforeLastGain");
        }
    }
}
