﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class add_variation_entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "VariationInterval",
                columns: table => new
                {
                    VariationIntervalId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    VariationInternvalName = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VariationInterval", x => x.VariationIntervalId);
                });

            migrationBuilder.CreateTable(
                name: "Variation",
                columns: table => new
                {
                    VariationId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Percent = table.Column<double>(type: "float", nullable: false),
                    VariationIntervalId = table.Column<int>(type: "int", nullable: true),
                    StatisticId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Variation", x => x.VariationId);
                    table.ForeignKey(
                        name: "FK_Variation_Statistic_StatisticId",
                        column: x => x.StatisticId,
                        principalTable: "Statistic",
                        principalColumn: "StatisticId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Variation_VariationInterval_VariationIntervalId",
                        column: x => x.VariationIntervalId,
                        principalTable: "VariationInterval",
                        principalColumn: "VariationIntervalId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Variation_StatisticId",
                table: "Variation",
                column: "StatisticId");

            migrationBuilder.CreateIndex(
                name: "IX_Variation_VariationIntervalId",
                table: "Variation",
                column: "VariationIntervalId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Variation");

            migrationBuilder.DropTable(
                name: "VariationInterval");
        }
    }
}
