﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class remove_position_from_regressionSubtype_and_rename_date_for_portfolio : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Position_RegressionSubtype_RegressionSubtypeId",
                table: "Position");

            migrationBuilder.DropIndex(
                name: "IX_Position_RegressionSubtypeId",
                table: "Position");

            migrationBuilder.DropColumn(
                name: "RegressionSubtypeId",
                table: "Position");

            migrationBuilder.RenameColumn(
                name: "LastUpdate",
                table: "Portfolio",
                newName: "Update");

            migrationBuilder.RenameColumn(
                name: "Invest",
                table: "Portfolio",
                newName: "Investment");

            migrationBuilder.RenameColumn(
                name: "FrenchOpenDate",
                table: "Portfolio",
                newName: "OpenDate");

            migrationBuilder.AddColumn<int>(
                name: "Days",
                table: "Portfolio",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Days",
                table: "Portfolio");

            migrationBuilder.RenameColumn(
                name: "Update",
                table: "Portfolio",
                newName: "LastUpdate");

            migrationBuilder.RenameColumn(
                name: "OpenDate",
                table: "Portfolio",
                newName: "FrenchOpenDate");

            migrationBuilder.RenameColumn(
                name: "Investment",
                table: "Portfolio",
                newName: "Invest");

            migrationBuilder.AddColumn<int>(
                name: "RegressionSubtypeId",
                table: "Position",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Position_RegressionSubtypeId",
                table: "Position",
                column: "RegressionSubtypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Position_RegressionSubtype_RegressionSubtypeId",
                table: "Position",
                column: "RegressionSubtypeId",
                principalTable: "RegressionSubtype",
                principalColumn: "RegressionSubtypeId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
