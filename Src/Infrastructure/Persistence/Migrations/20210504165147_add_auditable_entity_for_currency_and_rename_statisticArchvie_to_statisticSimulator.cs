﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class add_auditable_entity_for_currency_and_rename_statisticArchvie_to_statisticSimulator : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "StatisticArchiveId",
                table: "StatisticArchive",
                newName: "StatisticSimulatorId");

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "Created",
                table: "Currency",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "LastModified",
                table: "Currency",
                type: "datetimeoffset",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Created",
                table: "Currency");

            migrationBuilder.DropColumn(
                name: "LastModified",
                table: "Currency");

            migrationBuilder.RenameColumn(
                name: "StatisticSimulatorId",
                table: "StatisticArchive",
                newName: "StatisticArchiveId");
        }
    }
}
