﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class add_dates_for_position : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CloseDate",
                table: "Position");

            migrationBuilder.DropColumn(
                name: "OpenDate",
                table: "Position");

            migrationBuilder.DropColumn(
                name: "PositionLastUpdate",
                table: "Position");

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "CloseForeignDate",
                table: "Position",
                type: "datetimeoffset",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "CloseFrenchDate",
                table: "Position",
                type: "datetimeoffset",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "LastForeignUpdateDate",
                table: "Position",
                type: "datetimeoffset",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "LastFrenchUpdateDate",
                table: "Position",
                type: "datetimeoffset",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "OpenForeignDate",
                table: "Position",
                type: "datetimeoffset",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "OpenFrenchDate",
                table: "Position",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CloseForeignDate",
                table: "Position");

            migrationBuilder.DropColumn(
                name: "CloseFrenchDate",
                table: "Position");

            migrationBuilder.DropColumn(
                name: "LastForeignUpdateDate",
                table: "Position");

            migrationBuilder.DropColumn(
                name: "LastFrenchUpdateDate",
                table: "Position");

            migrationBuilder.DropColumn(
                name: "OpenForeignDate",
                table: "Position");

            migrationBuilder.DropColumn(
                name: "OpenFrenchDate",
                table: "Position");

            migrationBuilder.AddColumn<DateTime>(
                name: "CloseDate",
                table: "Position",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "OpenDate",
                table: "Position",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "PositionLastUpdate",
                table: "Position",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
