﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class add_IsRealTime_for_portfolio : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsOpen",
                table: "Position");

            migrationBuilder.AddColumn<bool>(
                name: "IsInRealTime",
                table: "Portfolio",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsInRealTime",
                table: "Portfolio");

            migrationBuilder.AddColumn<bool>(
                name: "IsOpen",
                table: "Position",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
