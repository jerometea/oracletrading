﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class add_regression_and_regressionSubtype : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Statistic_Reference_ReferenceId",
                table: "Statistic");

            migrationBuilder.DropTable(
                name: "PositionReference");

            migrationBuilder.DropTable(
                name: "Reference");

            migrationBuilder.DropTable(
                name: "Period");

            migrationBuilder.RenameColumn(
                name: "ReferenceId",
                table: "Statistic",
                newName: "TimeframeId");

            migrationBuilder.RenameIndex(
                name: "IX_Statistic_ReferenceId",
                table: "Statistic",
                newName: "IX_Statistic_TimeframeId");

            migrationBuilder.AddColumn<int>(
                name: "RegressionSubtypeId",
                table: "Statistic",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Regression",
                columns: table => new
                {
                    RegressionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RegressionName = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    HasPositiveCoefficient = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Regression", x => x.RegressionId);
                });

            migrationBuilder.CreateTable(
                name: "Timeframe",
                columns: table => new
                {
                    TimeframeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TimeframeName = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Timeframe", x => x.TimeframeId);
                });

            migrationBuilder.CreateTable(
                name: "RegressionSubtype",
                columns: table => new
                {
                    RegressionSubtypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RegressionSubtypeName = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    FirstY = table.Column<int>(type: "int", nullable: true),
                    LastY = table.Column<int>(type: "int", nullable: true),
                    RegressionId = table.Column<int>(type: "int", nullable: true),
                    Symbol = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RegressionSubtype", x => x.RegressionSubtypeId);
                    table.ForeignKey(
                        name: "FK_RegressionSubtype_Regression_RegressionId",
                        column: x => x.RegressionId,
                        principalTable: "Regression",
                        principalColumn: "RegressionId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PositionRegressionSubtype",
                columns: table => new
                {
                    PositionsPositionId = table.Column<int>(type: "int", nullable: false),
                    RegressionSubtypesRegressionSubtypeId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PositionRegressionSubtype", x => new { x.PositionsPositionId, x.RegressionSubtypesRegressionSubtypeId });
                    table.ForeignKey(
                        name: "FK_PositionRegressionSubtype_Position_PositionsPositionId",
                        column: x => x.PositionsPositionId,
                        principalTable: "Position",
                        principalColumn: "PositionId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PositionRegressionSubtype_RegressionSubtype_RegressionSubtypesRegressionSubtypeId",
                        column: x => x.RegressionSubtypesRegressionSubtypeId,
                        principalTable: "RegressionSubtype",
                        principalColumn: "RegressionSubtypeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Statistic_RegressionSubtypeId",
                table: "Statistic",
                column: "RegressionSubtypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PositionRegressionSubtype_RegressionSubtypesRegressionSubtypeId",
                table: "PositionRegressionSubtype",
                column: "RegressionSubtypesRegressionSubtypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Regression_RegressionName",
                table: "Regression",
                column: "RegressionName",
                unique: true,
                filter: "[RegressionName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_RegressionSubtype_RegressionId",
                table: "RegressionSubtype",
                column: "RegressionId");

            migrationBuilder.CreateIndex(
                name: "IX_RegressionSubtype_RegressionSubtypeName",
                table: "RegressionSubtype",
                column: "RegressionSubtypeName",
                unique: true,
                filter: "[RegressionSubtypeName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Timeframe_TimeframeName",
                table: "Timeframe",
                column: "TimeframeName",
                unique: true,
                filter: "[TimeframeName] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Statistic_RegressionSubtype_RegressionSubtypeId",
                table: "Statistic",
                column: "RegressionSubtypeId",
                principalTable: "RegressionSubtype",
                principalColumn: "RegressionSubtypeId",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Statistic_Timeframe_TimeframeId",
                table: "Statistic",
                column: "TimeframeId",
                principalTable: "Timeframe",
                principalColumn: "TimeframeId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Statistic_RegressionSubtype_RegressionSubtypeId",
                table: "Statistic");

            migrationBuilder.DropForeignKey(
                name: "FK_Statistic_Timeframe_TimeframeId",
                table: "Statistic");

            migrationBuilder.DropTable(
                name: "PositionRegressionSubtype");

            migrationBuilder.DropTable(
                name: "Timeframe");

            migrationBuilder.DropTable(
                name: "RegressionSubtype");

            migrationBuilder.DropTable(
                name: "Regression");

            migrationBuilder.DropIndex(
                name: "IX_Statistic_RegressionSubtypeId",
                table: "Statistic");

            migrationBuilder.DropColumn(
                name: "RegressionSubtypeId",
                table: "Statistic");

            migrationBuilder.RenameColumn(
                name: "TimeframeId",
                table: "Statistic",
                newName: "ReferenceId");

            migrationBuilder.RenameIndex(
                name: "IX_Statistic_TimeframeId",
                table: "Statistic",
                newName: "IX_Statistic_ReferenceId");

            migrationBuilder.CreateTable(
                name: "Period",
                columns: table => new
                {
                    PeriodId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PeriodName = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Period", x => x.PeriodId);
                });

            migrationBuilder.CreateTable(
                name: "Reference",
                columns: table => new
                {
                    ReferenceId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PeriodId = table.Column<int>(type: "int", nullable: true),
                    ReferenceName = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reference", x => x.ReferenceId);
                    table.ForeignKey(
                        name: "FK_Reference_Period_PeriodId",
                        column: x => x.PeriodId,
                        principalTable: "Period",
                        principalColumn: "PeriodId",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "PositionReference",
                columns: table => new
                {
                    PositionsPositionId = table.Column<int>(type: "int", nullable: false),
                    ReferencesReferenceId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PositionReference", x => new { x.PositionsPositionId, x.ReferencesReferenceId });
                    table.ForeignKey(
                        name: "FK_PositionReference_Position_PositionsPositionId",
                        column: x => x.PositionsPositionId,
                        principalTable: "Position",
                        principalColumn: "PositionId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PositionReference_Reference_ReferencesReferenceId",
                        column: x => x.ReferencesReferenceId,
                        principalTable: "Reference",
                        principalColumn: "ReferenceId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Period_PeriodName",
                table: "Period",
                column: "PeriodName",
                unique: true,
                filter: "[PeriodName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_PositionReference_ReferencesReferenceId",
                table: "PositionReference",
                column: "ReferencesReferenceId");

            migrationBuilder.CreateIndex(
                name: "IX_Reference_PeriodId",
                table: "Reference",
                column: "PeriodId");

            migrationBuilder.CreateIndex(
                name: "IX_Reference_ReferenceName",
                table: "Reference",
                column: "ReferenceName",
                unique: true,
                filter: "[ReferenceName] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Statistic_Reference_ReferenceId",
                table: "Statistic",
                column: "ReferenceId",
                principalTable: "Reference",
                principalColumn: "ReferenceId",
                onDelete: ReferentialAction.SetNull);
        }
    }
}
