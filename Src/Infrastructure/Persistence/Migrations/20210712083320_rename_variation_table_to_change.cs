﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class rename_variation_table_to_change : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Variation");

            migrationBuilder.RenameColumn(
                name: "Variation",
                table: "StatisticPosition",
                newName: "Change");

            migrationBuilder.RenameColumn(
                name: "Variation",
                table: "Statistic",
                newName: "Change");

            migrationBuilder.CreateTable(
                name: "Change",
                columns: table => new
                {
                    ChangeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Percent = table.Column<double>(type: "float", nullable: false),
                    Range = table.Column<int>(type: "int", nullable: false),
                    HistoricStartRangeIndex = table.Column<int>(type: "int", nullable: false),
                    HistoricEndRangeIndex = table.Column<int>(type: "int", nullable: false),
                    StatisticId = table.Column<int>(type: "int", nullable: true),
                    PeakPrice = table.Column<double>(type: "float", nullable: false),
                    EndRangePrice = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Change", x => x.ChangeId);
                    table.ForeignKey(
                        name: "FK_Change_Statistic_StatisticId",
                        column: x => x.StatisticId,
                        principalTable: "Statistic",
                        principalColumn: "StatisticId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Change_StatisticId",
                table: "Change",
                column: "StatisticId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Change");

            migrationBuilder.RenameColumn(
                name: "Change",
                table: "StatisticPosition",
                newName: "Variation");

            migrationBuilder.RenameColumn(
                name: "Change",
                table: "Statistic",
                newName: "Variation");

            migrationBuilder.CreateTable(
                name: "Variation",
                columns: table => new
                {
                    VariationId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Day = table.Column<int>(type: "int", nullable: false),
                    Percent = table.Column<double>(type: "float", nullable: false),
                    StatisticId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Variation", x => x.VariationId);
                    table.ForeignKey(
                        name: "FK_Variation_Statistic_StatisticId",
                        column: x => x.StatisticId,
                        principalTable: "Statistic",
                        principalColumn: "StatisticId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Variation_StatisticId",
                table: "Variation",
                column: "StatisticId");
        }
    }
}
