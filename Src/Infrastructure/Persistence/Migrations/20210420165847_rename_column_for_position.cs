﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class rename_column_for_position : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TotalPercentGain",
                table: "Position",
                newName: "TotalValue");

            migrationBuilder.RenameColumn(
                name: "TotalDays",
                table: "Position",
                newName: "Days");

            migrationBuilder.RenameColumn(
                name: "OpenFrenchDate",
                table: "Position",
                newName: "OpenDate");

            migrationBuilder.RenameColumn(
                name: "LocalPurchasePrice",
                table: "Position",
                newName: "TodayChange");

            migrationBuilder.RenameColumn(
                name: "LocalCurrentPrice",
                table: "Position",
                newName: "PurchasePrice");

            migrationBuilder.RenameColumn(
                name: "LastFrenchUpdateDate",
                table: "Position",
                newName: "LastUpdateDate");

            migrationBuilder.RenameColumn(
                name: "EuroTotalValue",
                table: "Position",
                newName: "PercentGain");

            migrationBuilder.RenameColumn(
                name: "EuroTotalGain",
                table: "Position",
                newName: "Investment");

            migrationBuilder.RenameColumn(
                name: "EuroTodayChange",
                table: "Position",
                newName: "Gain");

            migrationBuilder.RenameColumn(
                name: "EuroPurchasePrice",
                table: "Position",
                newName: "ForeignPurchasePrice");

            migrationBuilder.RenameColumn(
                name: "EuroCurrentPrice",
                table: "Position",
                newName: "ForeignCurrentPrice");

            migrationBuilder.RenameColumn(
                name: "CloseFrenchDate",
                table: "Position",
                newName: "CloseDate");

            migrationBuilder.AddColumn<double>(
                name: "CurrentPrice",
                table: "Position",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CurrentPrice",
                table: "Position");

            migrationBuilder.RenameColumn(
                name: "TotalValue",
                table: "Position",
                newName: "TotalPercentGain");

            migrationBuilder.RenameColumn(
                name: "TodayChange",
                table: "Position",
                newName: "LocalPurchasePrice");

            migrationBuilder.RenameColumn(
                name: "PurchasePrice",
                table: "Position",
                newName: "LocalCurrentPrice");

            migrationBuilder.RenameColumn(
                name: "PercentGain",
                table: "Position",
                newName: "EuroTotalValue");

            migrationBuilder.RenameColumn(
                name: "OpenDate",
                table: "Position",
                newName: "OpenFrenchDate");

            migrationBuilder.RenameColumn(
                name: "LastUpdateDate",
                table: "Position",
                newName: "LastFrenchUpdateDate");

            migrationBuilder.RenameColumn(
                name: "Investment",
                table: "Position",
                newName: "EuroTotalGain");

            migrationBuilder.RenameColumn(
                name: "Gain",
                table: "Position",
                newName: "EuroTodayChange");

            migrationBuilder.RenameColumn(
                name: "ForeignPurchasePrice",
                table: "Position",
                newName: "EuroPurchasePrice");

            migrationBuilder.RenameColumn(
                name: "ForeignCurrentPrice",
                table: "Position",
                newName: "EuroCurrentPrice");

            migrationBuilder.RenameColumn(
                name: "Days",
                table: "Position",
                newName: "TotalDays");

            migrationBuilder.RenameColumn(
                name: "CloseDate",
                table: "Position",
                newName: "CloseFrenchDate");
        }
    }
}
