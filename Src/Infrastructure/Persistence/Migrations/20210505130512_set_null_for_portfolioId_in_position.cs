﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class set_null_for_portfolioId_in_position : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Position_Portfolio_PortfolioId",
                table: "Position");

            migrationBuilder.AddColumn<double>(
                name: "BeforeLastGain",
                table: "Position",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<DateTime>(
                name: "BeforeLastUpdate",
                table: "Position",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddForeignKey(
                name: "FK_Position_Portfolio_PortfolioId",
                table: "Position",
                column: "PortfolioId",
                principalTable: "Portfolio",
                principalColumn: "PortfolioId",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Position_Portfolio_PortfolioId",
                table: "Position");

            migrationBuilder.DropColumn(
                name: "BeforeLastGain",
                table: "Position");

            migrationBuilder.DropColumn(
                name: "BeforeLastUpdate",
                table: "Position");

            migrationBuilder.AddForeignKey(
                name: "FK_Position_Portfolio_PortfolioId",
                table: "Position",
                column: "PortfolioId",
                principalTable: "Portfolio",
                principalColumn: "PortfolioId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
