﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class beforeLastTotalValue_for_portfolio : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "BeforeLastGain",
                table: "Portfolio",
                newName: "BeforeLastTotalValue");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "BeforeLastTotalValue",
                table: "Portfolio",
                newName: "BeforeLastGain");
        }
    }
}
