﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class allow_duplicate_name_for_regression : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Regression_RegressionName",
                table: "Regression");

            migrationBuilder.AlterColumn<string>(
                name: "RegressionName",
                table: "Regression",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "RegressionName",
                table: "Regression",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Regression_RegressionName",
                table: "Regression",
                column: "RegressionName",
                unique: true,
                filter: "[RegressionName] IS NOT NULL");
        }
    }
}
