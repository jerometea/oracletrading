﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class add_position : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Position",
                columns: table => new
                {
                    PositionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StockId = table.Column<int>(type: "int", nullable: true),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    PurchasePrice = table.Column<double>(type: "float", nullable: false),
                    CurrentPrice = table.Column<double>(type: "float", nullable: false),
                    TotalValue = table.Column<double>(type: "float", nullable: false),
                    TodayChange = table.Column<double>(type: "float", nullable: false),
                    TodayPercentChange = table.Column<double>(type: "float", nullable: false),
                    TotalGain = table.Column<double>(type: "float", nullable: false),
                    TotalPercentGain = table.Column<double>(type: "float", nullable: false),
                    IsOpen = table.Column<bool>(type: "bit", nullable: false),
                    OpenDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CloseDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    PositionLastUpdate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Position", x => x.PositionId);
                    table.ForeignKey(
                        name: "FK_Position_Stock_StockId",
                        column: x => x.StockId,
                        principalTable: "Stock",
                        principalColumn: "StockId",
                        onDelete: ReferentialAction.Restrict);
                });


            migrationBuilder.CreateTable(
                name: "PositionReference",
                columns: table => new
                {
                    PositionsPositionId = table.Column<int>(type: "int", nullable: false),
                    ReferencesReferenceId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PositionReference", x => new { x.PositionsPositionId, x.ReferencesReferenceId });
                    table.ForeignKey(
                        name: "FK_PositionReference_Position_PositionsPositionId",
                        column: x => x.PositionsPositionId,
                        principalTable: "Position",
                        principalColumn: "PositionId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PositionReference_Reference_ReferencesReferenceId",
                        column: x => x.ReferencesReferenceId,
                        principalTable: "Reference",
                        principalColumn: "ReferenceId",
                        onDelete: ReferentialAction.Cascade);
                });


            migrationBuilder.CreateIndex(
                name: "IX_Position_StockId",
                table: "Position",
                column: "StockId");

            migrationBuilder.CreateIndex(
                name: "IX_PositionReference_ReferencesReferenceId",
                table: "PositionReference",
                column: "ReferencesReferenceId");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PositionReference");

           
            migrationBuilder.DropTable(
                name: "Position");

        }
    }
}
