﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class add_before_last_for_portfolio_and_position_and_rename_statisticArchive_to_statisticPosition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StatisticArchive");

            migrationBuilder.AddColumn<double>(
                name: "BeforeLastGain",
                table: "Portfolio",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<DateTime>(
                name: "BeforeLastUpdate",
                table: "Portfolio",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "LastUpdate",
                table: "Portfolio",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.CreateTable(
                name: "StatisticPosition",
                columns: table => new
                {
                    StatisticPositionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StockId = table.Column<int>(type: "int", nullable: true),
                    RegressionSubtypeId = table.Column<int>(type: "int", nullable: true),
                    Score = table.Column<double>(type: "float", nullable: false),
                    Integrity = table.Column<double>(type: "float", nullable: false),
                    TotalValues = table.Column<int>(type: "int", nullable: false),
                    TimeframeId = table.Column<int>(type: "int", nullable: true),
                    PositionId = table.Column<int>(type: "int", nullable: true),
                    Created = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    LastModified = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StatisticPosition", x => x.StatisticPositionId);
                    table.ForeignKey(
                        name: "FK_StatisticPosition_Position_PositionId",
                        column: x => x.PositionId,
                        principalTable: "Position",
                        principalColumn: "PositionId",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_StatisticPosition_RegressionSubtype_RegressionSubtypeId",
                        column: x => x.RegressionSubtypeId,
                        principalTable: "RegressionSubtype",
                        principalColumn: "RegressionSubtypeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StatisticPosition_Stock_StockId",
                        column: x => x.StockId,
                        principalTable: "Stock",
                        principalColumn: "StockId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StatisticPosition_Timeframe_TimeframeId",
                        column: x => x.TimeframeId,
                        principalTable: "Timeframe",
                        principalColumn: "TimeframeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StatisticPosition_PositionId",
                table: "StatisticPosition",
                column: "PositionId");

            migrationBuilder.CreateIndex(
                name: "IX_StatisticPosition_RegressionSubtypeId",
                table: "StatisticPosition",
                column: "RegressionSubtypeId");

            migrationBuilder.CreateIndex(
                name: "IX_StatisticPosition_StockId",
                table: "StatisticPosition",
                column: "StockId");

            migrationBuilder.CreateIndex(
                name: "IX_StatisticPosition_TimeframeId",
                table: "StatisticPosition",
                column: "TimeframeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StatisticPosition");

            migrationBuilder.DropColumn(
                name: "BeforeLastGain",
                table: "Portfolio");

            migrationBuilder.DropColumn(
                name: "BeforeLastUpdate",
                table: "Portfolio");

            migrationBuilder.DropColumn(
                name: "LastUpdate",
                table: "Portfolio");

            migrationBuilder.CreateTable(
                name: "StatisticArchive",
                columns: table => new
                {
                    StatisticSimulatorId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Created = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    Integrity = table.Column<double>(type: "float", nullable: false),
                    LastModified = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    PositionId = table.Column<int>(type: "int", nullable: true),
                    RegressionSubtypeId = table.Column<int>(type: "int", nullable: true),
                    Score = table.Column<double>(type: "float", nullable: false),
                    StockId = table.Column<int>(type: "int", nullable: true),
                    TimeframeId = table.Column<int>(type: "int", nullable: true),
                    TotalValues = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StatisticArchive", x => x.StatisticSimulatorId);
                    table.ForeignKey(
                        name: "FK_StatisticArchive_Position_PositionId",
                        column: x => x.PositionId,
                        principalTable: "Position",
                        principalColumn: "PositionId",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_StatisticArchive_RegressionSubtype_RegressionSubtypeId",
                        column: x => x.RegressionSubtypeId,
                        principalTable: "RegressionSubtype",
                        principalColumn: "RegressionSubtypeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StatisticArchive_Stock_StockId",
                        column: x => x.StockId,
                        principalTable: "Stock",
                        principalColumn: "StockId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StatisticArchive_Timeframe_TimeframeId",
                        column: x => x.TimeframeId,
                        principalTable: "Timeframe",
                        principalColumn: "TimeframeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StatisticArchive_PositionId",
                table: "StatisticArchive",
                column: "PositionId");

            migrationBuilder.CreateIndex(
                name: "IX_StatisticArchive_RegressionSubtypeId",
                table: "StatisticArchive",
                column: "RegressionSubtypeId");

            migrationBuilder.CreateIndex(
                name: "IX_StatisticArchive_StockId",
                table: "StatisticArchive",
                column: "StockId");

            migrationBuilder.CreateIndex(
                name: "IX_StatisticArchive_TimeframeId",
                table: "StatisticArchive",
                column: "TimeframeId");
        }
    }
}
