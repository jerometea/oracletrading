﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class add_portfolio : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FrenchCloseDate",
                table: "Position");

            migrationBuilder.DropColumn(
                name: "FrenchLastUpdateDate",
                table: "Position");

            migrationBuilder.DropColumn(
                name: "FrenchOpenDate",
                table: "Position");

            migrationBuilder.RenameColumn(
                name: "GainPurcentage",
                table: "Position",
                newName: "GainPercentage");

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "ForeignOpenDate",
                table: "Position",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)),
                oldClrType: typeof(DateTimeOffset),
                oldType: "datetimeoffset",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "ForeignLastUpdateDate",
                table: "Position",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)),
                oldClrType: typeof(DateTimeOffset),
                oldType: "datetimeoffset",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PortfolioId",
                table: "Position",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Portfolio",
                columns: table => new
                {
                    PortfolioId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Comment = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Creation = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    LastUpdate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    TodayChange = table.Column<double>(type: "float", nullable: false),
                    TodayChangePercentage = table.Column<double>(type: "float", nullable: false),
                    Gain = table.Column<double>(type: "float", nullable: false),
                    GainPercentage = table.Column<double>(type: "float", nullable: false),
                    Invest = table.Column<double>(type: "float", nullable: false),
                    TotalValue = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Portfolio", x => x.PortfolioId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Position_PortfolioId",
                table: "Position",
                column: "PortfolioId");

            migrationBuilder.AddForeignKey(
                name: "FK_Position_Portfolio_PortfolioId",
                table: "Position",
                column: "PortfolioId",
                principalTable: "Portfolio",
                principalColumn: "PortfolioId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Position_Portfolio_PortfolioId",
                table: "Position");

            migrationBuilder.DropTable(
                name: "Portfolio");

            migrationBuilder.DropIndex(
                name: "IX_Position_PortfolioId",
                table: "Position");

            migrationBuilder.DropColumn(
                name: "PortfolioId",
                table: "Position");

            migrationBuilder.RenameColumn(
                name: "GainPercentage",
                table: "Position",
                newName: "GainPurcentage");

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "ForeignOpenDate",
                table: "Position",
                type: "datetimeoffset",
                nullable: true,
                oldClrType: typeof(DateTimeOffset),
                oldType: "datetimeoffset");

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "ForeignLastUpdateDate",
                table: "Position",
                type: "datetimeoffset",
                nullable: true,
                oldClrType: typeof(DateTimeOffset),
                oldType: "datetimeoffset");

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "FrenchCloseDate",
                table: "Position",
                type: "datetimeoffset",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "FrenchLastUpdateDate",
                table: "Position",
                type: "datetimeoffset",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "FrenchOpenDate",
                table: "Position",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));
        }
    }
}
