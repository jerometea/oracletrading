﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class allow_null_for_FK_StatisticId_in_table_change : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Change_Statistic_StatisticId",
                table: "Change");

            migrationBuilder.AddForeignKey(
                name: "FK_Change_Statistic_StatisticId",
                table: "Change",
                column: "StatisticId",
                principalTable: "Statistic",
                principalColumn: "StatisticId",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Change_Statistic_StatisticId",
                table: "Change");

            migrationBuilder.AddForeignKey(
                name: "FK_Change_Statistic_StatisticId",
                table: "Change",
                column: "StatisticId",
                principalTable: "Statistic",
                principalColumn: "StatisticId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
