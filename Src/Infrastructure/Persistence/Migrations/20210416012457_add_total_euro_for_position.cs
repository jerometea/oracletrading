﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class add_total_euro_for_position : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TotalValue",
                table: "Position",
                newName: "EuroTotalValue");

            migrationBuilder.RenameColumn(
                name: "TotalGain",
                table: "Position",
                newName: "EuroTotalGain");

            migrationBuilder.RenameColumn(
                name: "TodayChange",
                table: "Position",
                newName: "EuroTodayChange");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EuroTotalValue",
                table: "Position",
                newName: "TotalValue");

            migrationBuilder.RenameColumn(
                name: "EuroTotalGain",
                table: "Position",
                newName: "TotalGain");

            migrationBuilder.RenameColumn(
                name: "EuroTodayChange",
                table: "Position",
                newName: "TodayChange");
        }
    }
}
