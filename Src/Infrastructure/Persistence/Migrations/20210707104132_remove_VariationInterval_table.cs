﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class remove_VariationInterval_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Variation_VariationInterval_VariationIntervalId",
                table: "Variation");

            migrationBuilder.DropTable(
                name: "VariationInterval");

            migrationBuilder.DropIndex(
                name: "IX_Variation_VariationIntervalId",
                table: "Variation");

            migrationBuilder.DropColumn(
                name: "VariationIntervalId",
                table: "Variation");

            migrationBuilder.AddColumn<int>(
                name: "Day",
                table: "Variation",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Day",
                table: "Variation");

            migrationBuilder.AddColumn<int>(
                name: "VariationIntervalId",
                table: "Variation",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "VariationInterval",
                columns: table => new
                {
                    VariationIntervalId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    VariationInternvalName = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VariationInterval", x => x.VariationIntervalId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Variation_VariationIntervalId",
                table: "Variation",
                column: "VariationIntervalId");

            migrationBuilder.AddForeignKey(
                name: "FK_Variation_VariationInterval_VariationIntervalId",
                table: "Variation",
                column: "VariationIntervalId",
                principalTable: "VariationInterval",
                principalColumn: "VariationIntervalId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
