﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class add_euro_for_position : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PurchasePrice",
                table: "Position",
                newName: "LocalPurchasePrice");

            migrationBuilder.RenameColumn(
                name: "CurrentPrice",
                table: "Position",
                newName: "LocalCurrentPrice");

            migrationBuilder.AddColumn<double>(
                name: "EuroCurrentPrice",
                table: "Position",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "EuroPurchasePrice",
                table: "Position",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EuroCurrentPrice",
                table: "Position");

            migrationBuilder.DropColumn(
                name: "EuroPurchasePrice",
                table: "Position");

            migrationBuilder.RenameColumn(
                name: "LocalPurchasePrice",
                table: "Position",
                newName: "PurchasePrice");

            migrationBuilder.RenameColumn(
                name: "LocalCurrentPrice",
                table: "Position",
                newName: "CurrentPrice");
        }
    }
}
