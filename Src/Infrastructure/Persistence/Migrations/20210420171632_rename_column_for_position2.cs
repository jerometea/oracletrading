﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class rename_column_for_position2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TodayPercentChange",
                table: "Position",
                newName: "TodayChangePercentage");

            migrationBuilder.RenameColumn(
                name: "PercentGain",
                table: "Position",
                newName: "PurchasePriceInForeignCurrency");

            migrationBuilder.RenameColumn(
                name: "OpenForeignDate",
                table: "Position",
                newName: "FrenchLastUpdateDate");

            migrationBuilder.RenameColumn(
                name: "OpenDate",
                table: "Position",
                newName: "FrenchOpenDate");

            migrationBuilder.RenameColumn(
                name: "LastUpdateDate",
                table: "Position",
                newName: "FrenchCloseDate");

            migrationBuilder.RenameColumn(
                name: "LastForeignUpdateDate",
                table: "Position",
                newName: "ForeignOpenDate");

            migrationBuilder.RenameColumn(
                name: "ForeignPurchasePrice",
                table: "Position",
                newName: "GainPurcentage");

            migrationBuilder.RenameColumn(
                name: "ForeignCurrentPrice",
                table: "Position",
                newName: "CurrentPriceInForeignCurrency");

            migrationBuilder.RenameColumn(
                name: "CloseForeignDate",
                table: "Position",
                newName: "ForeignLastUpdateDate");

            migrationBuilder.RenameColumn(
                name: "CloseDate",
                table: "Position",
                newName: "ForeignCloseDate");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TodayChangePercentage",
                table: "Position",
                newName: "TodayPercentChange");

            migrationBuilder.RenameColumn(
                name: "PurchasePriceInForeignCurrency",
                table: "Position",
                newName: "PercentGain");

            migrationBuilder.RenameColumn(
                name: "GainPurcentage",
                table: "Position",
                newName: "ForeignPurchasePrice");

            migrationBuilder.RenameColumn(
                name: "FrenchOpenDate",
                table: "Position",
                newName: "OpenDate");

            migrationBuilder.RenameColumn(
                name: "FrenchLastUpdateDate",
                table: "Position",
                newName: "OpenForeignDate");

            migrationBuilder.RenameColumn(
                name: "FrenchCloseDate",
                table: "Position",
                newName: "LastUpdateDate");

            migrationBuilder.RenameColumn(
                name: "ForeignOpenDate",
                table: "Position",
                newName: "LastForeignUpdateDate");

            migrationBuilder.RenameColumn(
                name: "ForeignLastUpdateDate",
                table: "Position",
                newName: "CloseForeignDate");

            migrationBuilder.RenameColumn(
                name: "ForeignCloseDate",
                table: "Position",
                newName: "CloseDate");

            migrationBuilder.RenameColumn(
                name: "CurrentPriceInForeignCurrency",
                table: "Position",
                newName: "ForeignCurrentPrice");
        }
    }
}
