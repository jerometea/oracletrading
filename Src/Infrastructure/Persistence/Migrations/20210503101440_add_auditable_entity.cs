﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class add_auditable_entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Creation",
                table: "Portfolio");

            migrationBuilder.RenameColumn(
                name: "Update",
                table: "Portfolio",
                newName: "Created");

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "Created",
                table: "StatisticArchive",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "LastModified",
                table: "StatisticArchive",
                type: "datetimeoffset",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "Created",
                table: "Statistic",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "LastModified",
                table: "Statistic",
                type: "datetimeoffset",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "Created",
                table: "Position",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "LastModified",
                table: "Position",
                type: "datetimeoffset",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "LastModified",
                table: "Portfolio",
                type: "datetimeoffset",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Created",
                table: "StatisticArchive");

            migrationBuilder.DropColumn(
                name: "LastModified",
                table: "StatisticArchive");

            migrationBuilder.DropColumn(
                name: "Created",
                table: "Statistic");

            migrationBuilder.DropColumn(
                name: "LastModified",
                table: "Statistic");

            migrationBuilder.DropColumn(
                name: "Created",
                table: "Position");

            migrationBuilder.DropColumn(
                name: "LastModified",
                table: "Position");

            migrationBuilder.DropColumn(
                name: "LastModified",
                table: "Portfolio");

            migrationBuilder.RenameColumn(
                name: "Created",
                table: "Portfolio",
                newName: "Update");

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "Creation",
                table: "Portfolio",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));
        }
    }
}
