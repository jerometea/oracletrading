﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class add_statisticsArchives_for_position : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PositionRegressionSubtype");

            migrationBuilder.AddColumn<int>(
                name: "RegressionSubtypeId",
                table: "Position",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "StatisticArchive",
                columns: table => new
                {
                    StatisticArchiveId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StockId = table.Column<int>(type: "int", nullable: true),
                    RegressionSubtypeId = table.Column<int>(type: "int", nullable: true),
                    Score = table.Column<double>(type: "float", nullable: false),
                    Integrity = table.Column<double>(type: "float", nullable: false),
                    TotalValues = table.Column<int>(type: "int", nullable: false),
                    TimeframeId = table.Column<int>(type: "int", nullable: true),
                    PositionId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StatisticArchive", x => x.StatisticArchiveId);
                    table.ForeignKey(
                        name: "FK_StatisticArchive_Position_PositionId",
                        column: x => x.PositionId,
                        principalTable: "Position",
                        principalColumn: "PositionId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StatisticArchive_RegressionSubtype_RegressionSubtypeId",
                        column: x => x.RegressionSubtypeId,
                        principalTable: "RegressionSubtype",
                        principalColumn: "RegressionSubtypeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StatisticArchive_Stock_StockId",
                        column: x => x.StockId,
                        principalTable: "Stock",
                        principalColumn: "StockId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StatisticArchive_Timeframe_TimeframeId",
                        column: x => x.TimeframeId,
                        principalTable: "Timeframe",
                        principalColumn: "TimeframeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Position_RegressionSubtypeId",
                table: "Position",
                column: "RegressionSubtypeId");

            migrationBuilder.CreateIndex(
                name: "IX_StatisticArchive_PositionId",
                table: "StatisticArchive",
                column: "PositionId");

            migrationBuilder.CreateIndex(
                name: "IX_StatisticArchive_RegressionSubtypeId",
                table: "StatisticArchive",
                column: "RegressionSubtypeId");

            migrationBuilder.CreateIndex(
                name: "IX_StatisticArchive_StockId",
                table: "StatisticArchive",
                column: "StockId");

            migrationBuilder.CreateIndex(
                name: "IX_StatisticArchive_TimeframeId",
                table: "StatisticArchive",
                column: "TimeframeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Position_RegressionSubtype_RegressionSubtypeId",
                table: "Position",
                column: "RegressionSubtypeId",
                principalTable: "RegressionSubtype",
                principalColumn: "RegressionSubtypeId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Position_RegressionSubtype_RegressionSubtypeId",
                table: "Position");

            migrationBuilder.DropTable(
                name: "StatisticArchive");

            migrationBuilder.DropIndex(
                name: "IX_Position_RegressionSubtypeId",
                table: "Position");

            migrationBuilder.DropColumn(
                name: "RegressionSubtypeId",
                table: "Position");

            migrationBuilder.CreateTable(
                name: "PositionRegressionSubtype",
                columns: table => new
                {
                    PositionsPositionId = table.Column<int>(type: "int", nullable: false),
                    RegressionSubtypesRegressionSubtypeId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PositionRegressionSubtype", x => new { x.PositionsPositionId, x.RegressionSubtypesRegressionSubtypeId });
                    table.ForeignKey(
                        name: "FK_PositionRegressionSubtype_Position_PositionsPositionId",
                        column: x => x.PositionsPositionId,
                        principalTable: "Position",
                        principalColumn: "PositionId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PositionRegressionSubtype_RegressionSubtype_RegressionSubtypesRegressionSubtypeId",
                        column: x => x.RegressionSubtypesRegressionSubtypeId,
                        principalTable: "RegressionSubtype",
                        principalColumn: "RegressionSubtypeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PositionRegressionSubtype_RegressionSubtypesRegressionSubtypeId",
                table: "PositionRegressionSubtype",
                column: "RegressionSubtypesRegressionSubtypeId");
        }
    }
}
