﻿using Domain.Calculations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Domain.UnitTests
{
    public class CalculateChangeTest
    {
        [Fact]
        public void Calculate_variation_of_stock()
        {
            double variation1 = ChangePercent.Calculate(1.75, 3.5);
            double variation2 = ChangePercent.Calculate(100, 200);
            double variation3 = ChangePercent.Calculate(300, 600);
            double variation4 = ChangePercent.Calculate(1.5, 3);

            Assert.Equal(100, variation1);
            Assert.Equal(100, variation2);
            Assert.Equal(100, variation3);
            Assert.Equal(100, variation4);
        }
    }
}
