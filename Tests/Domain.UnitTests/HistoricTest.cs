﻿using Domain.Statistics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xunit;

namespace Domain.UnitTests
{
    public class HistoricTest
    {
        [Fact]
        public void Complete_invalid_historic()
        {
            List<double?> invalidHistoric = new List<double?> { null, null, null, null, null, 40, null, null, 43, null, 45, 46, 47, 48, null, null, null, null, null, null, null, 56, 57, null, null, null, null, null, 63, null, 65, 66, 67, 68, null, 70, null, 72, null, null, null, null, null, null, null, null, null, null, null, null };

            List<double> validHistoric = new List<double>() { 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84 };

            var result = HistoricFill.CompleteInvalidHistoric(invalidHistoric);

            Assert.Equal(validHistoric, result);
        }
    }
}

