﻿using Domain.Calculations;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Domain.UnitTests
{
    public class CountVariationsTest
    {
        [Fact]
        public void Count_numbers_of_variations_in_price_historic()
        {
            List<double?> historic1 = new List<double?>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, null, null, null, null, null, null, null, null, null, null, null, null, null };
            List<Variation> variations = HighChangeCounter.Count(historic1);

            #region price 1
            Variation first1 = new Variation() { Percent = 100, Day = 1 }; // 2
            Variation first2 = new Variation() { Percent = 200, Day = 2 }; // 3
            Variation first3 = new Variation() { Percent = 300, Day = 3 }; // 4
            Variation first4 = new Variation() { Percent = 400, Day = 4 }; // 5
            Variation first5 = new Variation() { Percent = 500, Day = 5 }; // 6
            Variation first6 = new Variation() { Percent = 600, Day = 6 }; // 7
            Variation first7 = new Variation() { Percent = 700, Day = 7 }; // 8
            Variation first8 = new Variation() { Percent = 800, Day = 8 }; // 9
            Variation first9 = new Variation() { Percent = 900, Day = 9 }; // 10
            Variation first10 = new Variation() { Percent = 1000, Day = 10 }; // 11
            Variation first11 = new Variation() { Percent = 1100, Day = 11 }; // 12
            Variation first12 = new Variation() { Percent = 1200, Day = 12 }; // 13

            Assert.Equal(first1.Percent, variations[0].Percent);
            Assert.Equal(first2.Percent, variations[1].Percent);
            Assert.Equal(first3.Percent, variations[2].Percent);
            Assert.Equal(first4.Percent, variations[3].Percent);
            Assert.Equal(first5.Percent, variations[4].Percent);
            Assert.Equal(first6.Percent, variations[5].Percent);
            Assert.Equal(first7.Percent, variations[6].Percent);
            Assert.Equal(first8.Percent, variations[7].Percent);
            Assert.Equal(first9.Percent, variations[8].Percent);
            Assert.Equal(first10.Percent, variations[9].Percent);
            Assert.Equal(first11.Percent, variations[10].Percent);
            Assert.Equal(first12.Percent, variations[11].Percent);

            Assert.Equal(1, variations[0].Day);
            Assert.Equal(2, variations[1].Day);
            Assert.Equal(3, variations[2].Day);
            Assert.Equal(4, variations[3].Day);
            Assert.Equal(5, variations[4].Day);
            Assert.Equal(6, variations[5].Day);
            Assert.Equal(7, variations[6].Day);
            Assert.Equal(8, variations[7].Day);
            Assert.Equal(9, variations[8].Day);
            Assert.Equal(10, variations[9].Day);
            Assert.Equal(11, variations[10].Day);
            Assert.Equal(12, variations[11].Day);
            #endregion

            #region price 2
            Variation second1 = new Variation() { Percent = 50, Day = 1 };
            Variation second2 = new Variation() { Percent = 100, Day = 2 };
            Variation second3 = new Variation() { Percent = 150, Day = 3 };
            Variation second4 = new Variation() { Percent = 200, Day = 4 };
            Variation second5 = new Variation() { Percent = 250, Day = 5 };
            Variation second6 = new Variation() { Percent = 300, Day = 6 };
            Variation second7 = new Variation() { Percent = 350, Day = 7 };
            Variation second8 = new Variation() { Percent = 400, Day = 8 };
            Variation second9 = new Variation() { Percent = 450, Day = 9 };
            Variation second10 = new Variation() { Percent = 500, Day = 10 };
            Variation second11 = new Variation() { Percent = 550, Day = 11 };
            Variation second12 = new Variation() { Percent = 600, Day = 12 };

            Assert.Equal(second1.Percent, variations[12].Percent);
            Assert.Equal(second2.Percent, variations[13].Percent);
            Assert.Equal(second3.Percent, variations[14].Percent);
            Assert.Equal(second4.Percent, variations[15].Percent);
            Assert.Equal(second5.Percent, variations[16].Percent);
            Assert.Equal(second6.Percent, variations[17].Percent);
            Assert.Equal(second7.Percent, variations[18].Percent);
            Assert.Equal(second8.Percent, variations[19].Percent);
            Assert.Equal(second9.Percent, variations[20].Percent);
            Assert.Equal(second10.Percent, variations[21].Percent);
            Assert.Equal(second11.Percent, variations[22].Percent);
            Assert.Equal(second12.Percent, variations[23].Percent);

            Assert.Equal(1, variations[12].Day);
            Assert.Equal(2, variations[13].Day);
            Assert.Equal(3, variations[14].Day);
            Assert.Equal(4, variations[15].Day);
            Assert.Equal(5, variations[16].Day);
            Assert.Equal(6, variations[17].Day);
            Assert.Equal(7, variations[18].Day);
            Assert.Equal(8, variations[19].Day);
            Assert.Equal(9, variations[20].Day);
            Assert.Equal(10, variations[21].Day);
            Assert.Equal(11, variations[22].Day);
            Assert.Equal(12, variations[23].Day);
            #endregion

            #region price 3
            Variation third01 = new Variation() { Percent = 33.3, Day = 1 };
            Variation third02 = new Variation() { Percent = 66.7, Day = 2 };
            Variation third03 = new Variation() { Percent = 100, Day = 3 };
            Variation third04 = new Variation() { Percent = 133.3, Day = 4 };
            Variation third05 = new Variation() { Percent = 166.7, Day = 5 };
            Variation third06 = new Variation() { Percent = 200, Day = 6 };
            Variation third07 = new Variation() { Percent = 233.3, Day = 7 };
            Variation third08 = new Variation() { Percent = 266.7, Day = 8 };
            Variation third09 = new Variation() { Percent = 300, Day = 9 };
            Variation third10 = new Variation() { Percent = 333.3, Day = 10 };
            Variation third11 = new Variation() { Percent = 366.7, Day = 11 };

            Assert.Equal(third01.Percent, variations[24].Percent);
            Assert.Equal(third02.Percent, variations[25].Percent);
            Assert.Equal(third03.Percent, variations[26].Percent);
            Assert.Equal(third04.Percent, variations[27].Percent);
            Assert.Equal(third05.Percent, variations[28].Percent);
            Assert.Equal(third06.Percent, variations[29].Percent);
            Assert.Equal(third07.Percent, variations[30].Percent);
            Assert.Equal(third08.Percent, variations[31].Percent);
            Assert.Equal(third09.Percent, variations[32].Percent);
            Assert.Equal(third10.Percent, variations[33].Percent);
            Assert.Equal(third11.Percent, variations[34].Percent);

            Assert.Equal(1, variations[24].Day);
            Assert.Equal(2, variations[25].Day);
            Assert.Equal(3, variations[26].Day);
            Assert.Equal(4, variations[27].Day);
            Assert.Equal(5, variations[28].Day);
            Assert.Equal(6, variations[29].Day);
            Assert.Equal(7, variations[30].Day);
            Assert.Equal(8, variations[31].Day);
            Assert.Equal(9, variations[32].Day);
            Assert.Equal(10, variations[33].Day);
            Assert.Equal(11, variations[34].Day);
            #endregion

            #region price 4
            Variation fourth01 = new Variation() { Percent = 25, Day = 1 };
            Variation fourth02 = new Variation() { Percent = 50, Day = 2 };
            Variation fourth03 = new Variation() { Percent = 75, Day = 3 };
            Variation fourth04 = new Variation() { Percent = 100, Day = 4 };
            Variation fourth05 = new Variation() { Percent = 125, Day = 5 };
            Variation fourth06 = new Variation() { Percent = 150, Day = 6 };
            Variation fourth07 = new Variation() { Percent = 175, Day = 7 };
            Variation fourth08 = new Variation() { Percent = 200, Day = 8 };
            Variation fourth09 = new Variation() { Percent = 225, Day = 9 };
            Variation fourth10 = new Variation() { Percent = 250, Day = 10 };

            Assert.Equal(fourth01.Percent, variations[35].Percent);
            Assert.Equal(fourth02.Percent, variations[36].Percent);
            Assert.Equal(fourth03.Percent, variations[37].Percent);
            Assert.Equal(fourth04.Percent, variations[38].Percent);
            Assert.Equal(fourth05.Percent, variations[39].Percent);
            Assert.Equal(fourth06.Percent, variations[40].Percent);
            Assert.Equal(fourth07.Percent, variations[41].Percent);
            Assert.Equal(fourth08.Percent, variations[42].Percent);
            Assert.Equal(fourth09.Percent, variations[43].Percent);
            Assert.Equal(fourth10.Percent, variations[44].Percent);

            Assert.Equal(1, variations[35].Day);
            Assert.Equal(2, variations[36].Day);
            Assert.Equal(3, variations[37].Day);
            Assert.Equal(4, variations[38].Day);
            Assert.Equal(5, variations[39].Day);
            Assert.Equal(6, variations[40].Day);
            Assert.Equal(7, variations[41].Day);
            Assert.Equal(8, variations[42].Day);
            Assert.Equal(9, variations[43].Day);
            Assert.Equal(10, variations[44].Day);
            #endregion

        }

        [Fact]
        public void Count_negative_variations_in_price_historic()
        {
            List<double?> historic1 = new List<double?>() { 50, 50, 40, 35, 40, 50, 35, 70, 75, 80, 70, 65, 60, 65, null, null, null, null, null, null, null, null, null, null, null, null, null };
            List<Variation> variations = HighChangeCounter.Count(historic1);

            #region price 50
            Variation first1 = new Variation() { Percent = 0, Day = 1 }; // 50
            Variation first2 = new Variation() { Percent = -20, Day = 2 }; // 40
            Variation first3 = new Variation() { Percent = -30, Day = 3 }; // 35
            Variation first4 = new Variation() { Percent = -20, Day = 4 }; // 40
            Variation first5 = new Variation() { Percent = 0, Day = 5 }; // 50
            Variation first6 = new Variation() { Percent = -30, Day = 6 }; // 35
            Variation first7 = new Variation() { Percent = 40, Day = 7 }; // 70
            Variation first8 = new Variation() { Percent = 50, Day = 8 }; // 75
            Variation first9 = new Variation() { Percent = 60, Day = 9 }; // 80
            Variation first10 = new Variation() { Percent = 40, Day = 10 }; // 70
            Variation first11 = new Variation() { Percent = 30, Day = 11 }; // 65
            Variation first12 = new Variation() { Percent = 20, Day = 12 }; // 60

            Assert.Equal(first1.Percent, variations[0].Percent);
            Assert.Equal(first2.Percent, variations[1].Percent);
            Assert.Equal(first3.Percent, variations[2].Percent);
            Assert.Equal(first4.Percent, variations[3].Percent);
            Assert.Equal(first5.Percent, variations[4].Percent);
            Assert.Equal(first6.Percent, variations[5].Percent);
            Assert.Equal(first7.Percent, variations[6].Percent);
            Assert.Equal(first8.Percent, variations[7].Percent);
            Assert.Equal(first9.Percent, variations[8].Percent);
            Assert.Equal(first10.Percent, variations[9].Percent);
            Assert.Equal(first11.Percent, variations[10].Percent);
            Assert.Equal(first12.Percent, variations[11].Percent);

            Assert.Equal(1, variations[0].Day);
            Assert.Equal(2, variations[1].Day);
            Assert.Equal(3, variations[2].Day);
            Assert.Equal(4, variations[3].Day);
            Assert.Equal(5, variations[4].Day);
            Assert.Equal(6, variations[5].Day);
            Assert.Equal(7, variations[6].Day);
            Assert.Equal(8, variations[7].Day);
            Assert.Equal(9, variations[8].Day);
            Assert.Equal(10, variations[9].Day);
            Assert.Equal(11, variations[10].Day);
            Assert.Equal(12, variations[11].Day);
            #endregion

        }

        [Fact]
        public void lol()
        {
            List<double?> historic1 = new List<double?>()
            {
                3195.340087890625,
                                3168.0400390625,
                                3220.080078125,
                                3203.530029296875,
                                3186.72998046875,
                                3162.580078125,
                                3158.0,
                                3177.2900390625,
                                3104.199951171875,
                                3101.489990234375,
                                3116.419921875,
                                3156.969970703125,
                                3165.1201171875,
                                3240.9599609375,
                                3236.080078125,
                                3201.64990234375,
                                3206.179931640625,
                                3206.52001953125,
                                3185.27001953125,
                                3172.68994140625,
                                3283.9599609375,
                                3322.0,
                                3285.85009765625,
                                3256.929931640625,
                                3186.6298828125,
                                3218.510009765625,
                                3138.3798828125,
                                3162.159912109375,
                                3182.699951171875,
                                3114.2099609375,
                                3120.830078125,
                                3165.889892578125,
                                3127.469970703125,
                                3104.25,
                                3120.760009765625,
                                3263.3798828125,
                                3306.989990234375,
                                3292.22998046875,
                                3294.0,
                                3326.1298828125,
                                3232.580078125,
                                3237.6201171875,
                                3206.199951171875,
                                3342.8798828125,
                                3380.0,
                                3312.530029296875,
                                3331.0,
                                3352.14990234375,
                                3322.93994140625,
                                3305.0,
                                3286.580078125,
                                3262.1298828125,
                                3277.7099609375,
                                3268.949951171875,
                                3308.639892578125,
                                3328.22998046875,
                                3249.89990234375,
                                3180.739990234375,
                                3194.5,
                                3159.530029296875,
                                3057.159912109375,
                                3092.929931640625,
                                3146.139892578125,
                                3094.530029296875,
                                3005.0,
                                2977.570068359375,
                                3000.4599609375,
                                2951.949951171875,
                                3062.85009765625,
                                3057.639892578125,
                                3113.590087890625,
                                3089.489990234375,
                                3081.679931640625,
                                3091.860107421875,
                                3135.72998046875,
                                3027.989990234375,
                                3074.9599609375,
                                3110.8701171875,
                                3137.5,
                                3087.070068359375,
                                3046.260009765625,
                                3052.030029296875,
                                3075.72998046875,
                                3055.2900390625,
                                3094.080078125,
                                3161.0,
                                3226.72998046875,
                                3223.820068359375,
                                3279.389892578125,
                                3299.300048828125,
                                3372.199951171875,
                                3379.389892578125,
                                3400.0,
                                3333.0,
                                3379.090087890625,
                                3399.43994140625,
                                3372.010009765625,
                                3334.68994140625,
                                3362.02001953125,
                                3309.0400390625,
                                3340.8798828125,
                                3409.0,
                                3417.429931640625,
                                3458.5,
                                3471.31005859375,
                                3467.419921875,
                                3386.489990234375,
                                3311.8701171875,
                                3270.5400390625,
                                3306.3701171875,
                                3291.610107421875,
                                3190.489990234375,
                                3223.909912109375,
                                3151.93994140625,
                                3161.469970703125,
                                3222.89990234375,
                                3270.389892578125,
                                3232.280029296875,
                                3231.800048828125,
                                3247.679931640625,
                                3203.080078125,
                                3244.989990234375,
                                3259.050048828125,
                                3265.159912109375,
                                3230.110107421875,
                                3223.070068359375
            };
            List<Variation> variations = HighChangeCounter.Count(historic1);

        }
    }
}
