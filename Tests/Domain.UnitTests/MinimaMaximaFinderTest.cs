﻿using Domain.Calculations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Domain.UnitTests
{
    public class MinimaMaximaFinderTest
    {
        [Fact]
        public void Find_minimas_and_maximas()
        {                                         
            List<double> prices = new List<double> { 1, 5, 4, 6, 7, 8, 10, 5, 5, 5, 7, 6 };
            IList<(int position, bool isMaxima)> minimasMaximasPositions = MinimaMaximaFinder.GetMinimasMaximasPositions(prices);

            Assert.Equal((1, true), minimasMaximasPositions[0]);
            Assert.Equal((2, false), minimasMaximasPositions[1]);
            Assert.Equal((6, true), minimasMaximasPositions[2]);
            Assert.Equal((8, false), minimasMaximasPositions[3]);
            Assert.Equal((10, true), minimasMaximasPositions[4]);
        }
    }
}
