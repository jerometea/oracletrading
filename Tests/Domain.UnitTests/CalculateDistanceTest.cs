﻿using Domain.Calculations;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Domain.UnitTests
{
    public class CalculateDistanceTest
    {
        [Fact]
        public void Calculate_difference_between_regression_subtypefile_count_and_historic_count()
        {
            int regressionSubtypeFileCount = 15;
            int historicCount = 10;

            int distance1 = Distance.Calculate(regressionSubtypeFileCount, historicCount);
            int distance2 = Distance.Calculate(historicCount, regressionSubtypeFileCount);

            Assert.Equal(5, distance1);
            Assert.Equal(5, distance2);
        }
    }
}
