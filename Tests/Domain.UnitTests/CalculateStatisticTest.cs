﻿using Domain.Entities;
using Domain.Statistics;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using Xunit;

namespace Domain.UnitTests
{
    public class CalculateStatisticTest
    {
        [Fact]
        public void Return_lowest_score_with_regression_subtype()
        {
            List<double> historic = new List<double>() { 10, 5, 5, 5, 5, 5, 5, 5 };

            List<RegressionSubtype> regressionSubtypes = new List<RegressionSubtype>();
            RegressionSubtype regressionSubtype_1 = new RegressionSubtype() { RegressionSubtypeName = "YTD_50C", Prices = new List<double>() { 100, 50, 50, 50, 50 } };
            RegressionSubtype regressionSubtype_2 = new RegressionSubtype() { RegressionSubtypeName = "YTD_50.5C", Prices = new List<double>() { 100, 51, 50, 49, 50.5 } };
            regressionSubtypes.Add(regressionSubtype_1);
            regressionSubtypes.Add(regressionSubtype_2);

            Mock<IRegressionSubtypeReader> mockRegressionSubtypeRegister = new Mock<IRegressionSubtypeReader>();
            mockRegressionSubtypeRegister.Setup(register => register.OpenRegressionSubtypes(false)).Returns(regressionSubtypes);

            RegressionFit calculateStatistic = new RegressionFit(mockRegressionSubtypeRegister.Object);

            Statistic result = calculateStatistic.Analyse(historic);

            Assert.Equal("YTD_50C", result.RegressionSubtype.RegressionSubtypeName);
            Assert.True(result.Score == 0);
        }
    }
}

