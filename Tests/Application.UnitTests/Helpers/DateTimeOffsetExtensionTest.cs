﻿using Application.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Application.Tests.Helpers
{
    public class DateTimeOffsetExtensionTest
    {
        [Fact]
        public void Add_working_days_to_DateTimeOffSet()
        {
            DateTimeOffset mondayOpenDate = new DateTimeOffset(2021, 06, 14, 9, 0, 0, DateTimeOffset.Now.Offset);

            DateTimeOffset junElevenResult = mondayOpenDate.AddWorkingDays(-1);
            DateTimeOffset junSevenResult = mondayOpenDate.AddWorkingDays(-5);
            DateTimeOffset junFourResult = mondayOpenDate.AddWorkingDays(-6);
            DateTimeOffset junThreeResult = mondayOpenDate.AddWorkingDays(-7);
            DateTimeOffset junOneResult = mondayOpenDate.AddWorkingDays(-9);
            DateTimeOffset mayThirtyOneResult = mondayOpenDate.AddWorkingDays(-10);
            DateTimeOffset mayTwentyEightResult = mondayOpenDate.AddWorkingDays(-11);
            DateTimeOffset mayTwentyFourResult = mondayOpenDate.AddWorkingDays(-15);
            DateTimeOffset mayTwentOneResult = mondayOpenDate.AddWorkingDays(-16);

            DateTime junElevenExpected = new DateTime(2021, 06, 11).Date;
            DateTime junSevenExpected = new DateTime(2021, 06, 7).Date;
            DateTime junFourExpected = new DateTime(2021, 06, 4).Date;
            DateTime junThreeExpected = new DateTime(2021, 06, 3).Date;
            DateTime junOneExpected = new DateTime(2021, 06, 1).Date;
            DateTime mayThirtyOneExpected = new DateTime(2021, 05, 31).Date;
            DateTime mayTwentyEightExpected = new DateTime(2021, 05, 28).Date;
            DateTime mayTwentyFourExpected = new DateTime(2021, 05, 24).Date;
            DateTime mayTwentyOneExpected = new DateTime(2021, 05, 21).Date;

            Assert.Throws<ArgumentOutOfRangeException>(() => mondayOpenDate.AddWorkingDays(1));
            Assert.Equal(junElevenExpected, junElevenResult.Date);
            Assert.Equal(junSevenExpected, junSevenResult.Date);
            Assert.Equal(junFourExpected, junFourResult.Date);
            Assert.Equal(junThreeExpected, junThreeResult.Date);
            Assert.Equal(junOneExpected, junOneResult.Date);
            Assert.Equal(mayThirtyOneExpected, mayThirtyOneResult.Date);
            Assert.Equal(mayTwentyEightExpected, mayTwentyEightResult.Date);
            Assert.Equal(mayTwentyFourExpected, mayTwentyFourResult.Date);
            Assert.Equal(mayTwentyOneExpected, mayTwentOneResult.Date);
        }

    }
}
