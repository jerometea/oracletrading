﻿using Application.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Application.Tests.Helpers
{
    public class RemoveNullsFromBuggyHistoricTest
    {
        [Fact]
        public void Remove_nulls_and_last_price_from_buggy_historic()
        {
            List<double?> nullsPriceHistoric = new List<double?>() { 10.2, 10.7, 11.6, 14.2, 11.3, null, 10.7, 12.6, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 50.6 };
            List<double?> expectedHistoric = new List<double?>() { 10.2, 10.7, 11.6, 14.2, 11.3, null, 10.7, 12.6 };

            List<double?> claenHistoric = RemoveNullsFromBuggyHistoric.RemoveNulls(nullsPriceHistoric);

            Assert.Equal(expectedHistoric, claenHistoric);
        }
    }
}
